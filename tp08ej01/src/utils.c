#include "utils.h"
#include "lpc_types.h"


uint8_t digitoBase10(uint32_t valor, uint8_t pos){
	uint32_t divisor=pow(10,pos+1);
	uint32_t resto = valor%divisor;
	uint32_t dig=resto*10/divisor;
	return (uint8_t) dig;
}

uint8_t numASCII(uint8_t num){
	uint8_t char0 = '0';
	return char0+num;
}

uint8_t cantDigitos(uint32_t v){
	uint8_t cant=1;
	while(v>=10){
		v/=10;
		cant++;
	}
	return cant;
}



