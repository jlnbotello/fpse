/*
 * menu.c
 *
 *  Created on: Oct 18, 2018
 *      Author: quack
 */
#include "menu.h"
#include "lpc_types.h"
#include "chip.h"
#include "hw.h"
#include "utils.h"



inline uint32_t stackSize ( void ){
	return & _sStack - & _eStack ;
}
inline uint32_t stackUsed ( void ){
	return & _sStack - ( uint8_t *) __get_MSP ();
}


/* >>MENU NIVEL 0<< */
const menuEntry menuPrincipal [] ={
	{"1 - Led 1\n", doLed1 },
	{"2 - Led 2\n", doLed2 },
	{"3 - Led 3\n", doLed3 },
	{"4 - Led RGB \n", doRgb },
	{"5 - Estado del Stack \n", doMemInfo },
	{"6 - Sistem Reset \n", doReset },
	{NULL , NULL }};
/* >>MENU NIVEL 1<< */
const menuEntry menuLed1 [] = {
	{"1 - Encender led 1\n", doLed1On },
	{"2 - Apagar led 1\n", doLed1Off },
	{"3 - Alterar estado \n", doLed1Toggle },
	{"4 - Volver ...\n", doBackMain },
	{NULL , NULL }};

const menuEntry menuLed2 [] = {
	{"1 - Encender led 2\n", doLed2On },
	{"2 - Apagar led 2\n", doLed2Off },
	{"3 - Alterar estado \n", doLed2Toggle },
	{"4 - Volver ...\n", doBackMain },
	{NULL , NULL }};

const menuEntry menuLed3 [] = {
	{"1 - Encender led 3\n", doLed3On },
	{"2 - Apagar led 3\n", doLed3Off },
	{"3 - Alterar estado \n", doLed3Toggle },
	{"4 - Volver ...\n", doBackMain },
	{NULL , NULL }};

const menuEntry menuRgb [] = {
	{"1 - Led Rojo\n", doLedR },
	{"2 - Led Verde\n", doLedG },
	{"3 - Led Azul\n", doLedB },
	{"4 - Apagar todos\n", doLedAllOff },
	{"5 - Volver ...\n", doBackMain },
	{NULL , NULL }};
/* >>MENU NIVEL 2<< */
const menuEntry menuLedR [] = {
	{"1 - Encender led R\n", doLedROn },
	{"2 - Apagar led R\n", doLedROff },
	{"3 - Alterar estado \n", doLedRToggle },
	{"4 - Volver ...\n", doBackRgb },
	{NULL , NULL }};

const menuEntry menuLedG [] = {
	{"1 - Encender led G\n", doLedGOn },
	{"2 - Apagar led G\n", doLedGOff },
	{"3 - Alterar estado \n", doLedGToggle },
	{"4 - Volver ...\n", doBackRgb },
	{NULL , NULL }};

const menuEntry menuLedB [] = {
	{"1 - Encender led B\n", doLedBOn },
	{"2 - Apagar led B\n", doLedBOff },
	{"3 - Alterar estado \n", doLedBToggle },
	{"4 - Volver ...\n", doBackRgb },
	{NULL , NULL }};

/* >>MENU NIVEL 0<< */
menuEntry * getMainMenu ( void ){return menuPrincipal;}
// OPCIONES
menuEntry * doLed1 	  	( void ){ return menuLed1;}
menuEntry * doLed2 	  	( void ){ return menuLed2;}
menuEntry * doLed3 		( void ){ return menuLed3;}
menuEntry * doRgb  	  	( void ){ return menuRgb; }
menuEntry * doMemInfo	( void ){
	printBlocking("Memoria usada en el stack: ");
	mostrarASCII(stackUsed());
	printBlocking(" Bytes \r\n");
	return menuPrincipal;} //TODO
menuEntry * doReset   	( void ){ Chip_RGU_TriggerReset ( RGU_CORE_RST ); return menuPrincipal ;}

/* >>MENU NIVEL 1<< */
menuEntry * doBackMain 	( void ){return menuPrincipal ;}
//LED 1
menuEntry * doLed1On    ( void ){ledOn(LED1);		return menuLed1 ;}
menuEntry * doLed1Off  	( void ){ledOff(LED1);		return menuLed1 ;}
menuEntry * doLed1Toggle( void ){toggleLed(LED1);	return menuLed1 ;}
//LED 2
menuEntry * doLed2On   	( void ){ledOn(LED2);		return menuLed2 ;}
menuEntry * doLed2Off 	( void ){ledOff(LED2);		return menuLed2 ;}
menuEntry * doLed2Toggle( void ){toggleLed(LED2);	return menuLed2 ;}
//LED 3
menuEntry * doLed3On   	( void ){ledOn(LED3);		return menuLed3 ;}
menuEntry * doLed3Off 	( void ){ledOff(LED3);		return menuLed3 ;}
menuEntry * doLed3Toggle( void ){toggleLed(LED3);	return menuLed3 ;}
//LEDS RGB
menuEntry * doLedR		( void ){ return menuLedR;}
menuEntry * doLedG		( void ){ return menuLedG;}
menuEntry * doLedB		( void ){ return menuLedB;}
menuEntry * doLedAllOff	( void ){ ledOff(LEDR);ledOff(LEDG);ledOff(LEDB);return menuRgb;}

/* >>MENU NIVEL 2<< */
menuEntry * doBackRgb	( void ){ return menuRgb;}
//LED R
menuEntry * doLedROn    ( void ){ledOn(LEDR);		return menuRgb ;}
menuEntry * doLedROff  	( void ){ledOff(LEDR);		return menuRgb ;}
menuEntry * doLedRToggle( void ){toggleLed(LEDR);	return menuRgb ;}
//LED G
menuEntry * doLedGOn   	( void ){ledOn(LEDG);		return menuRgb ;}
menuEntry * doLedGOff 	( void ){ledOff(LEDG);		return menuRgb ;}
menuEntry * doLedGToggle( void ){toggleLed(LEDG);	return menuRgb ;}
//LED B
menuEntry * doLedBOn   	( void ){ledOn(LEDB);		return menuRgb ;}
menuEntry * doLedBOff 	( void ){ledOff(LEDB);		return menuRgb ;}
menuEntry * doLedBToggle( void ){toggleLed(LEDB);	return menuRgb ;}










