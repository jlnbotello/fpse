/*
 * menu.h
 *
 *  Created on: Oct 18, 2018
 *      Author: quack
 */

#ifndef INC_MENU_H_
#define INC_MENU_H_
#include "stdint.h"

extern uint8_t _sStack ;
extern uint8_t _eStack ;

inline uint32_t stackSize ( void )
__attribute__ (( always_inline ));
inline uint32_t stackUsed ( void )
__attribute__ (( always_inline ));


typedef void * (* ptDo )( void ); /* Puntero a una función que representa una acción */

/* Representación de un elemento de menú */
typedef struct {
	char *txt;	 /* Etiqueta de la opción */
	ptDo doit ;	 /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry ;

menuEntry * getMainMenu ( void ); /* Retorna la dirección del menú principal */

/* Opciones de primer nivel ( acciones del menú principal ) */
menuEntry * doLed1 ( void ); 	/* Ingresa al submenú Led1 */
menuEntry * doLed2 ( void ); 	/* Ingresa al submenú Led2 */
menuEntry * doLed3 ( void ); 	/* Ingresa al submenú Led3 */
menuEntry * doRgb ( void ); 	/* Ingresa al submenú rgb */
menuEntry * doMemInfo ( void ); /* Información de consumo de stack */
menuEntry * doReset ( void ); 	/* Reinicia el equipo */

/* Volver al menú principal */
menuEntry * doBackMain ( void );

/* Opciones asociadas a Led1 */
menuEntry * doLed1On ( void ); 		/* Enciende el led 1 */
menuEntry * doLed1Off ( void );		/* Apaga el led 1 */
menuEntry * doLed1Toggle ( void ); 	/* Invierte el estado del led 1 */

/* Opciones asociadas a Led2 */
menuEntry * doLed2On ( void ); 		/* Enciende el led 2 */
menuEntry * doLed2Off ( void );		/* Apaga el led 2 */
menuEntry * doLed2Toggle ( void ); 	/* Invierte el estado del led 2 */

/* Opciones asociadas a Led3 */
menuEntry * doLed3On ( void ); 		/* Enciende el led 3 */
menuEntry * doLed3Off ( void );		/* Apaga el led 3 */
menuEntry * doLed3Toggle ( void ); 	/* Invierte el estado del led 3 */

/* Opciones asociadas a LedRgb */
menuEntry * doLedR		( void );
menuEntry * doLedG		( void );
menuEntry * doLedB		( void );
menuEntry * doLedAllOff	( void );
menuEntry * doBackRgb	( void );

/* Opciones asociadas a LedR */
menuEntry * doLedROn    ( void );
menuEntry * doLedROff  	( void );
menuEntry * doLedRToggle( void );
/* Opciones asociadas a LedG */
menuEntry * doLedGOn   	( void );
menuEntry * doLedGOff 	( void );
menuEntry * doLedGToggle( void );
/* Opciones asociadas a LedB */
menuEntry * doLedBOn   	( void );
menuEntry * doLedBOff 	( void );
menuEntry * doLedBToggle( void );





#endif /* INC_MENU_H_ */
