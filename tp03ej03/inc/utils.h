/** @addtogroup utils
   \brief Módulo que contiene utilidades para mostrar y oprera con números
*  @{
*  \file utils.h
*  \brief Funciones para operar y mostrar digitos en base 2 y en base 10.
*
*/


#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include <stdint.h>
#include <stdio.h>
#include <math.h>
/** \def UNA_CONSTANTE_SIMBOLICA
* \brief 	No sirve para nada... Quack!
*
* Un comentario sobre la variable simbólica
*/
#define UNA_CONSTANTE_SIMBOLICA  

/** \brief Calcula la cantidad de dígitos de una valor contenido en una variable de tipo entero de 32 bits.
* \param [in] valor Valor en el que se cuentan los digitos
* \return Cantidad de digitos
*/
uint8_t nDigitos(uint32_t valor);

/** \brief Retorna un valor conformado por los n dígitos más significativos de un valor entero, sin signo de 32 bits.
* \param [in] valor Valor de entrada
* \param [in] n Numero de digitos más significativos
* \return Los n digitos más significativos
*/
uint32_t nMasSigni(uint32_t valor, uint8_t n);

/** \brief Mostrar por pantalla la representación en base 2 de un valor entero, sin signo de 32 bits.
* /param [in] valor Valor a mostrar como binario.
*/
void mostrarBin(uint32_t valor);

/** \brief Retornar la representación ASCII del dígito (base 10) menos significativo de un valor entero de 32 bits, sin signo, dado como argumento.
* \param [in] valor Valor del que se obtiene el menos significativo
* \return El ASCII del digito menos significativo en base 10
*/
uint8_t menosSigni(uint32_t valor);

/** \brief Retorna el bit de la posición pasada como argunmento
* \param [in] valor Valor de entrada
* \param [in] pos Posición del bit
* \return El bit de la posición pasada como argumento
*/
uint8_t bitDePos(uint32_t valor, uint8_t pos);

/** \brief Retorna el digito en la posición N de un decimal
* \param [in] valor Valor de entrada
* \param [in] pos Posición del digito
* \return El digito de la posición pasada como argumento
*/
uint8_t digitoDecimalN(uint32_t valor, uint8_t pos);

/** \brief Muestra en la salida estandar un array de caracteres. El caracter de fin debe ser  '\0'
* \param [in] str Puntero a arreglo de caracteres finalizados con \0 
*/
void putStr(char* str);

/** \brief Muestra en la salida estandar los digitos de un decimal separados con espacios
* \param [in] valor Valor de entrada
*/
void mostrarDec(uint32_t valor);

/** \brief El puntero toASCII apuntará a un arreglo con los caracteres ASCII
* de un valor de uint32_t pasado como parámetro.
* \param [in] valor Valor de entrada
* \param [out] toASCII puntero a arreglo de 11 caracteres (10 maxima capacidad requerida para un dato de 32 bits + 1 caracter para el '\0')
*/
void uint32ToASCII(uint32_t valor, char* toASCII);

/** \brief Retorna la cantidad de digitos de un valor uint32_t. Se debe dar como parámetros un puntero a un arrreglo de 10 elementos (máximas cantidad de dítigos de un uint32_t)
* \param [in] valor Valor de entrada
* \param [out] digitos puntero a arreglo de 10 elementos uint8_t
* \return La cantidad de digitos (lugares ocupados en el arreglo)
*/
uint8_t digitosDec(uint32_t valor, uint8_t* digitos);


#endif /* INC_UTILS_H_*/

/** @}*/
