/*
 * sysCalls.c
 *
 *  Created on: Oct 19, 2018
 *      Author: quack
 */
#include "sysCalls.h"
#include "stdint.h"
#include <stdlib.h>
#include <errno.h>
#include <malloc.h>


# undef errno
extern int errno;
extern char * __bot_heap;
extern char * __top_heap;

int * _sbrk ( int incr )
{
	char * prev_heap_end ;
	static char * lastHeapAssigned = ( char *)&__bot_heap ;
	prev_heap_end = lastHeapAssigned ;
	lastHeapAssigned += incr ;
	if( lastHeapAssigned >= ( char *) (&__top_heap ))
	{
		lastHeapAssigned = prev_heap_end ;
		errno = ENOMEM ;
		//abort ();
	}
	return ( int *) prev_heap_end ;
}


/*
int _kill (int pid , int sig )
{
	errno = EINVAL ;
	return -1;
}
*/
