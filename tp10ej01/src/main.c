#include "stdint.h"
#include "hw.h"
#include "chip.h"
#include "sysCalls.h"
#include <stdlib.h>
#include "malloc.h"

#define CORE_M4

//#include " sysUtils.h"

int main ( void )
{
	systemInit( ) ;
	uint32_t * pVal1 ;
	uint32_t * pVal2 ;
	uint32_t * pVal3 ;
	uint32_t * pVal4 ;
	struct mallinfo m;
	m= mallinfo();
	pVal1 = malloc ( sizeof ( uint32_t ) ) ;
	m= mallinfo();
	pVal2 = malloc ( sizeof ( uint32_t ) ) ;
	m= mallinfo();
	pVal3 = malloc ( sizeof ( uint32_t ) ) ;
	m= mallinfo();
	pVal4 = malloc ( sizeof ( uint32_t ) ) ;
	m= mallinfo();
	*pVal1 = 0xAB;
	*pVal2 = 0xCD;
	*pVal3 = 0xEF ;
	*pVal4 = 0xFF;
	*pVal1 = *pVal4;
	m= mallinfo();
	free ( pVal1 ) ;
	m= mallinfo();
	free ( pVal2 ) ;
	m= mallinfo();
	free ( pVal3 ) ;
	m= mallinfo();
	free ( pVal4 ) ;
	m= mallinfo();

	while (1) ;
	return 0;

}
