#include <stdint.h>
#include <stdio.h>

// oDato es el dato que será reflejado y se guardará en rDato...

int main(int argc, char * argv[]){
	uint8_t oDato = 50;
	uint8_t rDato = 0;
	printf("Original: %d\n",oDato);
	for(uint8_t i=0;i<(8*sizeof(rDato)-1);i++){
		rDato+=(oDato%2);//adds the least significant bit from the origin to the reflected
		oDato/=2; //shifts oDato to right
		rDato*=2; //shifts rDato to left
	}
	printf("Reflejo: %d\n",rDato);
	return 0;
}
