#include <stdint.h>
#include "sysUtils.h"
#include "chip.h"
#include "sc_types.h"
#include "AllLeds.h"
//#include "timerTicks.h"
void allLedsIface_ledOn(const AllLeds* handle, const sc_integer led){
	ledOn(led);
}
void allLedsIface_ledOff(const AllLeds* handle, const sc_integer led){
	ledOff(led);
}

int main(void)
{
	AllLeds estados;
	sysInit();
	allLeds_init(&estados);
	allLeds_enter(&estados);
	while(1){
		__WFI();
	    if(getKeyPressed() != 0){
	    	allLedsIface_raise_keyPress(&estados);
	    	rstKeyPressed();
		}
	    allLeds_runCycle(&estados);
	  }
  return 0;
}
