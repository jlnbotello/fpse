
#ifndef ALLLEDS_H_
#define ALLLEDS_H_

#include "sc_types.h"
		
#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'AllLeds'.
*/

/*! Enumeration of all states */ 
typedef enum
{
	AllLeds_main_region_Led0,
	AllLeds_main_region_Led1,
	AllLeds_main_region_Led2,
	AllLeds_main_region_Led3,
	AllLeds_main_region_Wait,
	AllLeds_last_state
} AllLedsStates;

/*! Type definition of the data structure for the AllLedsIface interface scope. */
typedef struct
{
	sc_boolean keyPress_raised;
} AllLedsIface;


/*! Define dimension of the state configuration vector for orthogonal states. */
#define ALLLEDS_MAX_ORTHOGONAL_STATES 1

/*! 
 * Type definition of the data structure for the AllLeds state machine.
 * This data structure has to be allocated by the client code. 
 */
typedef struct
{
	AllLedsStates stateConfVector[ALLLEDS_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	AllLedsIface iface;
} AllLeds;

/*! Initializes the AllLeds state machine data structures. Must be called before first usage.*/
extern void allLeds_init(AllLeds* handle);

/*! Activates the state machine */
extern void allLeds_enter(AllLeds* handle);

/*! Deactivates the state machine */
extern void allLeds_exit(AllLeds* handle);

/*! Performs a 'run to completion' step. */
extern void allLeds_runCycle(AllLeds* handle);


/*! Raises the in event 'keyPress' that is defined in the default interface scope. */ 
extern void allLedsIface_raise_keyPress(AllLeds* handle);


/*!
 * Checks whether the state machine is active (until 2.4.1 this method was used for states).
 * A state machine is active if it was entered. It is inactive if it has not been entered at all or if it has been exited.
 */
extern sc_boolean allLeds_isActive(const AllLeds* handle);

/*!
 * Checks if all active states are final. 
 * If there are no active states then the state machine is considered being inactive. In this case this method returns false.
 */
extern sc_boolean allLeds_isFinal(const AllLeds* handle);

/*! Checks if the specified state is active (until 2.4.1 the used method for states was called isActive()). */
extern sc_boolean allLeds_isStateActive(const AllLeds* handle, AllLedsStates state);

#ifdef __cplusplus
}
#endif 

#endif /* ALLLEDS_H_ */
