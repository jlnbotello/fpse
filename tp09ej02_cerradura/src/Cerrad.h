
#ifndef CERRAD_H_
#define CERRAD_H_

#include "sc_types.h"
		
#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'cerrad'.
*/

/*! Enumeration of all states */ 
typedef enum
{
	Cerrad_main_region_start,
	Cerrad_main_region_locked,
	Cerrad_main_region_waitInput,
	Cerrad_main_region_unlocked,
	Cerrad_main_region_unlocked_TogGre5_LedOff,
	Cerrad_main_region_unlocked_TogGre5_LedOn,
	Cerrad_main_region_invalid,
	Cerrad_main_region_invalid_TogRed5_LedOff,
	Cerrad_main_region_invalid_TogRed5_LedOn,
	Cerrad_main_region_newPass,
	Cerrad_main_region_newPass_TogAll5_LedOff,
	Cerrad_main_region_newPass_TogAll5_LedOn,
	Cerrad_ledPressKey_led,
	Cerrad_ledPressKey_none,
	Cerrad_last_state
} CerradStates;

/*! Type definition of the data structure for the CerradIface interface scope. */
typedef struct
{
	sc_boolean keyPress_raised;
	sc_integer keyPress_value;
} CerradIface;

/*! Type definition of the data structure for the CerradInternal interface scope. */
typedef struct
{
	sc_integer pass;
	sc_integer input;
	sc_integer N;
	sc_integer cnt;
	sc_integer changePass;
} CerradInternal;

/*! Type definition of the data structure for the CerradTimeEvents interface scope. */
typedef struct
{
	sc_boolean cerrad_main_region_unlocked_TogGre5_LedOff_tev0_raised;
	sc_boolean cerrad_main_region_unlocked_TogGre5_LedOn_tev0_raised;
	sc_boolean cerrad_main_region_invalid_TogRed5_LedOff_tev0_raised;
	sc_boolean cerrad_main_region_invalid_TogRed5_LedOn_tev0_raised;
	sc_boolean cerrad_main_region_newPass_TogAll5_LedOff_tev0_raised;
	sc_boolean cerrad_main_region_newPass_TogAll5_LedOn_tev0_raised;
	sc_boolean cerrad_ledPressKey_led_tev0_raised;
} CerradTimeEvents;


/*! Define dimension of the state configuration vector for orthogonal states. */
#define CERRAD_MAX_ORTHOGONAL_STATES 2

/*! 
 * Type definition of the data structure for the Cerrad state machine.
 * This data structure has to be allocated by the client code. 
 */
typedef struct
{
	CerradStates stateConfVector[CERRAD_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	CerradIface iface;
	CerradInternal internal;
	CerradTimeEvents timeEvents;
} Cerrad;

/*! Initializes the Cerrad state machine data structures. Must be called before first usage.*/
extern void cerrad_init(Cerrad* handle);

/*! Activates the state machine */
extern void cerrad_enter(Cerrad* handle);

/*! Deactivates the state machine */
extern void cerrad_exit(Cerrad* handle);

/*! Performs a 'run to completion' step. */
extern void cerrad_runCycle(Cerrad* handle);

/*! Raises a time event. */
extern void cerrad_raiseTimeEvent(const Cerrad* handle, sc_eventid evid);

/*! Raises the in event 'keyPress' that is defined in the default interface scope. */ 
extern void cerradIface_raise_keyPress(Cerrad* handle, sc_integer value);


/*!
 * Checks whether the state machine is active (until 2.4.1 this method was used for states).
 * A state machine is active if it was entered. It is inactive if it has not been entered at all or if it has been exited.
 */
extern sc_boolean cerrad_isActive(const Cerrad* handle);

/*!
 * Checks if all active states are final. 
 * If there are no active states then the state machine is considered being inactive. In this case this method returns false.
 */
extern sc_boolean cerrad_isFinal(const Cerrad* handle);

/*! Checks if the specified state is active (until 2.4.1 the used method for states was called isActive()). */
extern sc_boolean cerrad_isStateActive(const Cerrad* handle, CerradStates state);

#ifdef __cplusplus
}
#endif 

#endif /* CERRAD_H_ */
