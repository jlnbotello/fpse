
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "Cerrad.h"
#include "CerradRequired.h"
/*! \file Implementation of the state machine 'cerrad'
*/

/* prototypes of all internal functions */
static sc_boolean cerrad_check_main_region_start_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_locked_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_waitInput_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_waitInput_tr1_tr1(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOff_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOff_tr1_tr1(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOn_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOff_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOff_tr1_tr1(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOn_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOff_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOff_tr1_tr1(const Cerrad* handle);
static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOn_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_ledPressKey_led_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_ledPressKey_none_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region__choice_0_tr0_tr0(const Cerrad* handle);
static sc_boolean cerrad_check_main_region__choice_0_tr2_tr2(const Cerrad* handle);
static sc_boolean cerrad_check_main_region__choice_0_tr1(const Cerrad* handle);
static void cerrad_effect_main_region_start_tr0(Cerrad* handle);
static void cerrad_effect_main_region_locked_tr0(Cerrad* handle);
static void cerrad_effect_main_region_waitInput_tr0(Cerrad* handle);
static void cerrad_effect_main_region_waitInput_tr1(Cerrad* handle);
static void cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr0(Cerrad* handle);
static void cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr1(Cerrad* handle);
static void cerrad_effect_main_region_unlocked_TogGre5_LedOn_tr0(Cerrad* handle);
static void cerrad_effect_main_region_invalid_TogRed5_LedOff_tr0(Cerrad* handle);
static void cerrad_effect_main_region_invalid_TogRed5_LedOff_tr1(Cerrad* handle);
static void cerrad_effect_main_region_invalid_TogRed5_LedOn_tr0(Cerrad* handle);
static void cerrad_effect_main_region_newPass_TogAll5_LedOff_tr0(Cerrad* handle);
static void cerrad_effect_main_region_newPass_TogAll5_LedOff_tr1(Cerrad* handle);
static void cerrad_effect_main_region_newPass_TogAll5_LedOn_tr0(Cerrad* handle);
static void cerrad_effect_ledPressKey_led_tr0(Cerrad* handle);
static void cerrad_effect_ledPressKey_none_tr0(Cerrad* handle);
static void cerrad_effect_main_region__choice_0_tr0(Cerrad* handle);
static void cerrad_effect_main_region__choice_0_tr2(Cerrad* handle);
static void cerrad_effect_main_region__choice_0_tr1(Cerrad* handle);
static void cerrad_enact_main_region_start(Cerrad* handle);
static void cerrad_enact_main_region_locked(Cerrad* handle);
static void cerrad_enact_main_region_waitInput(Cerrad* handle);
static void cerrad_enact_main_region_unlocked(Cerrad* handle);
static void cerrad_enact_main_region_unlocked_TogGre5_LedOff(Cerrad* handle);
static void cerrad_enact_main_region_unlocked_TogGre5_LedOn(Cerrad* handle);
static void cerrad_enact_main_region_invalid(Cerrad* handle);
static void cerrad_enact_main_region_invalid_TogRed5_LedOff(Cerrad* handle);
static void cerrad_enact_main_region_invalid_TogRed5_LedOn(Cerrad* handle);
static void cerrad_enact_main_region_newPass(Cerrad* handle);
static void cerrad_enact_main_region_newPass_TogAll5_LedOff(Cerrad* handle);
static void cerrad_enact_main_region_newPass_TogAll5_LedOn(Cerrad* handle);
static void cerrad_enact_ledPressKey_led(Cerrad* handle);
static void cerrad_exact_main_region_unlocked(Cerrad* handle);
static void cerrad_exact_main_region_unlocked_TogGre5_LedOff(Cerrad* handle);
static void cerrad_exact_main_region_unlocked_TogGre5_LedOn(Cerrad* handle);
static void cerrad_exact_main_region_invalid(Cerrad* handle);
static void cerrad_exact_main_region_invalid_TogRed5_LedOff(Cerrad* handle);
static void cerrad_exact_main_region_invalid_TogRed5_LedOn(Cerrad* handle);
static void cerrad_exact_main_region_newPass(Cerrad* handle);
static void cerrad_exact_main_region_newPass_TogAll5_LedOff(Cerrad* handle);
static void cerrad_exact_main_region_newPass_TogAll5_LedOn(Cerrad* handle);
static void cerrad_exact_ledPressKey_led(Cerrad* handle);
static void cerrad_enseq_main_region_start_default(Cerrad* handle);
static void cerrad_enseq_main_region_locked_default(Cerrad* handle);
static void cerrad_enseq_main_region_waitInput_default(Cerrad* handle);
static void cerrad_enseq_main_region_unlocked_default(Cerrad* handle);
static void cerrad_enseq_main_region_unlocked_TogGre5_LedOff_default(Cerrad* handle);
static void cerrad_enseq_main_region_unlocked_TogGre5_LedOn_default(Cerrad* handle);
static void cerrad_enseq_main_region_invalid_default(Cerrad* handle);
static void cerrad_enseq_main_region_invalid_TogRed5_LedOff_default(Cerrad* handle);
static void cerrad_enseq_main_region_invalid_TogRed5_LedOn_default(Cerrad* handle);
static void cerrad_enseq_main_region_newPass_default(Cerrad* handle);
static void cerrad_enseq_main_region_newPass_TogAll5_LedOff_default(Cerrad* handle);
static void cerrad_enseq_main_region_newPass_TogAll5_LedOn_default(Cerrad* handle);
static void cerrad_enseq_ledPressKey_led_default(Cerrad* handle);
static void cerrad_enseq_ledPressKey_none_default(Cerrad* handle);
static void cerrad_enseq_main_region_default(Cerrad* handle);
static void cerrad_enseq_main_region_unlocked_TogGre5_default(Cerrad* handle);
static void cerrad_enseq_main_region_invalid_TogRed5_default(Cerrad* handle);
static void cerrad_enseq_main_region_newPass_TogAll5_default(Cerrad* handle);
static void cerrad_enseq_ledPressKey_default(Cerrad* handle);
static void cerrad_exseq_main_region_start(Cerrad* handle);
static void cerrad_exseq_main_region_locked(Cerrad* handle);
static void cerrad_exseq_main_region_waitInput(Cerrad* handle);
static void cerrad_exseq_main_region_unlocked(Cerrad* handle);
static void cerrad_exseq_main_region_unlocked_TogGre5_LedOff(Cerrad* handle);
static void cerrad_exseq_main_region_unlocked_TogGre5_LedOn(Cerrad* handle);
static void cerrad_exseq_main_region_invalid(Cerrad* handle);
static void cerrad_exseq_main_region_invalid_TogRed5_LedOff(Cerrad* handle);
static void cerrad_exseq_main_region_invalid_TogRed5_LedOn(Cerrad* handle);
static void cerrad_exseq_main_region_newPass(Cerrad* handle);
static void cerrad_exseq_main_region_newPass_TogAll5_LedOff(Cerrad* handle);
static void cerrad_exseq_main_region_newPass_TogAll5_LedOn(Cerrad* handle);
static void cerrad_exseq_ledPressKey_led(Cerrad* handle);
static void cerrad_exseq_ledPressKey_none(Cerrad* handle);
static void cerrad_exseq_main_region(Cerrad* handle);
static void cerrad_exseq_main_region_unlocked_TogGre5(Cerrad* handle);
static void cerrad_exseq_main_region_invalid_TogRed5(Cerrad* handle);
static void cerrad_exseq_main_region_newPass_TogAll5(Cerrad* handle);
static void cerrad_exseq_ledPressKey(Cerrad* handle);
static void cerrad_react_main_region_start(Cerrad* handle);
static void cerrad_react_main_region_locked(Cerrad* handle);
static void cerrad_react_main_region_waitInput(Cerrad* handle);
static void cerrad_react_main_region_unlocked_TogGre5_LedOff(Cerrad* handle);
static void cerrad_react_main_region_unlocked_TogGre5_LedOn(Cerrad* handle);
static void cerrad_react_main_region_invalid_TogRed5_LedOff(Cerrad* handle);
static void cerrad_react_main_region_invalid_TogRed5_LedOn(Cerrad* handle);
static void cerrad_react_main_region_newPass_TogAll5_LedOff(Cerrad* handle);
static void cerrad_react_main_region_newPass_TogAll5_LedOn(Cerrad* handle);
static void cerrad_react_ledPressKey_led(Cerrad* handle);
static void cerrad_react_ledPressKey_none(Cerrad* handle);
static void cerrad_react_main_region__choice_0(Cerrad* handle);
static void cerrad_react_main_region__entry_Default(Cerrad* handle);
static void cerrad_react_main_region_unlocked_TogGre5__entry_Default(Cerrad* handle);
static void cerrad_react_main_region_invalid_TogRed5__entry_Default(Cerrad* handle);
static void cerrad_react_main_region_newPass_TogAll5__entry_Default(Cerrad* handle);
static void cerrad_react_ledPressKey__entry_Default(Cerrad* handle);
static void cerrad_clearInEvents(Cerrad* handle);
static void cerrad_clearOutEvents(Cerrad* handle);


void cerrad_init(Cerrad* handle)
{
	sc_integer i;

	for (i = 0; i < CERRAD_MAX_ORTHOGONAL_STATES; ++i)
	{
		handle->stateConfVector[i] = Cerrad_last_state;
	}
	
	
	handle->stateConfVectorPosition = 0;

	cerrad_clearInEvents(handle);
	cerrad_clearOutEvents(handle);

	/* Default init sequence for statechart cerrad */
	handle->internal.pass = 0;
	handle->internal.input = 0;
	handle->internal.N = 0;
	handle->internal.cnt = 0;
	handle->internal.changePass = 0;

}

void cerrad_enter(Cerrad* handle)
{
	/* Default enter sequence for statechart cerrad */
	cerrad_enseq_main_region_default(handle);
	cerrad_enseq_ledPressKey_default(handle);
}

void cerrad_exit(Cerrad* handle)
{
	/* Default exit sequence for statechart cerrad */
	cerrad_exseq_main_region(handle);
	cerrad_exseq_ledPressKey(handle);
}

sc_boolean cerrad_isActive(const Cerrad* handle)
{
	sc_boolean result;
	if (handle->stateConfVector[0] != Cerrad_last_state || handle->stateConfVector[1] != Cerrad_last_state)
	{
		result =  bool_true;
	}
	else
	{
		result = bool_false;
	}
	return result;
}

/* 
 * Always returns 'false' since this state machine can never become final.
 */
sc_boolean cerrad_isFinal(const Cerrad* handle)
{
   return bool_false;
}

static void cerrad_clearInEvents(Cerrad* handle)
{
	handle->iface.keyPress_raised = bool_false;
	handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOff_tev0_raised = bool_false;
	handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOn_tev0_raised = bool_false;
	handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOff_tev0_raised = bool_false;
	handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOn_tev0_raised = bool_false;
	handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOff_tev0_raised = bool_false;
	handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOn_tev0_raised = bool_false;
	handle->timeEvents.cerrad_ledPressKey_led_tev0_raised = bool_false;
}

static void cerrad_clearOutEvents(Cerrad* handle)
{
}

void cerrad_runCycle(Cerrad* handle)
{
	
	cerrad_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < CERRAD_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++)
		{
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition])
		{
		case Cerrad_main_region_start :
		{
			cerrad_react_main_region_start(handle);
			break;
		}
		case Cerrad_main_region_locked :
		{
			cerrad_react_main_region_locked(handle);
			break;
		}
		case Cerrad_main_region_waitInput :
		{
			cerrad_react_main_region_waitInput(handle);
			break;
		}
		case Cerrad_main_region_unlocked_TogGre5_LedOff :
		{
			cerrad_react_main_region_unlocked_TogGre5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_unlocked_TogGre5_LedOn :
		{
			cerrad_react_main_region_unlocked_TogGre5_LedOn(handle);
			break;
		}
		case Cerrad_main_region_invalid_TogRed5_LedOff :
		{
			cerrad_react_main_region_invalid_TogRed5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_invalid_TogRed5_LedOn :
		{
			cerrad_react_main_region_invalid_TogRed5_LedOn(handle);
			break;
		}
		case Cerrad_main_region_newPass_TogAll5_LedOff :
		{
			cerrad_react_main_region_newPass_TogAll5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_newPass_TogAll5_LedOn :
		{
			cerrad_react_main_region_newPass_TogAll5_LedOn(handle);
			break;
		}
		case Cerrad_ledPressKey_led :
		{
			cerrad_react_ledPressKey_led(handle);
			break;
		}
		case Cerrad_ledPressKey_none :
		{
			cerrad_react_ledPressKey_none(handle);
			break;
		}
		default:
			break;
		}
	}
	
	cerrad_clearInEvents(handle);
}

void cerrad_raiseTimeEvent(const Cerrad* handle, sc_eventid evid)
{
	if ( ((sc_intptr_t)evid) >= ((sc_intptr_t)&(handle->timeEvents))
		&&  ((sc_intptr_t)evid) < ((sc_intptr_t)&(handle->timeEvents)) + sizeof(CerradTimeEvents))
		{
		*(sc_boolean*)evid = bool_true;
	}		
}

sc_boolean cerrad_isStateActive(const Cerrad* handle, CerradStates state)
{
	sc_boolean result = bool_false;
	switch (state)
	{
		case Cerrad_main_region_start :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_start
			);
			break;
		case Cerrad_main_region_locked :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_locked
			);
			break;
		case Cerrad_main_region_waitInput :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_waitInput
			);
			break;
		case Cerrad_main_region_unlocked :
			result = (sc_boolean) (handle->stateConfVector[0] >= Cerrad_main_region_unlocked
				&& handle->stateConfVector[0] <= Cerrad_main_region_unlocked_TogGre5_LedOn);
			break;
		case Cerrad_main_region_unlocked_TogGre5_LedOff :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_unlocked_TogGre5_LedOff
			);
			break;
		case Cerrad_main_region_unlocked_TogGre5_LedOn :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_unlocked_TogGre5_LedOn
			);
			break;
		case Cerrad_main_region_invalid :
			result = (sc_boolean) (handle->stateConfVector[0] >= Cerrad_main_region_invalid
				&& handle->stateConfVector[0] <= Cerrad_main_region_invalid_TogRed5_LedOn);
			break;
		case Cerrad_main_region_invalid_TogRed5_LedOff :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_invalid_TogRed5_LedOff
			);
			break;
		case Cerrad_main_region_invalid_TogRed5_LedOn :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_invalid_TogRed5_LedOn
			);
			break;
		case Cerrad_main_region_newPass :
			result = (sc_boolean) (handle->stateConfVector[0] >= Cerrad_main_region_newPass
				&& handle->stateConfVector[0] <= Cerrad_main_region_newPass_TogAll5_LedOn);
			break;
		case Cerrad_main_region_newPass_TogAll5_LedOff :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_newPass_TogAll5_LedOff
			);
			break;
		case Cerrad_main_region_newPass_TogAll5_LedOn :
			result = (sc_boolean) (handle->stateConfVector[0] == Cerrad_main_region_newPass_TogAll5_LedOn
			);
			break;
		case Cerrad_ledPressKey_led :
			result = (sc_boolean) (handle->stateConfVector[1] == Cerrad_ledPressKey_led
			);
			break;
		case Cerrad_ledPressKey_none :
			result = (sc_boolean) (handle->stateConfVector[1] == Cerrad_ledPressKey_none
			);
			break;
		default:
			result = bool_false;
			break;
	}
	return result;
}

void cerradIface_raise_keyPress(Cerrad* handle, sc_integer value)
{
	handle->iface.keyPress_value = value;
	handle->iface.keyPress_raised = bool_true;
}



/* implementations of all internal functions */

static sc_boolean cerrad_check_main_region_start_tr0_tr0(const Cerrad* handle)
{
	return bool_true;
}

static sc_boolean cerrad_check_main_region_locked_tr0_tr0(const Cerrad* handle)
{
	return handle->iface.keyPress_raised;
}

static sc_boolean cerrad_check_main_region_waitInput_tr0_tr0(const Cerrad* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->internal.N != 0)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_waitInput_tr1_tr1(const Cerrad* handle)
{
	return (handle->internal.N == 0) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOff_tr0_tr0(const Cerrad* handle)
{
	return ((handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOff_tev0_raised) && (handle->internal.cnt < 5)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOff_tr1_tr1(const Cerrad* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->internal.cnt == 5)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_unlocked_TogGre5_LedOn_tr0_tr0(const Cerrad* handle)
{
	return handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOn_tev0_raised;
}

static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOff_tr0_tr0(const Cerrad* handle)
{
	return ((handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOff_tev0_raised) && (handle->internal.cnt < 5)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOff_tr1_tr1(const Cerrad* handle)
{
	return (handle->internal.cnt == 5) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_invalid_TogRed5_LedOn_tr0_tr0(const Cerrad* handle)
{
	return handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOn_tev0_raised;
}

static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOff_tr0_tr0(const Cerrad* handle)
{
	return ((handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOff_tev0_raised) && (handle->internal.cnt < 5)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOff_tr1_tr1(const Cerrad* handle)
{
	return (handle->internal.cnt == 5) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region_newPass_TogAll5_LedOn_tr0_tr0(const Cerrad* handle)
{
	return handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOn_tev0_raised;
}

static sc_boolean cerrad_check_ledPressKey_led_tr0_tr0(const Cerrad* handle)
{
	return handle->timeEvents.cerrad_ledPressKey_led_tev0_raised;
}

static sc_boolean cerrad_check_ledPressKey_none_tr0_tr0(const Cerrad* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value != 0)) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region__choice_0_tr0_tr0(const Cerrad* handle)
{
	return (handle->internal.input == handle->internal.pass) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region__choice_0_tr2_tr2(const Cerrad* handle)
{
	return (handle->internal.changePass == 1) ? bool_true : bool_false;
}

static sc_boolean cerrad_check_main_region__choice_0_tr1(const Cerrad* handle)
{
	return bool_true;
}

static void cerrad_effect_main_region_start_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_start(handle);
	cerrad_enseq_main_region_locked_default(handle);
}

static void cerrad_effect_main_region_locked_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_locked(handle);
	cerrad_enseq_main_region_waitInput_default(handle);
}

static void cerrad_effect_main_region_waitInput_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_waitInput(handle);
	cerrad_enseq_main_region_waitInput_default(handle);
}

static void cerrad_effect_main_region_waitInput_tr1(Cerrad* handle)
{
	cerrad_exseq_main_region_waitInput(handle);
	cerrad_react_main_region__choice_0(handle);
}

static void cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_unlocked_TogGre5_LedOff(handle);
	cerrad_enseq_main_region_unlocked_TogGre5_LedOn_default(handle);
}

static void cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr1(Cerrad* handle)
{
	cerrad_exseq_main_region_unlocked(handle);
	handle->internal.N = 6;
	cerrad_enseq_main_region_waitInput_default(handle);
}

static void cerrad_effect_main_region_unlocked_TogGre5_LedOn_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_unlocked_TogGre5_LedOn(handle);
	cerrad_enseq_main_region_unlocked_TogGre5_LedOff_default(handle);
}

static void cerrad_effect_main_region_invalid_TogRed5_LedOff_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_invalid_TogRed5_LedOff(handle);
	cerrad_enseq_main_region_invalid_TogRed5_LedOn_default(handle);
}

static void cerrad_effect_main_region_invalid_TogRed5_LedOff_tr1(Cerrad* handle)
{
	cerrad_exseq_main_region_invalid(handle);
	cerrad_enseq_main_region_locked_default(handle);
}

static void cerrad_effect_main_region_invalid_TogRed5_LedOn_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_invalid_TogRed5_LedOn(handle);
	cerrad_enseq_main_region_invalid_TogRed5_LedOff_default(handle);
}

static void cerrad_effect_main_region_newPass_TogAll5_LedOff_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_newPass_TogAll5_LedOff(handle);
	cerrad_enseq_main_region_newPass_TogAll5_LedOn_default(handle);
}

static void cerrad_effect_main_region_newPass_TogAll5_LedOff_tr1(Cerrad* handle)
{
	cerrad_exseq_main_region_newPass(handle);
	cerrad_enseq_main_region_locked_default(handle);
}

static void cerrad_effect_main_region_newPass_TogAll5_LedOn_tr0(Cerrad* handle)
{
	cerrad_exseq_main_region_newPass_TogAll5_LedOn(handle);
	cerrad_enseq_main_region_newPass_TogAll5_LedOff_default(handle);
}

static void cerrad_effect_ledPressKey_led_tr0(Cerrad* handle)
{
	cerrad_exseq_ledPressKey_led(handle);
	cerrad_enseq_ledPressKey_none_default(handle);
}

static void cerrad_effect_ledPressKey_none_tr0(Cerrad* handle)
{
	cerrad_exseq_ledPressKey_none(handle);
	cerrad_enseq_ledPressKey_led_default(handle);
}

static void cerrad_effect_main_region__choice_0_tr0(Cerrad* handle)
{
	cerrad_enseq_main_region_unlocked_default(handle);
}

static void cerrad_effect_main_region__choice_0_tr2(Cerrad* handle)
{
	cerrad_enseq_main_region_newPass_default(handle);
}

static void cerrad_effect_main_region__choice_0_tr1(Cerrad* handle)
{
	cerrad_enseq_main_region_invalid_default(handle);
}

/* Entry action for state 'start'. */
static void cerrad_enact_main_region_start(Cerrad* handle)
{
	/* Entry action for state 'start'. */
	handle->internal.pass = 123321;
	handle->internal.changePass = 0;
}

/* Entry action for state 'locked'. */
static void cerrad_enact_main_region_locked(Cerrad* handle)
{
	/* Entry action for state 'locked'. */
	handle->internal.input = 0;
	handle->internal.N = 6;
}

/* Entry action for state 'waitInput'. */
static void cerrad_enact_main_region_waitInput(Cerrad* handle)
{
	/* Entry action for state 'waitInput'. */
	handle->internal.N = handle->internal.N - 1;
	handle->internal.input = handle->internal.input * 10 + handle->iface.keyPress_value;
}

/* Entry action for state 'unlocked'. */
static void cerrad_enact_main_region_unlocked(Cerrad* handle)
{
	/* Entry action for state 'unlocked'. */
	handle->internal.cnt = 0;
}

/* Entry action for state 'LedOff'. */
static void cerrad_enact_main_region_unlocked_TogGre5_LedOff(Cerrad* handle)
{
	/* Entry action for state 'LedOff'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOff_tev0_raised) , 500, bool_false);
	cerradIface_ledOff(handle, 3);
}

/* Entry action for state 'LedOn'. */
static void cerrad_enact_main_region_unlocked_TogGre5_LedOn(Cerrad* handle)
{
	/* Entry action for state 'LedOn'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOn_tev0_raised) , 500, bool_false);
	handle->internal.cnt = handle->internal.cnt + 1;
	cerradIface_ledOn(handle, 3);
}

/* Entry action for state 'invalid'. */
static void cerrad_enact_main_region_invalid(Cerrad* handle)
{
	/* Entry action for state 'invalid'. */
	handle->internal.cnt = 0;
}

/* Entry action for state 'LedOff'. */
static void cerrad_enact_main_region_invalid_TogRed5_LedOff(Cerrad* handle)
{
	/* Entry action for state 'LedOff'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOff_tev0_raised) , 500, bool_false);
	cerradIface_ledOff(handle, 1);
}

/* Entry action for state 'LedOn'. */
static void cerrad_enact_main_region_invalid_TogRed5_LedOn(Cerrad* handle)
{
	/* Entry action for state 'LedOn'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOn_tev0_raised) , 500, bool_false);
	handle->internal.cnt = handle->internal.cnt + 1;
	cerradIface_ledOn(handle, 1);
}

/* Entry action for state 'newPass'. */
static void cerrad_enact_main_region_newPass(Cerrad* handle)
{
	/* Entry action for state 'newPass'. */
	handle->internal.changePass = 0;
	handle->internal.pass = handle->internal.input;
	handle->internal.cnt = 0;
}

/* Entry action for state 'LedOff'. */
static void cerrad_enact_main_region_newPass_TogAll5_LedOff(Cerrad* handle)
{
	/* Entry action for state 'LedOff'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOff_tev0_raised) , 500, bool_false);
	cerradIface_ledOff(handle, 0);
	cerradIface_ledOff(handle, 1);
	cerradIface_ledOff(handle, 2);
	cerradIface_ledOff(handle, 3);
}

/* Entry action for state 'LedOn'. */
static void cerrad_enact_main_region_newPass_TogAll5_LedOn(Cerrad* handle)
{
	/* Entry action for state 'LedOn'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOn_tev0_raised) , 500, bool_false);
	cerradIface_ledOn(handle, 0);
	cerradIface_ledOn(handle, 1);
	cerradIface_ledOn(handle, 2);
	cerradIface_ledOn(handle, 3);
	handle->internal.cnt = handle->internal.cnt + 1;
}

/* Entry action for state 'led'. */
static void cerrad_enact_ledPressKey_led(Cerrad* handle)
{
	/* Entry action for state 'led'. */
	cerrad_setTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_ledPressKey_led_tev0_raised) , 500, bool_false);
	cerradIface_ledOn(handle, handle->iface.keyPress_value - 1);
}

/* Exit action for state 'unlocked'. */
static void cerrad_exact_main_region_unlocked(Cerrad* handle)
{
	/* Exit action for state 'unlocked'. */
	cerradIface_ledOff(handle, 3);
	handle->internal.input = 0;
	handle->internal.changePass = 1;
}

/* Exit action for state 'LedOff'. */
static void cerrad_exact_main_region_unlocked_TogGre5_LedOff(Cerrad* handle)
{
	/* Exit action for state 'LedOff'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOff_tev0_raised) );		
}

/* Exit action for state 'LedOn'. */
static void cerrad_exact_main_region_unlocked_TogGre5_LedOn(Cerrad* handle)
{
	/* Exit action for state 'LedOn'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_unlocked_TogGre5_LedOn_tev0_raised) );		
}

/* Exit action for state 'invalid'. */
static void cerrad_exact_main_region_invalid(Cerrad* handle)
{
	/* Exit action for state 'invalid'. */
	cerradIface_ledOff(handle, 1);
}

/* Exit action for state 'LedOff'. */
static void cerrad_exact_main_region_invalid_TogRed5_LedOff(Cerrad* handle)
{
	/* Exit action for state 'LedOff'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOff_tev0_raised) );		
}

/* Exit action for state 'LedOn'. */
static void cerrad_exact_main_region_invalid_TogRed5_LedOn(Cerrad* handle)
{
	/* Exit action for state 'LedOn'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_invalid_TogRed5_LedOn_tev0_raised) );		
}

/* Exit action for state 'newPass'. */
static void cerrad_exact_main_region_newPass(Cerrad* handle)
{
	/* Exit action for state 'newPass'. */
	cerradIface_ledOff(handle, 0);
	cerradIface_ledOff(handle, 1);
	cerradIface_ledOff(handle, 2);
	cerradIface_ledOff(handle, 3);
}

/* Exit action for state 'LedOff'. */
static void cerrad_exact_main_region_newPass_TogAll5_LedOff(Cerrad* handle)
{
	/* Exit action for state 'LedOff'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOff_tev0_raised) );		
}

/* Exit action for state 'LedOn'. */
static void cerrad_exact_main_region_newPass_TogAll5_LedOn(Cerrad* handle)
{
	/* Exit action for state 'LedOn'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_main_region_newPass_TogAll5_LedOn_tev0_raised) );		
}

/* Exit action for state 'led'. */
static void cerrad_exact_ledPressKey_led(Cerrad* handle)
{
	/* Exit action for state 'led'. */
	cerrad_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.cerrad_ledPressKey_led_tev0_raised) );		
	cerradIface_ledOff(handle, handle->iface.keyPress_value - 1);
}

/* 'default' enter sequence for state start */
static void cerrad_enseq_main_region_start_default(Cerrad* handle)
{
	/* 'default' enter sequence for state start */
	cerrad_enact_main_region_start(handle);
	handle->stateConfVector[0] = Cerrad_main_region_start;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state locked */
static void cerrad_enseq_main_region_locked_default(Cerrad* handle)
{
	/* 'default' enter sequence for state locked */
	cerrad_enact_main_region_locked(handle);
	handle->stateConfVector[0] = Cerrad_main_region_locked;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state waitInput */
static void cerrad_enseq_main_region_waitInput_default(Cerrad* handle)
{
	/* 'default' enter sequence for state waitInput */
	cerrad_enact_main_region_waitInput(handle);
	handle->stateConfVector[0] = Cerrad_main_region_waitInput;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state unlocked */
static void cerrad_enseq_main_region_unlocked_default(Cerrad* handle)
{
	/* 'default' enter sequence for state unlocked */
	cerrad_enact_main_region_unlocked(handle);
	cerrad_enseq_main_region_unlocked_TogGre5_default(handle);
}

/* 'default' enter sequence for state LedOff */
static void cerrad_enseq_main_region_unlocked_TogGre5_LedOff_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOff */
	cerrad_enact_main_region_unlocked_TogGre5_LedOff(handle);
	handle->stateConfVector[0] = Cerrad_main_region_unlocked_TogGre5_LedOff;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state LedOn */
static void cerrad_enseq_main_region_unlocked_TogGre5_LedOn_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOn */
	cerrad_enact_main_region_unlocked_TogGre5_LedOn(handle);
	handle->stateConfVector[0] = Cerrad_main_region_unlocked_TogGre5_LedOn;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state invalid */
static void cerrad_enseq_main_region_invalid_default(Cerrad* handle)
{
	/* 'default' enter sequence for state invalid */
	cerrad_enact_main_region_invalid(handle);
	cerrad_enseq_main_region_invalid_TogRed5_default(handle);
}

/* 'default' enter sequence for state LedOff */
static void cerrad_enseq_main_region_invalid_TogRed5_LedOff_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOff */
	cerrad_enact_main_region_invalid_TogRed5_LedOff(handle);
	handle->stateConfVector[0] = Cerrad_main_region_invalid_TogRed5_LedOff;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state LedOn */
static void cerrad_enseq_main_region_invalid_TogRed5_LedOn_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOn */
	cerrad_enact_main_region_invalid_TogRed5_LedOn(handle);
	handle->stateConfVector[0] = Cerrad_main_region_invalid_TogRed5_LedOn;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state newPass */
static void cerrad_enseq_main_region_newPass_default(Cerrad* handle)
{
	/* 'default' enter sequence for state newPass */
	cerrad_enact_main_region_newPass(handle);
	cerrad_enseq_main_region_newPass_TogAll5_default(handle);
}

/* 'default' enter sequence for state LedOff */
static void cerrad_enseq_main_region_newPass_TogAll5_LedOff_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOff */
	cerrad_enact_main_region_newPass_TogAll5_LedOff(handle);
	handle->stateConfVector[0] = Cerrad_main_region_newPass_TogAll5_LedOff;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state LedOn */
static void cerrad_enseq_main_region_newPass_TogAll5_LedOn_default(Cerrad* handle)
{
	/* 'default' enter sequence for state LedOn */
	cerrad_enact_main_region_newPass_TogAll5_LedOn(handle);
	handle->stateConfVector[0] = Cerrad_main_region_newPass_TogAll5_LedOn;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state led */
static void cerrad_enseq_ledPressKey_led_default(Cerrad* handle)
{
	/* 'default' enter sequence for state led */
	cerrad_enact_ledPressKey_led(handle);
	handle->stateConfVector[1] = Cerrad_ledPressKey_led;
	handle->stateConfVectorPosition = 1;
}

/* 'default' enter sequence for state none */
static void cerrad_enseq_ledPressKey_none_default(Cerrad* handle)
{
	/* 'default' enter sequence for state none */
	handle->stateConfVector[1] = Cerrad_ledPressKey_none;
	handle->stateConfVectorPosition = 1;
}

/* 'default' enter sequence for region main region */
static void cerrad_enseq_main_region_default(Cerrad* handle)
{
	/* 'default' enter sequence for region main region */
	cerrad_react_main_region__entry_Default(handle);
}

/* 'default' enter sequence for region TogGre5 */
static void cerrad_enseq_main_region_unlocked_TogGre5_default(Cerrad* handle)
{
	/* 'default' enter sequence for region TogGre5 */
	cerrad_react_main_region_unlocked_TogGre5__entry_Default(handle);
}

/* 'default' enter sequence for region TogRed5 */
static void cerrad_enseq_main_region_invalid_TogRed5_default(Cerrad* handle)
{
	/* 'default' enter sequence for region TogRed5 */
	cerrad_react_main_region_invalid_TogRed5__entry_Default(handle);
}

/* 'default' enter sequence for region TogAll5 */
static void cerrad_enseq_main_region_newPass_TogAll5_default(Cerrad* handle)
{
	/* 'default' enter sequence for region TogAll5 */
	cerrad_react_main_region_newPass_TogAll5__entry_Default(handle);
}

/* 'default' enter sequence for region ledPressKey */
static void cerrad_enseq_ledPressKey_default(Cerrad* handle)
{
	/* 'default' enter sequence for region ledPressKey */
	cerrad_react_ledPressKey__entry_Default(handle);
}

/* Default exit sequence for state start */
static void cerrad_exseq_main_region_start(Cerrad* handle)
{
	/* Default exit sequence for state start */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state locked */
static void cerrad_exseq_main_region_locked(Cerrad* handle)
{
	/* Default exit sequence for state locked */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state waitInput */
static void cerrad_exseq_main_region_waitInput(Cerrad* handle)
{
	/* Default exit sequence for state waitInput */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state unlocked */
static void cerrad_exseq_main_region_unlocked(Cerrad* handle)
{
	/* Default exit sequence for state unlocked */
	cerrad_exseq_main_region_unlocked_TogGre5(handle);
	cerrad_exact_main_region_unlocked(handle);
}

/* Default exit sequence for state LedOff */
static void cerrad_exseq_main_region_unlocked_TogGre5_LedOff(Cerrad* handle)
{
	/* Default exit sequence for state LedOff */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_unlocked_TogGre5_LedOff(handle);
}

/* Default exit sequence for state LedOn */
static void cerrad_exseq_main_region_unlocked_TogGre5_LedOn(Cerrad* handle)
{
	/* Default exit sequence for state LedOn */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_unlocked_TogGre5_LedOn(handle);
}

/* Default exit sequence for state invalid */
static void cerrad_exseq_main_region_invalid(Cerrad* handle)
{
	/* Default exit sequence for state invalid */
	cerrad_exseq_main_region_invalid_TogRed5(handle);
	cerrad_exact_main_region_invalid(handle);
}

/* Default exit sequence for state LedOff */
static void cerrad_exseq_main_region_invalid_TogRed5_LedOff(Cerrad* handle)
{
	/* Default exit sequence for state LedOff */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_invalid_TogRed5_LedOff(handle);
}

/* Default exit sequence for state LedOn */
static void cerrad_exseq_main_region_invalid_TogRed5_LedOn(Cerrad* handle)
{
	/* Default exit sequence for state LedOn */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_invalid_TogRed5_LedOn(handle);
}

/* Default exit sequence for state newPass */
static void cerrad_exseq_main_region_newPass(Cerrad* handle)
{
	/* Default exit sequence for state newPass */
	cerrad_exseq_main_region_newPass_TogAll5(handle);
	cerrad_exact_main_region_newPass(handle);
}

/* Default exit sequence for state LedOff */
static void cerrad_exseq_main_region_newPass_TogAll5_LedOff(Cerrad* handle)
{
	/* Default exit sequence for state LedOff */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_newPass_TogAll5_LedOff(handle);
}

/* Default exit sequence for state LedOn */
static void cerrad_exseq_main_region_newPass_TogAll5_LedOn(Cerrad* handle)
{
	/* Default exit sequence for state LedOn */
	handle->stateConfVector[0] = Cerrad_last_state;
	handle->stateConfVectorPosition = 0;
	cerrad_exact_main_region_newPass_TogAll5_LedOn(handle);
}

/* Default exit sequence for state led */
static void cerrad_exseq_ledPressKey_led(Cerrad* handle)
{
	/* Default exit sequence for state led */
	handle->stateConfVector[1] = Cerrad_last_state;
	handle->stateConfVectorPosition = 1;
	cerrad_exact_ledPressKey_led(handle);
}

/* Default exit sequence for state none */
static void cerrad_exseq_ledPressKey_none(Cerrad* handle)
{
	/* Default exit sequence for state none */
	handle->stateConfVector[1] = Cerrad_last_state;
	handle->stateConfVectorPosition = 1;
}

/* Default exit sequence for region main region */
static void cerrad_exseq_main_region(Cerrad* handle)
{
	/* Default exit sequence for region main region */
	/* Handle exit of all possible states (of cerrad.main_region) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case Cerrad_main_region_start :
		{
			cerrad_exseq_main_region_start(handle);
			break;
		}
		case Cerrad_main_region_locked :
		{
			cerrad_exseq_main_region_locked(handle);
			break;
		}
		case Cerrad_main_region_waitInput :
		{
			cerrad_exseq_main_region_waitInput(handle);
			break;
		}
		case Cerrad_main_region_unlocked_TogGre5_LedOff :
		{
			cerrad_exseq_main_region_unlocked_TogGre5_LedOff(handle);
			cerrad_exact_main_region_unlocked(handle);
			break;
		}
		case Cerrad_main_region_unlocked_TogGre5_LedOn :
		{
			cerrad_exseq_main_region_unlocked_TogGre5_LedOn(handle);
			cerrad_exact_main_region_unlocked(handle);
			break;
		}
		case Cerrad_main_region_invalid_TogRed5_LedOff :
		{
			cerrad_exseq_main_region_invalid_TogRed5_LedOff(handle);
			cerrad_exact_main_region_invalid(handle);
			break;
		}
		case Cerrad_main_region_invalid_TogRed5_LedOn :
		{
			cerrad_exseq_main_region_invalid_TogRed5_LedOn(handle);
			cerrad_exact_main_region_invalid(handle);
			break;
		}
		case Cerrad_main_region_newPass_TogAll5_LedOff :
		{
			cerrad_exseq_main_region_newPass_TogAll5_LedOff(handle);
			cerrad_exact_main_region_newPass(handle);
			break;
		}
		case Cerrad_main_region_newPass_TogAll5_LedOn :
		{
			cerrad_exseq_main_region_newPass_TogAll5_LedOn(handle);
			cerrad_exact_main_region_newPass(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region TogGre5 */
static void cerrad_exseq_main_region_unlocked_TogGre5(Cerrad* handle)
{
	/* Default exit sequence for region TogGre5 */
	/* Handle exit of all possible states (of cerrad.main_region.unlocked.TogGre5) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case Cerrad_main_region_unlocked_TogGre5_LedOff :
		{
			cerrad_exseq_main_region_unlocked_TogGre5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_unlocked_TogGre5_LedOn :
		{
			cerrad_exseq_main_region_unlocked_TogGre5_LedOn(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region TogRed5 */
static void cerrad_exseq_main_region_invalid_TogRed5(Cerrad* handle)
{
	/* Default exit sequence for region TogRed5 */
	/* Handle exit of all possible states (of cerrad.main_region.invalid.TogRed5) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case Cerrad_main_region_invalid_TogRed5_LedOff :
		{
			cerrad_exseq_main_region_invalid_TogRed5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_invalid_TogRed5_LedOn :
		{
			cerrad_exseq_main_region_invalid_TogRed5_LedOn(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region TogAll5 */
static void cerrad_exseq_main_region_newPass_TogAll5(Cerrad* handle)
{
	/* Default exit sequence for region TogAll5 */
	/* Handle exit of all possible states (of cerrad.main_region.newPass.TogAll5) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case Cerrad_main_region_newPass_TogAll5_LedOff :
		{
			cerrad_exseq_main_region_newPass_TogAll5_LedOff(handle);
			break;
		}
		case Cerrad_main_region_newPass_TogAll5_LedOn :
		{
			cerrad_exseq_main_region_newPass_TogAll5_LedOn(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region ledPressKey */
static void cerrad_exseq_ledPressKey(Cerrad* handle)
{
	/* Default exit sequence for region ledPressKey */
	/* Handle exit of all possible states (of cerrad.ledPressKey) at position 1... */
	switch(handle->stateConfVector[ 1 ])
	{
		case Cerrad_ledPressKey_led :
		{
			cerrad_exseq_ledPressKey_led(handle);
			break;
		}
		case Cerrad_ledPressKey_none :
		{
			cerrad_exseq_ledPressKey_none(handle);
			break;
		}
		default: break;
	}
}

/* The reactions of state start. */
static void cerrad_react_main_region_start(Cerrad* handle)
{
	/* The reactions of state start. */
	cerrad_effect_main_region_start_tr0(handle);
}

/* The reactions of state locked. */
static void cerrad_react_main_region_locked(Cerrad* handle)
{
	/* The reactions of state locked. */
	if (cerrad_check_main_region_locked_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_locked_tr0(handle);
	} 
}

/* The reactions of state waitInput. */
static void cerrad_react_main_region_waitInput(Cerrad* handle)
{
	/* The reactions of state waitInput. */
	if (cerrad_check_main_region_waitInput_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_waitInput_tr0(handle);
	}  else
	{
		if (cerrad_check_main_region_waitInput_tr1_tr1(handle) == bool_true)
		{ 
			cerrad_effect_main_region_waitInput_tr1(handle);
		} 
	}
}

/* The reactions of state LedOff. */
static void cerrad_react_main_region_unlocked_TogGre5_LedOff(Cerrad* handle)
{
	/* The reactions of state LedOff. */
	if (cerrad_check_main_region_unlocked_TogGre5_LedOff_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr0(handle);
	}  else
	{
		if (cerrad_check_main_region_unlocked_TogGre5_LedOff_tr1_tr1(handle) == bool_true)
		{ 
			cerrad_effect_main_region_unlocked_TogGre5_LedOff_tr1(handle);
		} 
	}
}

/* The reactions of state LedOn. */
static void cerrad_react_main_region_unlocked_TogGre5_LedOn(Cerrad* handle)
{
	/* The reactions of state LedOn. */
	if (cerrad_check_main_region_unlocked_TogGre5_LedOn_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_unlocked_TogGre5_LedOn_tr0(handle);
	} 
}

/* The reactions of state LedOff. */
static void cerrad_react_main_region_invalid_TogRed5_LedOff(Cerrad* handle)
{
	/* The reactions of state LedOff. */
	if (cerrad_check_main_region_invalid_TogRed5_LedOff_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_invalid_TogRed5_LedOff_tr0(handle);
	}  else
	{
		if (cerrad_check_main_region_invalid_TogRed5_LedOff_tr1_tr1(handle) == bool_true)
		{ 
			cerrad_effect_main_region_invalid_TogRed5_LedOff_tr1(handle);
		} 
	}
}

/* The reactions of state LedOn. */
static void cerrad_react_main_region_invalid_TogRed5_LedOn(Cerrad* handle)
{
	/* The reactions of state LedOn. */
	if (cerrad_check_main_region_invalid_TogRed5_LedOn_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_invalid_TogRed5_LedOn_tr0(handle);
	} 
}

/* The reactions of state LedOff. */
static void cerrad_react_main_region_newPass_TogAll5_LedOff(Cerrad* handle)
{
	/* The reactions of state LedOff. */
	if (cerrad_check_main_region_newPass_TogAll5_LedOff_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_newPass_TogAll5_LedOff_tr0(handle);
	}  else
	{
		if (cerrad_check_main_region_newPass_TogAll5_LedOff_tr1_tr1(handle) == bool_true)
		{ 
			cerrad_effect_main_region_newPass_TogAll5_LedOff_tr1(handle);
		} 
	}
}

/* The reactions of state LedOn. */
static void cerrad_react_main_region_newPass_TogAll5_LedOn(Cerrad* handle)
{
	/* The reactions of state LedOn. */
	if (cerrad_check_main_region_newPass_TogAll5_LedOn_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region_newPass_TogAll5_LedOn_tr0(handle);
	} 
}

/* The reactions of state led. */
static void cerrad_react_ledPressKey_led(Cerrad* handle)
{
	/* The reactions of state led. */
	if (cerrad_check_ledPressKey_led_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_ledPressKey_led_tr0(handle);
	} 
}

/* The reactions of state none. */
static void cerrad_react_ledPressKey_none(Cerrad* handle)
{
	/* The reactions of state none. */
	if (cerrad_check_ledPressKey_none_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_ledPressKey_none_tr0(handle);
	} 
}

/* The reactions of state null. */
static void cerrad_react_main_region__choice_0(Cerrad* handle)
{
	/* The reactions of state null. */
	if (cerrad_check_main_region__choice_0_tr0_tr0(handle) == bool_true)
	{ 
		cerrad_effect_main_region__choice_0_tr0(handle);
	}  else
	{
		if (cerrad_check_main_region__choice_0_tr2_tr2(handle) == bool_true)
		{ 
			cerrad_effect_main_region__choice_0_tr2(handle);
		}  else
		{
			cerrad_effect_main_region__choice_0_tr1(handle);
		}
	}
}

/* Default react sequence for initial entry  */
static void cerrad_react_main_region__entry_Default(Cerrad* handle)
{
	/* Default react sequence for initial entry  */
	cerrad_enseq_main_region_start_default(handle);
}

/* Default react sequence for initial entry  */
static void cerrad_react_main_region_unlocked_TogGre5__entry_Default(Cerrad* handle)
{
	/* Default react sequence for initial entry  */
	cerrad_enseq_main_region_unlocked_TogGre5_LedOn_default(handle);
}

/* Default react sequence for initial entry  */
static void cerrad_react_main_region_invalid_TogRed5__entry_Default(Cerrad* handle)
{
	/* Default react sequence for initial entry  */
	cerrad_enseq_main_region_invalid_TogRed5_LedOn_default(handle);
}

/* Default react sequence for initial entry  */
static void cerrad_react_main_region_newPass_TogAll5__entry_Default(Cerrad* handle)
{
	/* Default react sequence for initial entry  */
	cerrad_enseq_main_region_newPass_TogAll5_LedOn_default(handle);
}

/* Default react sequence for initial entry  */
static void cerrad_react_ledPressKey__entry_Default(Cerrad* handle)
{
	/* Default react sequence for initial entry  */
	cerrad_enseq_ledPressKey_none_default(handle);
}


