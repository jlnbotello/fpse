#include <stdint.h>
#include "sysUtils.h"
#include "chip.h"
#include "sc_types.h"
#include "Cerrad.h"
#include "timerTicks.h"
#define NOF_TIMERS (sizeof(CerradTimeEvents)/sizeof(sc_boolean))

static TimerTicks ticks[NOF_TIMERS];

void cerrad_setTimer(Cerrad* handle, const sc_eventid evid, const sc_integer time_ms, const sc_boolean periodic)
{
	SetNewTimerTick(ticks, NOF_TIMERS, evid, time_ms, periodic);
}


void cerrad_unsetTimer(Cerrad* handle, const sc_eventid evid)
{
	UnsetTimerTick(ticks, NOF_TIMERS, evid);
}

void cerradIface_ledOn(const Cerrad* handle, const sc_integer n){
	ledOn(n);
}
void cerradIface_ledOff(const Cerrad* handle, const sc_integer n){
	ledOff(n);
}

int main(void)
{
	int i;
	Cerrad estados;
	sysInit();
	cerrad_init(&estados);
	cerrad_enter(&estados);
	while(1){
	    __WFI();
	    if(getSysTickEv())
	    {
	      rstSysTickEv();
	      UpdateTimers(ticks, NOF_TIMERS);
	      for (i = 0; i < NOF_TIMERS; i++)
	      {
	        if (IsPendEvent(ticks, NOF_TIMERS, ticks[i].evid))
	        {
	          cerrad_raiseTimeEvent(&estados, ticks[i].evid);
	          MarkAsAttEvent(ticks, NOF_TIMERS, ticks[i].evid);
	        }
	      }

		}
	    if(getKeyPressed() != 0)
	    {
	    	cerradIface_raise_keyPress(&estados, getKeyPressed());
	    	rstKeyPressed();
	    }
		cerrad_runCycle(&estados);
	  }
  return 0;
}






