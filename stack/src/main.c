#include "stdint.h"
#include "hw.h"
#include "chip.h"


#define CORE_M4

inline uint32_t stackSize ( void )
__attribute__ (( always_inline ));
inline uint32_t stackUsed ( void )
__attribute__ (( always_inline ));
extern uint8_t _sStack ;
extern uint8_t _eStack ;
int recursivo (int );

int main(void){

	systemInit ();
	printBlocking(" Prueba de stack \n\r");
	printBlocking (" Tamaño del stack : ");
	mostrarASCII(stackUsed());
	printBlocking ("\n\r");
	recursivo (10);

	while (1)
	return 0;
}


inline uint32_t stackSize ( void ){
	return & _sStack - & _eStack ;
}
inline uint32_t stackUsed ( void ){
	return & _sStack - ( uint8_t *) __get_MSP ();
}
int recursivo ( int v)
{
	int ret;
	uint32_t stackAddr = stackUsed ();
	char txt [20];
	printBlocking (" Tamaño del stack : ");
	mostrarASCII(stackAddr);
	printBlocking ("\n\r");
	if (v == 0)
		ret = 1;
	else
		ret = recursivo (v - 1);
	printBlocking (" Tamaño del stack : ");
	mostrarASCII(stackAddr);
	printBlocking ("\n\r");

	return ret ;
}
