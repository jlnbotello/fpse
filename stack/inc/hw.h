#ifndef INC_HW_H_
#define INC_HW_H_

#include <chip.h>
#include <stopwatch.h>
#include <stdint.h>
#include <lpc_types.h>
#include <string.h>
#include "fpu_init.h"


#define PORT_PIN_LED1  0x02  /**< Puerto del microcontrolador asociado al led1 */
#define PIN_LED1       0x0A  /**< Pin del puerto 2 asociado al led 1  */
#define PORT_PIN_LED2  0x02  /**< Puerto del microcontrolador asociado al led2 */
#define PIN_LED2       0x0B  /**< Pin del puerto 2 asociado al led 2  */
#define PORT_PIN_LED3  0x02  /**< Puerto del microcontrolador asociado al led3 */
#define PIN_LED3       0x0C  /**< Pin del puerto 2 asociado al led 3  */

#define PORT_PIN_RGB   0x02  /**< Puerto del microcontrolador asociado al led RGB. Si bien utiliza 3 pines, están todos en el mismo puerto */
#define PIN_RGB_RED    0x00  /**< Pin del puerto 2 asociado al color rojo del led RGB   */
#define PIN_RGB_GRN    0x01  /**< Pin del puerto 2 asociado al color verde del led RGB   */
#define PIN_RGB_BLU    0x02  /**< Pin del puerto 2 asociado al color azul del led RGB   */

#define GPIO_PORT_LED1 0x00  /**< Puerto del módulo GPIO asociado al led 1 */
#define GPIO_PIN_LED1  0x0E  /**< Identificación del pin asociado al led 1 del módulo GPIO */
#define GPIO_PORT_LED2 0x01  /**< Puerto del módulo GPIO asociado al led 2 */
#define GPIO_PIN_LED2  0x0B  /**< Identificación del pin asociado al led 2 del módulo GPIO */
#define GPIO_PORT_LED3 0x01  /**< Puerto del módulo GPIO asociado al led 3 */
#define GPIO_PIN_LED3  0x0C  /**< Identificación del pin asociado al led 3 del módulo GPIO */

#define GPIO_PORT_RGB  0x05  /**< Puerto del módulo GPIO asociado al led RGB */
#define GPIO_PIN_RED   0x00  /**< Identificación del pin asociado al color rojo del LED RGB del módulo GPIO */
#define GPIO_PIN_GRN   0x01  /**< Identificación del pin asociado al color verde del LED RGB del módulo GPIO */
#define GPIO_PIN_BLU   0x02  /**< Identificación del pin asociado al color azul del LED RGB del módulo GPIO */

#define PORT_PIN_KEY1  0x01  /**< Puerto del microcontrolador asociado al pulsador 1 */
#define PIN_KEY1       0x00  /**< Pin del puerto asociado al pulsador 1  */
#define PORT_PIN_KEY2  0x01  /**< Puerto del microcontrolador asociado al pulsador 2 */
#define PIN_KEY2       0x01  /**< Pin del puerto asociado al pulsador 2  */
#define PORT_PIN_KEY3  0x01  /**< Puerto del microcontrolador asociado al pulsador 3 */
#define PIN_KEY3       0x02  /**< Pin del puerto asociado al pulsador 3  */
#define PORT_PIN_KEY4  0x01  /**< Puerto del microcontrolador asociado al pulsador 4 */
#define PIN_KEY4       0x06  /**< Pin del puerto asociado al pulsador 4  */

#define GPIO_PORT_KEY1 0x00  /**< Puerto del módulo GPIO asociado al pulsador 1 */
#define GPIO_PIN_KEY1  0x04  /**< Identificación del pin asociado al pulsador 1 del módulo GPIO */
#define GPIO_PORT_KEY2 0x00  /**< Puerto del módulo GPIO asociado al pulsador 2 */
#define GPIO_PIN_KEY2  0x08  /**< Identificación del pin asociado al pulsador 2 del módulo GPIO */
#define GPIO_PORT_KEY3 0x00  /**< Puerto del módulo GPIO asociado al pulsador 3 */
#define GPIO_PIN_KEY3  0x09  /**< Identificación del pin asociado al pulsador 3 del módulo GPIO */
#define GPIO_PORT_KEY4 0x01  /**< Puerto del módulo GPIO asociado al pulsador 4 */
#define GPIO_PIN_KEY4  0x09  /**< Identificación del pin asociado al pulsador 4 del módulo GPIO */

#define LED0	0
#define LED1 	1
#define LED2 	2
#define LED3 	3
#define LEDR	4
#define LEDG	5
#define LEDB	0
#define ALL		6

#define DECRECIENTE	0
#define CRECIENTE 	1

#define TECLA1	0
#define TECLA2 	1
#define TECLA3	2
#define TECLA4 	3

void systemInit(void);
void ledsInit(void);
void keysInit(void);
void uartInit(void);
void pinsIRQInit();

void UART2_IRQHandler (void);
void GPIO0_IRQHandler(void);
void GPIO1_IRQHandler(void);
void GPIO2_IRQHandler(void);
void GPIO3_IRQHandler(void);
void SysTick_Handler ( void );

unsigned int serialWrite(const uint8_t *data, unsigned int dataLen);

unsigned int serialRead(uint8_t *data, unsigned int maxData);
//numerados segund la serigrafia. led 0 = rgb azul. led 4 = todos
void led(uint8_t led, _Bool setting);

void toggleLed(uint8_t led);

void destellar(uint8_t led,uint8_t N,uint32_t ms );

void putChr(char ch);

unsigned int print(char *txt);

void displayCounter(uint8_t v);

void mostrarASCII(uint32_t valor);

void ledZigZag();

#define delayMs(x) StopWatch_DelayMs(x)

#define delayUs(x) StopWatch_DelayUs(x)

#define ledOn(x)  led(x,true)

#define ledOff(x) led(x,false)

void printBlocking(char * cadena);

#endif /* INC_HW_H_ */
