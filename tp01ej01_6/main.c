#include <stdint.h>
#include <stdio.h>

// oDato es el dato que será reflejado y se guardará en rDato.
// Aquí se cambia un la variable de 8 bits sin signo por una de 32 con signo y se observa el comportamiento.

int main(int argc, char * argv[]){
	int32_t oDato1 =  50;
	int32_t oDato2 = -50;
	int32_t rDato1 = 0;
	int32_t rDato2 = 0;
	printf("Original 1: %d\n",oDato1);
	printf("Original 2: %d\n",oDato2);
	for(uint8_t i=0;i<(8*sizeof(int32_t)-1);i++){
		rDato1+=(oDato1%2);//adds the least significant bit from the origin to the reflected
		oDato1/=2; //shifts oDato to right
		rDato1*=2; //shifts rDato to left
		rDato2+=(oDato2%2);//adds the least significant bit from the origin to the reflected
		oDato2/=2; //shifts oDato to right
		rDato2*=2; //shifts rDato to left
	}
	printf("Reflejo 1: %d\n",rDato1);
	printf("Reflejo 2: %d\n",rDato2);
	return 0;
}

// The system is little-endian. To check "lscpu" command on terminal
