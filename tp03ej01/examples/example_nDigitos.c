/*! \example example_nDigitos.c
* \brief Ejemplo de uso de la función nDigitos
*/
int main(int argc, char * argv[]){
	//Ejemplo de uso de nDigitos
	uint8_t N= nDigitos(98012);
	//Se muestra por salida estandar
	putStr("Numero de digitos: ");
	uint32ToASCII(N, valorASCII);
	putStr(valorASCII);
}

