/** \addtogroup Principal
 *  \brief Aplicación para manipular número en base 2 y en base 10.
 *  \date 26/10/2018
 *  Aplicación desarrollada en: \image html logo_ingenieria.png
 *  \cite chaudhary2014effective
 *  @{
 */


/** \file main.c
 *  \brief Archivo de entrada de la aplicación.
 *
 * Contiene la funcion \ref main.c
 */

#include "utils.h"


int main(int argc, char * argv[]){
	uint32_t valor=0;
	char valorASCII[11];
	putStr("Ingrese un valor de prueba: ");
	scanf("%u", &valor);
	// Cantidad de ditigos
	uint8_t N= nDigitos(valor);
	putStr("Tiene ");
	uint32ToASCII(N, valorASCII);
	putStr(valorASCII);
	putStr(" digitos\n");
}

/*! @} final addtogroup */
