/*
 * sysCalls.h
 *
 *  Created on: Oct 19, 2018
 *      Author: quack
 */

#ifndef INC_SYSCALLS_H_
#define INC_SYSCALLS_H_

int * _sbrk ( int incr );

/* Es invocada cuando la aplicación debe finalizar */
void _exit (int errCode);

/* Transfiere el control a un nuevo proceso */
int _execve ( char *name , char ** argv , char ** env );

/* Crea un nuevo proceso*/
int _fork ( void );

/* Retorna un valor entero que identifica el proceso desde el que
 * se realiza la invocación a la función */
int _getpid ( void );

/*  Envía una se~nal (sig) a un proceso determinado (pid) para detenerlo*/
int _kill (int pid , int sig );

/* Espera por el cambio de estado en un proceso hijo del proceso actual*/
int _wait ( int * status );

/* Retorna información sobre un archivo abierto y asociado al argumento file */
//int _fstat(int file, struct stat *st);

/* Retorna información sobre un archivo abierto con nombre file */
//int _stat(char *file, struct stat *st);

/* Comprueba si el argumento (un identificador de archivo) esta asociado a una terminal */
int _isatty ( int file );

/* Establece un nuevo nombre para un archivo existente */
int _link(char *old, char *new);

/* Elimina una entrada del sistema de archivos indicada a traves de su nombre name */
int _unlink(char *name);

/* Abre el archivo indicado a traves del argumento name segun el modo  */
int _open ( const char *name , int flags , int mode );


int _lseek ( int file , int offset , int whence );
/*
 * lectura y escritura sobre los identificadores dados como argumento (file).
 *  ptr representa el lugar donde se deberan almacenar len bytes leıdos
 *  desde el flujo en el caso de la funcion read y donde almacenar la
 *  informacion en el caso de write
 */
int _read(int file, char *ptr, int len);
int _write(int file, char *ptr, int len);

/* Cierra un archivo asociado al identificador recibido como argumento */
int _close(int file);

/* Retorna informacion de tiempo asociada al proceso actual, completando
 * los campos de la estructura buf*/
//int _times ( struct tms * buf );

/*requerida si desde la aplicacion principal se invoca a la funcion time*/
//int _gettimeofday ( struct timeval *tv , struct timezone *tz);
#endif /* INC_SYSCALLS_H_ */

