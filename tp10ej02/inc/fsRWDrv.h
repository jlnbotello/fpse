/*
 * fsRWDrv.h
 *
 *  Created on: Oct 20, 2018
 *      Author: quack
 */

#ifndef INC_FSRWDRV_H_
#define INC_FSRWDRV_H_

# include <stdint.h>
uint8_t * getFsStartAddr ( void );
uint8_t * getFsEndAddr ( void );
uint32_t getFsSize ( void );



#endif /* INC_FSRWDRV_H_ */
