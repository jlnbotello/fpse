/*!
 * \mainpage TP 03 - Ejercicio 2
 * \section genDesc Descripción general
 * Ejerccio basado en ejercicio 5 del TP 2, que a su vez se basa en ejercicio 1 del TP 1.
 * \section perData Datos Personales
 * \author Julián Botello, 
 * Estudiante de Bioingeniería
 * \section changelog Registro de cambios
 *
 * |   Fecha    | Descripción                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/10/2018 | Version 0.1 del documento                      |
 *
 */

/*! 
 * \file main.c
 * \brief Reflexión de una palabra de ancho fijo
 *
 * Aplicación que refleja la variable \ref oDato en la variable \ref rDato
 */

#include <stdint.h>
#include <stdio.h>
// DOS FORMAS DE DOCUMENTAR UNA VARIABLE GLOBAL. LOCALES NO SE PUEDE
/*! \var uint8_t oDato
 *  \brief Dato que será reflejado 
 */
uint8_t oDato = 50; 
uint8_t rDato = 0; /*!<Dato reflejado */


/*! \fn int main(int argc, char * argv[]) 
* \brief Función principal de la aplicación
*
* Refleja una palabra de ancho fijo. Muestra primero la palabra original y luego la palabra reflejada
*
* \param [in] argc Número de argumento pasados dentro de argv 
* \param [in] argv[] Puntero a arreglo de arreglo de caracteres
* \return [out] Retorna __0__ cuando finaliza correctamente
*/
int main(int argc, char * argv[]) ;



int main(int argc, char * argv[]){
	
	printf("Original: %d\n",oDato);
	for(uint8_t i=0;i<(8*sizeof(rDato)-1);i++){
		rDato+=(oDato%2);//adds the least significant bit from the oDato to rDato
		oDato/=2; //desplaza oDato a la derecha 1 bit. Podría realizar con operador de bits >>1
		rDato*=2; //desplaza rDato a la izquierda 1 bit. Podría realizar con operador de bits <<1
	}
	printf("Reflejo: %d\n",rDato);
	return 0;
}
