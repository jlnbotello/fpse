#include "stdint.h"
#include "hw.h"
#include "chip.h"
#include "sysCalls.h"
#include <stdlib.h>
#include "malloc.h"
#include <stdio.h>

#define CORE_M4

//#include " sysUtils.h"

int main ( void )
{
	int op;
	float op1 , op2 , r;
	systemInit();
	printf ("\r\n\t -= Calculadora =-\t\r\n");
	while (1){
		printf (" Indique la operación:\r\n");
		printf ("1 - Suma \r\n");
		printf ("2 - Resta \r\n");
		printf ("3 - Multiplicación\r\n");
		printf ("4 - División\r\n");
		while ( scanf ("%d", &op) != 1)
			fflush ( stdin );
		if(op >= 1 && op <= 4)
		{
			printf (" Operador 1:");
			scanf ("%f", & op1 );
			printf (" Operador 2:");
			scanf ("%f", & op2 );
			switch (op)
			{
			case 1:
				printf (" Suma : %f\r\n", op1 + op2 );
				break ;
			case 2:
				printf (" Resta : %f\r\n", op1 - op2 );
				break ;
			case 3:
				printf (" Multip .: %f\r\n", op1 * op2 );
				break ;
			case 4:
				printf ("Div .: %f\r\n", op1 / op2 );
				break ;
			default :
				printf (" Error !\r\n");
			}
		}
	}
	return 0;

}
