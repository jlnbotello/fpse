################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lpc/src/adc_18xx_43xx.c \
../lpc/src/aes_18xx_43xx.c \
../lpc/src/atimer_18xx_43xx.c \
../lpc/src/ccan_18xx_43xx.c \
../lpc/src/chip_18xx_43xx.c \
../lpc/src/clock_18xx_43xx.c \
../lpc/src/dac_18xx_43xx.c \
../lpc/src/eeprom.c \
../lpc/src/eeprom_18xx_43xx.c \
../lpc/src/emc_18xx_43xx.c \
../lpc/src/enet_18xx_43xx.c \
../lpc/src/evrt_18xx_43xx.c \
../lpc/src/fpu_init.c \
../lpc/src/gpdma_18xx_43xx.c \
../lpc/src/gpio_18xx_43xx.c \
../lpc/src/gpiogroup_18xx_43xx.c \
../lpc/src/hsadc_18xx_43xx.c \
../lpc/src/i2c_18xx_43xx.c \
../lpc/src/i2cm_18xx_43xx.c \
../lpc/src/i2s_18xx_43xx.c \
../lpc/src/iap_18xx_43xx.c \
../lpc/src/lcd_18xx_43xx.c \
../lpc/src/otp_18xx_43xx.c \
../lpc/src/pinint_18xx_43xx.c \
../lpc/src/pmc_18xx_43xx.c \
../lpc/src/ring_buffer.c \
../lpc/src/ritimer_18xx_43xx.c \
../lpc/src/rtc_18xx_43xx.c \
../lpc/src/rtc_ut.c \
../lpc/src/sct_18xx_43xx.c \
../lpc/src/sct_pwm_18xx_43xx.c \
../lpc/src/sdif_18xx_43xx.c \
../lpc/src/sdio_18xx_43xx.c \
../lpc/src/sdmmc_18xx_43xx.c \
../lpc/src/spi_18xx_43xx.c \
../lpc/src/ssp_18xx_43xx.c \
../lpc/src/stopwatch_18xx_43xx.c \
../lpc/src/sysinit_18xx_43xx.c \
../lpc/src/timer_18xx_43xx.c \
../lpc/src/uart_18xx_43xx.c \
../lpc/src/wwdt_18xx_43xx.c 

OBJS += \
./lpc/src/adc_18xx_43xx.o \
./lpc/src/aes_18xx_43xx.o \
./lpc/src/atimer_18xx_43xx.o \
./lpc/src/ccan_18xx_43xx.o \
./lpc/src/chip_18xx_43xx.o \
./lpc/src/clock_18xx_43xx.o \
./lpc/src/dac_18xx_43xx.o \
./lpc/src/eeprom.o \
./lpc/src/eeprom_18xx_43xx.o \
./lpc/src/emc_18xx_43xx.o \
./lpc/src/enet_18xx_43xx.o \
./lpc/src/evrt_18xx_43xx.o \
./lpc/src/fpu_init.o \
./lpc/src/gpdma_18xx_43xx.o \
./lpc/src/gpio_18xx_43xx.o \
./lpc/src/gpiogroup_18xx_43xx.o \
./lpc/src/hsadc_18xx_43xx.o \
./lpc/src/i2c_18xx_43xx.o \
./lpc/src/i2cm_18xx_43xx.o \
./lpc/src/i2s_18xx_43xx.o \
./lpc/src/iap_18xx_43xx.o \
./lpc/src/lcd_18xx_43xx.o \
./lpc/src/otp_18xx_43xx.o \
./lpc/src/pinint_18xx_43xx.o \
./lpc/src/pmc_18xx_43xx.o \
./lpc/src/ring_buffer.o \
./lpc/src/ritimer_18xx_43xx.o \
./lpc/src/rtc_18xx_43xx.o \
./lpc/src/rtc_ut.o \
./lpc/src/sct_18xx_43xx.o \
./lpc/src/sct_pwm_18xx_43xx.o \
./lpc/src/sdif_18xx_43xx.o \
./lpc/src/sdio_18xx_43xx.o \
./lpc/src/sdmmc_18xx_43xx.o \
./lpc/src/spi_18xx_43xx.o \
./lpc/src/ssp_18xx_43xx.o \
./lpc/src/stopwatch_18xx_43xx.o \
./lpc/src/sysinit_18xx_43xx.o \
./lpc/src/timer_18xx_43xx.o \
./lpc/src/uart_18xx_43xx.o \
./lpc/src/wwdt_18xx_43xx.o 

C_DEPS += \
./lpc/src/adc_18xx_43xx.d \
./lpc/src/aes_18xx_43xx.d \
./lpc/src/atimer_18xx_43xx.d \
./lpc/src/ccan_18xx_43xx.d \
./lpc/src/chip_18xx_43xx.d \
./lpc/src/clock_18xx_43xx.d \
./lpc/src/dac_18xx_43xx.d \
./lpc/src/eeprom.d \
./lpc/src/eeprom_18xx_43xx.d \
./lpc/src/emc_18xx_43xx.d \
./lpc/src/enet_18xx_43xx.d \
./lpc/src/evrt_18xx_43xx.d \
./lpc/src/fpu_init.d \
./lpc/src/gpdma_18xx_43xx.d \
./lpc/src/gpio_18xx_43xx.d \
./lpc/src/gpiogroup_18xx_43xx.d \
./lpc/src/hsadc_18xx_43xx.d \
./lpc/src/i2c_18xx_43xx.d \
./lpc/src/i2cm_18xx_43xx.d \
./lpc/src/i2s_18xx_43xx.d \
./lpc/src/iap_18xx_43xx.d \
./lpc/src/lcd_18xx_43xx.d \
./lpc/src/otp_18xx_43xx.d \
./lpc/src/pinint_18xx_43xx.d \
./lpc/src/pmc_18xx_43xx.d \
./lpc/src/ring_buffer.d \
./lpc/src/ritimer_18xx_43xx.d \
./lpc/src/rtc_18xx_43xx.d \
./lpc/src/rtc_ut.d \
./lpc/src/sct_18xx_43xx.d \
./lpc/src/sct_pwm_18xx_43xx.d \
./lpc/src/sdif_18xx_43xx.d \
./lpc/src/sdio_18xx_43xx.d \
./lpc/src/sdmmc_18xx_43xx.d \
./lpc/src/spi_18xx_43xx.d \
./lpc/src/ssp_18xx_43xx.d \
./lpc/src/stopwatch_18xx_43xx.d \
./lpc/src/sysinit_18xx_43xx.d \
./lpc/src/timer_18xx_43xx.d \
./lpc/src/uart_18xx_43xx.d \
./lpc/src/wwdt_18xx_43xx.d 


# Each subdirectory must supply rules for building sources it contributes
lpc/src/%.o: ../lpc/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O2  -g -I/home/quack/opt/gcc-arm-none-eabi-7-2018-q2-update/arm-none-eabi/include -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


