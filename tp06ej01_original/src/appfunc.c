/*
 * appfunc.c
 *
 *  Created on: Sep 28, 2018
 *      Author: quack
 */
#include "appfunc.h"
#include "hw.h"
#include "stdint.h"


uint8_t* obtenerVector(){
	static uint8_t sec[10]={0};
	// 0 1 2 3 0 1 2 3 0 1
	for(uint8_t i=0;i<10;i++){
		sec[i]=i%4;
	};
	return sec;
}
void ledZigZag(){
	static uint8_t i=0;
	static uint8_t dir=CRECIENTE;
	led(ALL,FALSE);
	led(i,TRUE);

	if(i==0)
		dir=CRECIENTE;
	if(i==3)
		dir=DECRECIENTE;

	if(dir==CRECIENTE)
		i++;
	if(dir==DECRECIENTE)
		i--;

}


