/*
 * hw.c
 *
 *  Created on: Sep 15, 2018
 *      Author: quack
 */
#include "hw.h"
#include <lpc_types.h>
#include "core_cm4.h"

const uint32_t ExtRateIn = 0;  /**< Frecuencia de clock externa inyectada al microcontrolador. No utilizado en las CIAA. */
const uint32_t OscRateIn = 12000000; /**< Frecuencia del oscilador externo incorporado en la CIAA-NXP. */

#define BUFFSize 512 /**< Cantidad máxima de valores a alojar en el buffer de entrada y en el de salida*/
static unsigned char rxBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de entrada */
static unsigned char txBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de salida */

static RINGBUFF_T txRing; /**< Estructura asociado al buffer circular de transmisión. */
static RINGBUFF_T rxRing; /**< Estructura asociada al buffer circular de recepción.  */


void systemInit(void)
{
	Chip_SetupXtalClocking();
	SystemCoreClockUpdate();
    fpuInit();
    // LEDS MUX
	Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);
	//GPIO INIT
	Chip_GPIO_Init(LPC_GPIO_PORT);
	//LEDS DIR: OUTPUT
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);
	//LEDS INITIAL STATUS: OFF
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED2, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED3, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU, FALSE);
	//KEYS MUX
	Chip_SCU_PinMux(PORT_PIN_KEY1, PIN_KEY1, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY2, PIN_KEY2, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY3, PIN_KEY3, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY4, PIN_KEY4, MD_EZI|MD_ZI, FUNC0);
	//KEYS DIR: INPUT
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY1, GPIO_PIN_KEY1);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY2, GPIO_PIN_KEY2);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY3, GPIO_PIN_KEY3);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY4, GPIO_PIN_KEY4);
	//MUX UART
	Chip_SCU_PinMux(7, 1, MD_PDN, FUNC6);
	Chip_SCU_PinMux(7, 2, MD_PLN|MD_EZI|MD_ZI, FUNC6);
	//UART CONFIG
	Chip_UART_Init(LPC_USART2);
    Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
    Chip_UART_SetBaud(LPC_USART2, 115200);
    Chip_UART_SetupFIFOS(LPC_USART2, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
    //RINGBUFFER
    RingBuffer_Init(&rxRing, rxBuff, 1, BUFFSize);
    RingBuffer_Init(&txRing, txBuff, 1, BUFFSize);
    //UART INTERRUP
    Chip_UART_IntEnable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT));
    NVIC_EnableIRQ(USART2_IRQn);
    Chip_UART_TXEnable(LPC_USART2);
    //STOPWATCH
    StopWatch_Init();
}

void UART2_IRQHandler (void)
{
  Chip_UART_IRQRBHandler(LPC_USART2, &rxRing, &txRing);
}

unsigned int serialWrite(const uint8_t *data, unsigned int dataLen)
{
  unsigned int transmited = 0;
  unsigned int toInsert;
  unsigned int freeSpc;
  while(transmited < dataLen)
  {
    freeSpc = RingBuffer_GetFree(&txRing);
    while(!freeSpc)
      freeSpc = RingBuffer_GetFree(&txRing);
    toInsert = ((freeSpc > dataLen) ? dataLen : freeSpc);
    Chip_UART_SendRB(LPC_USART2, &txRing, &data[transmited], toInsert);
    transmited += toInsert;
  }
  return transmited;
}

unsigned int serialRead(uint8_t *data, unsigned int maxData)
{
  return Chip_UART_ReadRB(LPC_USART2, &rxRing, data, maxData);
}

void toggleLed(uint8_t led){

	switch (led) {
		case 0:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU);
			break;
		case 1:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1);
			break;
		case 2:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2);
			break;
		case 3:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3);
			break;
		case 4:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3);
			break;
		default:
			break;
	}
}

//numerados segund la serigrafia. led 0 = rgb azul. led 4 = todos
void led(uint8_t led, bool setting){

	switch (led) {
		case 0:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU,setting);
			break;
		case 1:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1,setting);
			break;
		case 2:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2,setting);
			break;
		case 3:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3,setting);
			break;
		case 4:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3,setting);
			break;
		default:
			break;
	}
}

void displayCounter(uint8_t v){
	uint8_t bit0=v%2;
	uint8_t bit1=(v>>1)%2;
	uint8_t bit2=(v>>2)%2;
	uint8_t bit3=(v>>3)%2;
	led(LED3,bit0);
	led(LED2,bit1);
	led(LED1,bit2);
	led(LED0,bit3);
}

void putChr(char ch){
	Chip_UART_SendBlocking ( LPC_USART2 ,&ch , 1);
}

void print(char *txt){
	serialWrite(txt,strlen(txt));
}

void GPIO0_IRQHandler(void){
	SysTick->CTRL = SysTick->CTRL | (SysTick_CTRL_ENABLE_Msk);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));
}

void GPIO1_IRQHandler(void){
	SysTick->CTRL = SysTick->CTRL | (SysTick_CTRL_ENABLE_Msk);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(1));
}

void GPIO2_IRQHandler(void){
	SysTick->CTRL = SysTick->CTRL & (~SysTick_CTRL_ENABLE_Msk);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(2));
}

void GPIO3_IRQHandler(void){
	SysTick->CTRL = SysTick->CTRL & (~SysTick_CTRL_ENABLE_Msk);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(3));
}





void initPinInt(){
	Chip_PININT_Init ( LPC_GPIO_PIN_INT );
	Chip_SCU_GPIOIntPinSel (0, GPIO_PORT_KEY1, GPIO_PIN_KEY1);
	Chip_SCU_GPIOIntPinSel (1, GPIO_PORT_KEY2, GPIO_PIN_KEY2);
	Chip_SCU_GPIOIntPinSel (2, GPIO_PORT_KEY3, GPIO_PIN_KEY3);
	Chip_SCU_GPIOIntPinSel (3, GPIO_PORT_KEY4, GPIO_PIN_KEY4);

	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
	Chip_PININT_SetPinModeEdge (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
	Chip_PININT_EnableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
	NVIC_ClearPendingIRQ( PIN_INT0_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT1_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT2_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT3_IRQn );
	NVIC_EnableIRQ( PIN_INT0_IRQn);
	NVIC_EnableIRQ( PIN_INT1_IRQn);
	NVIC_EnableIRQ( PIN_INT2_IRQn);
	NVIC_EnableIRQ( PIN_INT3_IRQn);
}

uint8_t digitoBase10(uint32_t valor, uint8_t pos){
	uint8_t dig=0;
	uint32_t divisor=pow(10,pos);
	if(pos<9){
		divisor*=10;
		uint32_t resto = valor%divisor;
		divisor/=10;
		dig=resto/divisor;
	}
	else{
	dig=valor/divisor;
	}
	return dig;
}

uint8_t numASCII(uint8_t num){
	uint8_t char0 = '0';
	return char0+num;
}

uint8_t cantDigitos(uint32_t v){
	uint8_t cant=1;
	while(v>=10){
		v/=10;
		cant++;
	}
	return cant;
}

void mostrarASCII(uint32_t valor){
	uint8_t total = cantDigitos(valor);
	uint8_t num = 0;
	for(int8_t i=total-1;i>=0;i--){
		num = digitoBase10(valor,i);
		putChr(numASCII(num));
	}
	putChr('\n');
	putChr('\r');
}

void destellar(uint8_t led,uint8_t N,uint32_t ms ){
	for(uint8_t i=0;i<2*N;i++){
		toggleLed(led);
		StopWatch_DelayMs(ms);
	}
}

void SysTick_Handler ( void ){
	static uint32_t cnt = 0;
	cnt ++;
	cnt = cnt % 1000;
	if (cnt%125 == 0)
		SecuenciaInicial();

}

void registroDeCuentas(uint8_t tecla){
	static uint32_t contador=0;
	static bool habilitado=TRUE;
	switch (tecla) {
		case 1:
			if(habilitado==TRUE)
				contador++;
			break;
		case 2:
			habilitado=!habilitado;
			break;
		case 3:
			if(habilitado==TRUE)
				contador--;
			break;
		case 4:
			contador=0;
			break;
		default:
			break;
	}
	mostrarASCII(contador);
}

