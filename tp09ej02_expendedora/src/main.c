#include <stdint.h>
#include "sysUtils.h"
#include "chip.h"
#include "sc_types.h"
#include "timerTicks.h"
#include "Expen.h"
#define NOF_TIMERS (sizeof(ExpenTimeEvents)/sizeof(sc_boolean))

static TimerTicks ticks[NOF_TIMERS];

void expen_setTimer(Expen* handle, const sc_eventid evid, const sc_integer time_ms, const sc_boolean periodic)
{
	SetNewTimerTick(ticks, NOF_TIMERS, evid, time_ms, periodic);
}


void expen_unsetTimer(Expen* handle, const sc_eventid evid)
{
	UnsetTimerTick(ticks, NOF_TIMERS, evid);
}

void expenIface_ledOn(const Expen* handle, const sc_integer n){
	ledOn(n);
}
void expenIface_ledOff(const Expen* handle, const sc_integer n){
	ledOff(n);
}

int main(void)

{
	int i;
	ledOn(3);
	Expen estados;
	sysInit();
	expen_init(&estados);
	expen_enter(&estados);
	while(1){
	    __WFI();
	    if(getSysTickEv())
	    {
	      rstSysTickEv();
	      UpdateTimers(ticks, NOF_TIMERS);
	      for (i = 0; i < NOF_TIMERS; i++)
	      {
	        if (IsPendEvent(ticks, NOF_TIMERS, ticks[i].evid))
	        {
	        	expen_raiseTimeEvent(&estados, ticks[i].evid);
	          MarkAsAttEvent(ticks, NOF_TIMERS, ticks[i].evid);
	        }
	      }

		}
	    if(getKeyPressed() != 0)
	    {
	    	expenIface_raise_keyPress(&estados, getKeyPressed());
	    	rstKeyPressed();
	    }
	    expen_runCycle(&estados);
	  }
  return 0;
}






