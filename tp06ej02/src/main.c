#include "stdint.h"

#include "../inc/pila.h"
#include "hw.h"
#include "utils.h"

#define CORE_M4
#define BUF_LEN	10
#define DAT_SIZ	1

int main(void){;
	systemInit();
	pilaData *pd;
	uint8_t buffer[BUF_LEN*DAT_SIZ];
	pilaInit (pd, buffer , BUF_LEN , DAT_SIZ);
	obtenerDigitos(98012,pd);
	while(!pilaEmpty(pd)){
		uint8_t aux;
		pilaPop(pd,&aux);
		putChr(numASCII(aux));
	}
	while(1){};
	return 0;

}
