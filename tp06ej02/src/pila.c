#include "hw.h"
#include "stdint.h"

#include "../inc/pila.h"


void pilaInit ( pilaData *p,void * buffer , uint8_t bufLen , uint8_t dataSize){
	p->buf=buffer; // debo castearlo?
	p->dataSize=dataSize;
	p->dataCount=bufLen;
	p->pos=0;
}

void pilaPush ( pilaData *p, void * data ){
	if(p->pos<(p->dataSize)*(p->dataCount)){
		uint8_t *pData=(uint8_t *) data;
		for(uint8_t i=0;i<(p->dataSize);i++){
			p->buf[p->pos+i]=pData[i];
		}
		p->pos+=p->dataSize;
	}
}

void pilaPop ( pilaData *p, void * data ){
	if(p->pos>0){
		uint8_t *pData=(uint8_t *) data;
		p->pos-=p->dataSize;
		for(uint8_t i=0;i<(p->dataSize);i++){
			pData[i]=p->buf[p->pos+i];
		}
	}
}

uint8_t pilaEmpty ( pilaData *p){
	if(p->pos==0){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

uint8_t pilaFull ( pilaData *p){
	if(p->pos==(p->dataSize)*(p->dataCount)){
		return TRUE;
	}
	else{
		return FALSE;
	}
}



