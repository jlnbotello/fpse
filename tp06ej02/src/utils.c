#include "utils.h"
#include "lpc_types.h"


uint8_t digitoBase10(uint32_t valor, uint8_t pos){
	uint8_t dig=0;
	uint32_t divisor=pow(10,pos);
	if(pos<9){
		divisor*=10;
		uint32_t resto = valor%divisor;
		divisor/=10;
		dig=resto/divisor;
	}
	else{
	dig=valor/divisor;
	}
	return dig;
}

uint8_t numASCII(uint8_t num){
	uint8_t char0 = '0';
	return char0+num;
}

uint8_t cantDigitos(uint32_t v){
	uint8_t cant=1;
	while(v>=10){
		v/=10;
		cant++;
	}
	return cant;
}

void registroDeCuentas(uint8_t tecla){
	static uint32_t contador=0;
	static bool habilitado=TRUE;
	switch (tecla) {
		case 1:
			if(habilitado==TRUE)
				contador--;
			break;
		case 2:
				contador++;
			break;
		case 3:
			if(habilitado==TRUE)
				habilitado=!habilitado;
			break;
		case 4:
			contador=0;
			break;
		default:
			break;
	}
	mostrarASCII(contador);
}

void obtenerDigitos(uint32_t v,pilaData *pilaData){
	while(v>0){
		uint8_t aux=v%10;
		v/=10;
		if(!pilaFull(pilaData))
			pilaPush (pilaData, &aux);
	}
}


