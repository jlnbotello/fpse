
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "AllLeds.h"
#include "AllLedsRequired.h"
/*! \file Implementation of the state machine 'AllLeds'
*/

/* prototypes of all internal functions */
static sc_boolean allLeds_check_main_region_fordward_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_fordward_r1_Led0_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_fordward_r1_Led1_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_fordward_r1_Led2_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_fordward_r1_Led3_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_backward_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_backward_r1_Led3_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_backward_r1_Led2_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_backward_r1_Led1_tr0_tr0(const AllLeds* handle);
static sc_boolean allLeds_check_main_region_backward_r1_Led0_tr0_tr0(const AllLeds* handle);
static void allLeds_effect_main_region_fordward_tr0(AllLeds* handle);
static void allLeds_effect_main_region_fordward_r1_Led0_tr0(AllLeds* handle);
static void allLeds_effect_main_region_fordward_r1_Led1_tr0(AllLeds* handle);
static void allLeds_effect_main_region_fordward_r1_Led2_tr0(AllLeds* handle);
static void allLeds_effect_main_region_fordward_r1_Led3_tr0(AllLeds* handle);
static void allLeds_effect_main_region_backward_tr0(AllLeds* handle);
static void allLeds_effect_main_region_backward_r1_Led3_tr0(AllLeds* handle);
static void allLeds_effect_main_region_backward_r1_Led2_tr0(AllLeds* handle);
static void allLeds_effect_main_region_backward_r1_Led1_tr0(AllLeds* handle);
static void allLeds_effect_main_region_backward_r1_Led0_tr0(AllLeds* handle);
static void allLeds_enact_main_region_fordward_r1_Led0(AllLeds* handle);
static void allLeds_enact_main_region_fordward_r1_Led1(AllLeds* handle);
static void allLeds_enact_main_region_fordward_r1_Led2(AllLeds* handle);
static void allLeds_enact_main_region_fordward_r1_Led3(AllLeds* handle);
static void allLeds_enact_main_region_backward_r1_Led3(AllLeds* handle);
static void allLeds_enact_main_region_backward_r1_Led2(AllLeds* handle);
static void allLeds_enact_main_region_backward_r1_Led1(AllLeds* handle);
static void allLeds_enact_main_region_backward_r1_Led0(AllLeds* handle);
static void allLeds_exact_main_region_fordward_r1_Led0(AllLeds* handle);
static void allLeds_exact_main_region_fordward_r1_Led1(AllLeds* handle);
static void allLeds_exact_main_region_fordward_r1_Led2(AllLeds* handle);
static void allLeds_exact_main_region_fordward_r1_Led3(AllLeds* handle);
static void allLeds_exact_main_region_backward_r1_Led3(AllLeds* handle);
static void allLeds_exact_main_region_backward_r1_Led2(AllLeds* handle);
static void allLeds_exact_main_region_backward_r1_Led1(AllLeds* handle);
static void allLeds_exact_main_region_backward_r1_Led0(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_default(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_r1_Led0_default(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_r1_Led1_default(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_r1_Led2_default(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_r1_Led3_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_r1_Led3_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_r1_Led2_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_r1_Led1_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_r1_Led0_default(AllLeds* handle);
static void allLeds_enseq_main_region_default(AllLeds* handle);
static void allLeds_enseq_main_region_fordward_r1_default(AllLeds* handle);
static void allLeds_enseq_main_region_backward_r1_default(AllLeds* handle);
static void allLeds_exseq_main_region_fordward(AllLeds* handle);
static void allLeds_exseq_main_region_fordward_r1_Led0(AllLeds* handle);
static void allLeds_exseq_main_region_fordward_r1_Led1(AllLeds* handle);
static void allLeds_exseq_main_region_fordward_r1_Led2(AllLeds* handle);
static void allLeds_exseq_main_region_fordward_r1_Led3(AllLeds* handle);
static void allLeds_exseq_main_region_backward(AllLeds* handle);
static void allLeds_exseq_main_region_backward_r1_Led3(AllLeds* handle);
static void allLeds_exseq_main_region_backward_r1_Led2(AllLeds* handle);
static void allLeds_exseq_main_region_backward_r1_Led1(AllLeds* handle);
static void allLeds_exseq_main_region_backward_r1_Led0(AllLeds* handle);
static void allLeds_exseq_main_region(AllLeds* handle);
static void allLeds_exseq_main_region_fordward_r1(AllLeds* handle);
static void allLeds_exseq_main_region_backward_r1(AllLeds* handle);
static void allLeds_react_main_region_fordward_r1_Led0(AllLeds* handle);
static void allLeds_react_main_region_fordward_r1_Led1(AllLeds* handle);
static void allLeds_react_main_region_fordward_r1_Led2(AllLeds* handle);
static void allLeds_react_main_region_fordward_r1_Led3(AllLeds* handle);
static void allLeds_react_main_region_backward_r1_Led3(AllLeds* handle);
static void allLeds_react_main_region_backward_r1_Led2(AllLeds* handle);
static void allLeds_react_main_region_backward_r1_Led1(AllLeds* handle);
static void allLeds_react_main_region_backward_r1_Led0(AllLeds* handle);
static void allLeds_react_main_region_fordward_r1__entry_Default(AllLeds* handle);
static void allLeds_react_main_region_backward_r1__entry_Default(AllLeds* handle);
static void allLeds_react_main_region__entry_Default(AllLeds* handle);
static void allLeds_clearInEvents(AllLeds* handle);
static void allLeds_clearOutEvents(AllLeds* handle);


void allLeds_init(AllLeds* handle)
{
	sc_integer i;

	for (i = 0; i < ALLLEDS_MAX_ORTHOGONAL_STATES; ++i)
	{
		handle->stateConfVector[i] = AllLeds_last_state;
	}
	
	
	handle->stateConfVectorPosition = 0;

	allLeds_clearInEvents(handle);
	allLeds_clearOutEvents(handle);


}

void allLeds_enter(AllLeds* handle)
{
	/* Default enter sequence for statechart AllLeds */
	allLeds_enseq_main_region_default(handle);
}

void allLeds_exit(AllLeds* handle)
{
	/* Default exit sequence for statechart AllLeds */
	allLeds_exseq_main_region(handle);
}

sc_boolean allLeds_isActive(const AllLeds* handle)
{
	sc_boolean result;
	if (handle->stateConfVector[0] != AllLeds_last_state)
	{
		result =  bool_true;
	}
	else
	{
		result = bool_false;
	}
	return result;
}

/* 
 * Always returns 'false' since this state machine can never become final.
 */
sc_boolean allLeds_isFinal(const AllLeds* handle)
{
   return bool_false;
}

static void allLeds_clearInEvents(AllLeds* handle)
{
	handle->iface.keyPress_raised = bool_false;
	handle->timeEvents.allLeds_main_region_fordward_r1_Led0_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_fordward_r1_Led1_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_fordward_r1_Led2_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_fordward_r1_Led3_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_backward_r1_Led3_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_backward_r1_Led2_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_backward_r1_Led1_tev0_raised = bool_false;
	handle->timeEvents.allLeds_main_region_backward_r1_Led0_tev0_raised = bool_false;
}

static void allLeds_clearOutEvents(AllLeds* handle)
{
}

void allLeds_runCycle(AllLeds* handle)
{
	
	allLeds_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < ALLLEDS_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++)
		{
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition])
		{
		case AllLeds_main_region_fordward_r1_Led0 :
		{
			allLeds_react_main_region_fordward_r1_Led0(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led1 :
		{
			allLeds_react_main_region_fordward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led2 :
		{
			allLeds_react_main_region_fordward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led3 :
		{
			allLeds_react_main_region_fordward_r1_Led3(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led3 :
		{
			allLeds_react_main_region_backward_r1_Led3(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led2 :
		{
			allLeds_react_main_region_backward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led1 :
		{
			allLeds_react_main_region_backward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led0 :
		{
			allLeds_react_main_region_backward_r1_Led0(handle);
			break;
		}
		default:
			break;
		}
	}
	
	allLeds_clearInEvents(handle);
}

void allLeds_raiseTimeEvent(const AllLeds* handle, sc_eventid evid)
{
	if ( ((sc_intptr_t)evid) >= ((sc_intptr_t)&(handle->timeEvents))
		&&  ((sc_intptr_t)evid) < ((sc_intptr_t)&(handle->timeEvents)) + sizeof(AllLedsTimeEvents))
		{
		*(sc_boolean*)evid = bool_true;
	}		
}

sc_boolean allLeds_isStateActive(const AllLeds* handle, AllLedsStates state)
{
	sc_boolean result = bool_false;
	switch (state)
	{
		case AllLeds_main_region_fordward :
			result = (sc_boolean) (handle->stateConfVector[0] >= AllLeds_main_region_fordward
				&& handle->stateConfVector[0] <= AllLeds_main_region_fordward_r1_Led3);
			break;
		case AllLeds_main_region_fordward_r1_Led0 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_fordward_r1_Led0
			);
			break;
		case AllLeds_main_region_fordward_r1_Led1 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_fordward_r1_Led1
			);
			break;
		case AllLeds_main_region_fordward_r1_Led2 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_fordward_r1_Led2
			);
			break;
		case AllLeds_main_region_fordward_r1_Led3 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_fordward_r1_Led3
			);
			break;
		case AllLeds_main_region_backward :
			result = (sc_boolean) (handle->stateConfVector[0] >= AllLeds_main_region_backward
				&& handle->stateConfVector[0] <= AllLeds_main_region_backward_r1_Led0);
			break;
		case AllLeds_main_region_backward_r1_Led3 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_backward_r1_Led3
			);
			break;
		case AllLeds_main_region_backward_r1_Led2 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_backward_r1_Led2
			);
			break;
		case AllLeds_main_region_backward_r1_Led1 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_backward_r1_Led1
			);
			break;
		case AllLeds_main_region_backward_r1_Led0 :
			result = (sc_boolean) (handle->stateConfVector[0] == AllLeds_main_region_backward_r1_Led0
			);
			break;
		default:
			result = bool_false;
			break;
	}
	return result;
}

void allLedsIface_raise_keyPress(AllLeds* handle, sc_integer value)
{
	handle->iface.keyPress_value = value;
	handle->iface.keyPress_raised = bool_true;
}



/* implementations of all internal functions */

static sc_boolean allLeds_check_main_region_fordward_tr0_tr0(const AllLeds* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value == 4)) ? bool_true : bool_false;
}

static sc_boolean allLeds_check_main_region_fordward_r1_Led0_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_fordward_r1_Led0_tev0_raised;
}

static sc_boolean allLeds_check_main_region_fordward_r1_Led1_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_fordward_r1_Led1_tev0_raised;
}

static sc_boolean allLeds_check_main_region_fordward_r1_Led2_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_fordward_r1_Led2_tev0_raised;
}

static sc_boolean allLeds_check_main_region_fordward_r1_Led3_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_fordward_r1_Led3_tev0_raised;
}

static sc_boolean allLeds_check_main_region_backward_tr0_tr0(const AllLeds* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value == 4)) ? bool_true : bool_false;
}

static sc_boolean allLeds_check_main_region_backward_r1_Led3_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_backward_r1_Led3_tev0_raised;
}

static sc_boolean allLeds_check_main_region_backward_r1_Led2_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_backward_r1_Led2_tev0_raised;
}

static sc_boolean allLeds_check_main_region_backward_r1_Led1_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_backward_r1_Led1_tev0_raised;
}

static sc_boolean allLeds_check_main_region_backward_r1_Led0_tr0_tr0(const AllLeds* handle)
{
	return handle->timeEvents.allLeds_main_region_backward_r1_Led0_tev0_raised;
}

static void allLeds_effect_main_region_fordward_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_fordward(handle);
	allLeds_enseq_main_region_backward_default(handle);
}

static void allLeds_effect_main_region_fordward_r1_Led0_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_fordward_r1_Led0(handle);
	allLeds_enseq_main_region_fordward_r1_Led1_default(handle);
}

static void allLeds_effect_main_region_fordward_r1_Led1_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_fordward_r1_Led1(handle);
	allLeds_enseq_main_region_fordward_r1_Led2_default(handle);
}

static void allLeds_effect_main_region_fordward_r1_Led2_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_fordward_r1_Led2(handle);
	allLeds_enseq_main_region_fordward_r1_Led3_default(handle);
}

static void allLeds_effect_main_region_fordward_r1_Led3_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_fordward_r1_Led3(handle);
	allLeds_enseq_main_region_fordward_r1_Led0_default(handle);
}

static void allLeds_effect_main_region_backward_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_backward(handle);
	allLeds_enseq_main_region_fordward_default(handle);
}

static void allLeds_effect_main_region_backward_r1_Led3_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_backward_r1_Led3(handle);
	allLeds_enseq_main_region_backward_r1_Led2_default(handle);
}

static void allLeds_effect_main_region_backward_r1_Led2_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_backward_r1_Led2(handle);
	allLeds_enseq_main_region_backward_r1_Led1_default(handle);
}

static void allLeds_effect_main_region_backward_r1_Led1_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_backward_r1_Led1(handle);
	allLeds_enseq_main_region_backward_r1_Led0_default(handle);
}

static void allLeds_effect_main_region_backward_r1_Led0_tr0(AllLeds* handle)
{
	allLeds_exseq_main_region_backward_r1_Led0(handle);
	allLeds_enseq_main_region_backward_r1_Led3_default(handle);
}

/* Entry action for state 'Led0'. */
static void allLeds_enact_main_region_fordward_r1_Led0(AllLeds* handle)
{
	/* Entry action for state 'Led0'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led0_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 0);
}

/* Entry action for state 'Led1'. */
static void allLeds_enact_main_region_fordward_r1_Led1(AllLeds* handle)
{
	/* Entry action for state 'Led1'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led1_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 1);
}

/* Entry action for state 'Led2'. */
static void allLeds_enact_main_region_fordward_r1_Led2(AllLeds* handle)
{
	/* Entry action for state 'Led2'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led2_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 2);
}

/* Entry action for state 'Led3'. */
static void allLeds_enact_main_region_fordward_r1_Led3(AllLeds* handle)
{
	/* Entry action for state 'Led3'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led3_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 3);
}

/* Entry action for state 'Led3'. */
static void allLeds_enact_main_region_backward_r1_Led3(AllLeds* handle)
{
	/* Entry action for state 'Led3'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led3_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 3);
}

/* Entry action for state 'Led2'. */
static void allLeds_enact_main_region_backward_r1_Led2(AllLeds* handle)
{
	/* Entry action for state 'Led2'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led2_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 2);
}

/* Entry action for state 'Led1'. */
static void allLeds_enact_main_region_backward_r1_Led1(AllLeds* handle)
{
	/* Entry action for state 'Led1'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led1_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 1);
}

/* Entry action for state 'Led0'. */
static void allLeds_enact_main_region_backward_r1_Led0(AllLeds* handle)
{
	/* Entry action for state 'Led0'. */
	allLeds_setTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led0_tev0_raised) , 250, bool_false);
	allLedsIface_ledOn(handle, 0);
}

/* Exit action for state 'Led0'. */
static void allLeds_exact_main_region_fordward_r1_Led0(AllLeds* handle)
{
	/* Exit action for state 'Led0'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led0_tev0_raised) );		
	allLedsIface_ledOff(handle, 0);
}

/* Exit action for state 'Led1'. */
static void allLeds_exact_main_region_fordward_r1_Led1(AllLeds* handle)
{
	/* Exit action for state 'Led1'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led1_tev0_raised) );		
	allLedsIface_ledOff(handle, 1);
}

/* Exit action for state 'Led2'. */
static void allLeds_exact_main_region_fordward_r1_Led2(AllLeds* handle)
{
	/* Exit action for state 'Led2'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led2_tev0_raised) );		
	allLedsIface_ledOff(handle, 2);
}

/* Exit action for state 'Led3'. */
static void allLeds_exact_main_region_fordward_r1_Led3(AllLeds* handle)
{
	/* Exit action for state 'Led3'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_fordward_r1_Led3_tev0_raised) );		
	allLedsIface_ledOff(handle, 3);
}

/* Exit action for state 'Led3'. */
static void allLeds_exact_main_region_backward_r1_Led3(AllLeds* handle)
{
	/* Exit action for state 'Led3'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led3_tev0_raised) );		
	allLedsIface_ledOff(handle, 3);
}

/* Exit action for state 'Led2'. */
static void allLeds_exact_main_region_backward_r1_Led2(AllLeds* handle)
{
	/* Exit action for state 'Led2'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led2_tev0_raised) );		
	allLedsIface_ledOff(handle, 2);
}

/* Exit action for state 'Led1'. */
static void allLeds_exact_main_region_backward_r1_Led1(AllLeds* handle)
{
	/* Exit action for state 'Led1'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led1_tev0_raised) );		
	allLedsIface_ledOff(handle, 1);
}

/* Exit action for state 'Led0'. */
static void allLeds_exact_main_region_backward_r1_Led0(AllLeds* handle)
{
	/* Exit action for state 'Led0'. */
	allLeds_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.allLeds_main_region_backward_r1_Led0_tev0_raised) );		
	allLedsIface_ledOff(handle, 0);
}

/* 'default' enter sequence for state fordward */
static void allLeds_enseq_main_region_fordward_default(AllLeds* handle)
{
	/* 'default' enter sequence for state fordward */
	allLeds_enseq_main_region_fordward_r1_default(handle);
}

/* 'default' enter sequence for state Led0 */
static void allLeds_enseq_main_region_fordward_r1_Led0_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led0 */
	allLeds_enact_main_region_fordward_r1_Led0(handle);
	handle->stateConfVector[0] = AllLeds_main_region_fordward_r1_Led0;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led1 */
static void allLeds_enseq_main_region_fordward_r1_Led1_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led1 */
	allLeds_enact_main_region_fordward_r1_Led1(handle);
	handle->stateConfVector[0] = AllLeds_main_region_fordward_r1_Led1;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led2 */
static void allLeds_enseq_main_region_fordward_r1_Led2_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led2 */
	allLeds_enact_main_region_fordward_r1_Led2(handle);
	handle->stateConfVector[0] = AllLeds_main_region_fordward_r1_Led2;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led3 */
static void allLeds_enseq_main_region_fordward_r1_Led3_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led3 */
	allLeds_enact_main_region_fordward_r1_Led3(handle);
	handle->stateConfVector[0] = AllLeds_main_region_fordward_r1_Led3;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state backward */
static void allLeds_enseq_main_region_backward_default(AllLeds* handle)
{
	/* 'default' enter sequence for state backward */
	allLeds_enseq_main_region_backward_r1_default(handle);
}

/* 'default' enter sequence for state Led3 */
static void allLeds_enseq_main_region_backward_r1_Led3_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led3 */
	allLeds_enact_main_region_backward_r1_Led3(handle);
	handle->stateConfVector[0] = AllLeds_main_region_backward_r1_Led3;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led2 */
static void allLeds_enseq_main_region_backward_r1_Led2_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led2 */
	allLeds_enact_main_region_backward_r1_Led2(handle);
	handle->stateConfVector[0] = AllLeds_main_region_backward_r1_Led2;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led1 */
static void allLeds_enseq_main_region_backward_r1_Led1_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led1 */
	allLeds_enact_main_region_backward_r1_Led1(handle);
	handle->stateConfVector[0] = AllLeds_main_region_backward_r1_Led1;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led0 */
static void allLeds_enseq_main_region_backward_r1_Led0_default(AllLeds* handle)
{
	/* 'default' enter sequence for state Led0 */
	allLeds_enact_main_region_backward_r1_Led0(handle);
	handle->stateConfVector[0] = AllLeds_main_region_backward_r1_Led0;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for region main region */
static void allLeds_enseq_main_region_default(AllLeds* handle)
{
	/* 'default' enter sequence for region main region */
	allLeds_react_main_region__entry_Default(handle);
}

/* 'default' enter sequence for region r1 */
static void allLeds_enseq_main_region_fordward_r1_default(AllLeds* handle)
{
	/* 'default' enter sequence for region r1 */
	allLeds_react_main_region_fordward_r1__entry_Default(handle);
}

/* 'default' enter sequence for region r1 */
static void allLeds_enseq_main_region_backward_r1_default(AllLeds* handle)
{
	/* 'default' enter sequence for region r1 */
	allLeds_react_main_region_backward_r1__entry_Default(handle);
}

/* Default exit sequence for state fordward */
static void allLeds_exseq_main_region_fordward(AllLeds* handle)
{
	/* Default exit sequence for state fordward */
	allLeds_exseq_main_region_fordward_r1(handle);
}

/* Default exit sequence for state Led0 */
static void allLeds_exseq_main_region_fordward_r1_Led0(AllLeds* handle)
{
	/* Default exit sequence for state Led0 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_fordward_r1_Led0(handle);
}

/* Default exit sequence for state Led1 */
static void allLeds_exseq_main_region_fordward_r1_Led1(AllLeds* handle)
{
	/* Default exit sequence for state Led1 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_fordward_r1_Led1(handle);
}

/* Default exit sequence for state Led2 */
static void allLeds_exseq_main_region_fordward_r1_Led2(AllLeds* handle)
{
	/* Default exit sequence for state Led2 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_fordward_r1_Led2(handle);
}

/* Default exit sequence for state Led3 */
static void allLeds_exseq_main_region_fordward_r1_Led3(AllLeds* handle)
{
	/* Default exit sequence for state Led3 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_fordward_r1_Led3(handle);
}

/* Default exit sequence for state backward */
static void allLeds_exseq_main_region_backward(AllLeds* handle)
{
	/* Default exit sequence for state backward */
	allLeds_exseq_main_region_backward_r1(handle);
}

/* Default exit sequence for state Led3 */
static void allLeds_exseq_main_region_backward_r1_Led3(AllLeds* handle)
{
	/* Default exit sequence for state Led3 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_backward_r1_Led3(handle);
}

/* Default exit sequence for state Led2 */
static void allLeds_exseq_main_region_backward_r1_Led2(AllLeds* handle)
{
	/* Default exit sequence for state Led2 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_backward_r1_Led2(handle);
}

/* Default exit sequence for state Led1 */
static void allLeds_exseq_main_region_backward_r1_Led1(AllLeds* handle)
{
	/* Default exit sequence for state Led1 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_backward_r1_Led1(handle);
}

/* Default exit sequence for state Led0 */
static void allLeds_exseq_main_region_backward_r1_Led0(AllLeds* handle)
{
	/* Default exit sequence for state Led0 */
	handle->stateConfVector[0] = AllLeds_last_state;
	handle->stateConfVectorPosition = 0;
	allLeds_exact_main_region_backward_r1_Led0(handle);
}

/* Default exit sequence for region main region */
static void allLeds_exseq_main_region(AllLeds* handle)
{
	/* Default exit sequence for region main region */
	/* Handle exit of all possible states (of AllLeds.main_region) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case AllLeds_main_region_fordward_r1_Led0 :
		{
			allLeds_exseq_main_region_fordward_r1_Led0(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led1 :
		{
			allLeds_exseq_main_region_fordward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led2 :
		{
			allLeds_exseq_main_region_fordward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led3 :
		{
			allLeds_exseq_main_region_fordward_r1_Led3(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led3 :
		{
			allLeds_exseq_main_region_backward_r1_Led3(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led2 :
		{
			allLeds_exseq_main_region_backward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led1 :
		{
			allLeds_exseq_main_region_backward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led0 :
		{
			allLeds_exseq_main_region_backward_r1_Led0(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region r1 */
static void allLeds_exseq_main_region_fordward_r1(AllLeds* handle)
{
	/* Default exit sequence for region r1 */
	/* Handle exit of all possible states (of AllLeds.main_region.fordward.r1) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case AllLeds_main_region_fordward_r1_Led0 :
		{
			allLeds_exseq_main_region_fordward_r1_Led0(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led1 :
		{
			allLeds_exseq_main_region_fordward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led2 :
		{
			allLeds_exseq_main_region_fordward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_fordward_r1_Led3 :
		{
			allLeds_exseq_main_region_fordward_r1_Led3(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region r1 */
static void allLeds_exseq_main_region_backward_r1(AllLeds* handle)
{
	/* Default exit sequence for region r1 */
	/* Handle exit of all possible states (of AllLeds.main_region.backward.r1) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case AllLeds_main_region_backward_r1_Led3 :
		{
			allLeds_exseq_main_region_backward_r1_Led3(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led2 :
		{
			allLeds_exseq_main_region_backward_r1_Led2(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led1 :
		{
			allLeds_exseq_main_region_backward_r1_Led1(handle);
			break;
		}
		case AllLeds_main_region_backward_r1_Led0 :
		{
			allLeds_exseq_main_region_backward_r1_Led0(handle);
			break;
		}
		default: break;
	}
}

/* The reactions of state Led0. */
static void allLeds_react_main_region_fordward_r1_Led0(AllLeds* handle)
{
	/* The reactions of state Led0. */
	if (allLeds_check_main_region_fordward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_fordward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_fordward_r1_Led0_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_fordward_r1_Led0_tr0(handle);
		} 
	}
}

/* The reactions of state Led1. */
static void allLeds_react_main_region_fordward_r1_Led1(AllLeds* handle)
{
	/* The reactions of state Led1. */
	if (allLeds_check_main_region_fordward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_fordward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_fordward_r1_Led1_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_fordward_r1_Led1_tr0(handle);
		} 
	}
}

/* The reactions of state Led2. */
static void allLeds_react_main_region_fordward_r1_Led2(AllLeds* handle)
{
	/* The reactions of state Led2. */
	if (allLeds_check_main_region_fordward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_fordward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_fordward_r1_Led2_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_fordward_r1_Led2_tr0(handle);
		} 
	}
}

/* The reactions of state Led3. */
static void allLeds_react_main_region_fordward_r1_Led3(AllLeds* handle)
{
	/* The reactions of state Led3. */
	if (allLeds_check_main_region_fordward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_fordward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_fordward_r1_Led3_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_fordward_r1_Led3_tr0(handle);
		} 
	}
}

/* The reactions of state Led3. */
static void allLeds_react_main_region_backward_r1_Led3(AllLeds* handle)
{
	/* The reactions of state Led3. */
	if (allLeds_check_main_region_backward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_backward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_backward_r1_Led3_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_backward_r1_Led3_tr0(handle);
		} 
	}
}

/* The reactions of state Led2. */
static void allLeds_react_main_region_backward_r1_Led2(AllLeds* handle)
{
	/* The reactions of state Led2. */
	if (allLeds_check_main_region_backward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_backward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_backward_r1_Led2_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_backward_r1_Led2_tr0(handle);
		} 
	}
}

/* The reactions of state Led1. */
static void allLeds_react_main_region_backward_r1_Led1(AllLeds* handle)
{
	/* The reactions of state Led1. */
	if (allLeds_check_main_region_backward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_backward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_backward_r1_Led1_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_backward_r1_Led1_tr0(handle);
		} 
	}
}

/* The reactions of state Led0. */
static void allLeds_react_main_region_backward_r1_Led0(AllLeds* handle)
{
	/* The reactions of state Led0. */
	if (allLeds_check_main_region_backward_tr0_tr0(handle) == bool_true)
	{ 
		allLeds_effect_main_region_backward_tr0(handle);
	}  else
	{
		if (allLeds_check_main_region_backward_r1_Led0_tr0_tr0(handle) == bool_true)
		{ 
			allLeds_effect_main_region_backward_r1_Led0_tr0(handle);
		} 
	}
}

/* Default react sequence for initial entry  */
static void allLeds_react_main_region_fordward_r1__entry_Default(AllLeds* handle)
{
	/* Default react sequence for initial entry  */
	allLeds_enseq_main_region_fordward_r1_Led0_default(handle);
}

/* Default react sequence for initial entry  */
static void allLeds_react_main_region_backward_r1__entry_Default(AllLeds* handle)
{
	/* Default react sequence for initial entry  */
	allLeds_enseq_main_region_backward_r1_Led3_default(handle);
}

/* Default react sequence for initial entry  */
static void allLeds_react_main_region__entry_Default(AllLeds* handle)
{
	/* Default react sequence for initial entry  */
	allLeds_enseq_main_region_fordward_default(handle);
}


