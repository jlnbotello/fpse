#include <stdint.h>
#include "sysUtils.h"
#include "chip.h"
#include "sc_types.h"
#include "AllLeds.h"
#include "timerTicks.h"
#define NOF_TIMERS (sizeof(AllLedsTimeEvents)/sizeof(sc_boolean))

static TimerTicks ticks[NOF_TIMERS];

void allLeds_setTimer(AllLeds* handle, const sc_eventid evid, const sc_integer time_ms, const sc_boolean periodic)
{
	SetNewTimerTick(ticks, NOF_TIMERS, evid, time_ms, periodic);
}


void allLeds_unsetTimer(AllLeds* handle, const sc_eventid evid)
{
	UnsetTimerTick(ticks, NOF_TIMERS, evid);
}

void allLedsIface_ledOn(const AllLeds* handle, const sc_integer led){
	ledOn(led);
}
void allLedsIface_ledOff(const AllLeds* handle, const sc_integer led){
	ledOff(led);
}

int main(void)
{
	int i;
	AllLeds estados;
	sysInit();
	allLeds_init(&estados);
	allLeds_enter(&estados);
	while(1){
	    __WFI();
	    if(getSysTickEv())
	    {
	      rstSysTickEv();
	      UpdateTimers(ticks, NOF_TIMERS);
	      for (i = 0; i < NOF_TIMERS; i++)
	      {
	        if (IsPendEvent(ticks, NOF_TIMERS, ticks[i].evid))
	        {
	          allLeds_raiseTimeEvent(&estados, ticks[i].evid);
	          MarkAsAttEvent(ticks, NOF_TIMERS, ticks[i].evid);
	        }
	      }

		}
	    if(getKeyPressed() != 0)
	    {
	    	allLedsIface_raise_keyPress(&estados, getKeyPressed());
	    	rstKeyPressed();
	    }
		allLeds_runCycle(&estados);
	  }
  return 0;
}






