/*
 * paquete.c
 *
 *  Created on: Oct 6, 2018
 *      Author: quack
 */

#include "packet.h"


bool checkSum(packet packet){
	uint8_t sum=packet.st.start+packet.st.cmd+packet.st.target+packet.st.end;
	if(sum==packet.st.crc)
		return true;
	else
		return false;
}

void procesar(uint8_t byte){
	static packet packet;
	static uint8_t position=0;
	static bool receiving=false;
	if(position==0 && byte==STR_BYTE){
		packet.db[0]=byte;
		position++;
	}
	else{
		packet.db[position]=byte;
		position++;
		if(position==dataLen){
			position=0;
			if(byte==END_BYTE /*&& checkSum(packet)==true*/)
				runAction(packet);
		}
	}
}



void runAction(packet packet){
	if(packet.st.cmd==0x0F){
		led(packet.st.target%6,true);
	}
	if(packet.st.cmd==0xF0){
		led(packet.st.target%6,false);
	}
}




