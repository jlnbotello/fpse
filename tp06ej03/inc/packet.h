/*
 * paquete.h
 *
 *  Created on: Oct 6, 2018
 *      Author: quack
 */

#ifndef INC_PACKET_H_
#define INC_PACKET_H_

#include "stdint.h"
#include "lpc_types.h"
#include "hw.h"

#define dataLen 5
#define STR_BYTE 0X0C
#define END_BYTE 0XC0
typedef struct
{
uint8_t start ;
uint8_t cmd ;
uint8_t target ;
uint8_t crc ;
uint8_t end ;
} dataPckt __attribute__ (( packed ));

typedef uint8_t dataBytes[dataLen];

typedef union
{
dataPckt st;
dataBytes db;
} packet ;


bool checkSum(packet packet);

void procesar(uint8_t byte);

void runAction(packet packet);


#endif /* INC_PACKET_H_ */
