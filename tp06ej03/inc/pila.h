#ifndef INC_PILA_H_
#define INC_PILA_H_

#include "stdint.h"

/*
Estructura definida para representar cada pila particular . Nótese que el
vector de almacenamiento se establece a través de la función pilaInit
y debe ser definido , de forma externa a la estructura y de tipo congruente
con los valores a alojar en la pila .
*/
typedef struct
{
uint8_t pos ; /* Posición donde cargar el proximo valor */
uint8_t dataSize ; /* Tamaño de cada dato en el arreglo buf */
uint8_t dataCount ; /* Tamaño del buffer */
uint8_t * buf ; /* Buffer de almacenamiento */
} pilaData ;


void pilaInit ( pilaData *p, /* Representación de una pila */
				void * buffer , /* Vector de almacenamiento */
				uint8_t bufLen , /* Tamaño del arreglo */
				uint8_t dataSize /* Tamaño de cada dato en el arreglo */
				);

void pilaPush ( pilaData *p, void * data );

void pilaPop ( pilaData *p, void * data );

uint8_t pilaEmpty ( pilaData *p);

uint8_t pilaFull ( pilaData *p);



#endif /* INC_PILA_H_ */
