#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include <stdint.h>
#include <stdio.h>
#include <math.h>

/* Retorna la cantidad de dígitos de una valor contenido en una variable
 * de tipo entero de 32 bits.
 */
uint8_t nDigitos(uint32_t valor);

/* Retorna un valor conformado por los n dígitos más significativos de un
 * valor entero, sin signo de 32 bits.
 */
uint32_t nMasSigni(uint32_t valor, uint8_t n);

/* Mostrar por pantalla la representación en base 2 de un valor entero,
 * sin signo de 32 bits.
 */
void mostrarBin(uint32_t valor);

/* Retornar la representación ASCII del dígito menos significativo de un
 * valor entero de 32 bits, sin signo, dado como argumento.
 */
uint8_t menosSigni(uint32_t valor);

/* Retorna el bit de la posición pasada como argunmento
 */
uint8_t bitDePos(uint32_t valor, uint8_t pos);

/* Retorna el digito en la posición N de un decimal
 */
uint8_t digitoDecimalN(uint32_t valor, uint8_t pos);

/* Muestra en la salida estandar un array de caracteres.
 * El caracter de fin debe ser  '\0'
 */
void putStr(char * str);

/* Muestra en la salida estandar los digitos de un decimal separados con espacios
 */
void mostrarDec(uint32_t valor);

/* En puntero tuASCII apuntará a un arreglo con los caracteres ASCII
 * de un valor de uint32_t pasado como parámetro.
 */
void uint32ToASCII(uint32_t valor, char * toASCII);

/* Retorna la cantidad de digitos de un valor uint32_t. Se debe dar como parámetros
 * un puntero a un arrreglo de 10 elementos (máximas cantidad de dítigos de un uint32_t)
 */
uint8_t digitosDec(uint32_t valor, uint8_t * digitos);


#endif /* INC_UTILS_H_ */
