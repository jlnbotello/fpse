#include "funciones.h"


int main(int argc, char * argv[]){
	uint32_t valor=0;
	char valorASCII[11];
	putStr("Ingrese un valor de prueba: ");
	scanf("%u", &valor);
	// Cantidad de ditigos
	uint8_t N= nDigitos(valor);
	putStr("Tiene ");
	uint32ToASCII(N, valorASCII);
	putStr(valorASCII);
	putStr(" digitos\n");
	// Digitos significativos
	for(uint8_t i=1;i<=N;i++){
		putStr("Cantidad de dígitos significativos ");
		uint32ToASCII(i, valorASCII);
		putStr(valorASCII);
		putStr(" = ");
		uint32ToASCII(nMasSigni(valor,i), valorASCII);
		putStr(valorASCII);
		putStr("\n");
	}
	// Base 2
	putStr("Representación en base 2: ");
	mostrarBin(valor);
	putStr("\n");
	// Por carateres
	putStr("Valor mostrado por caracteres: ");
	mostrarDec(valor);
}


