/*! \mainpage Mi primera aplicación
 * \date 01/09/2018
 * \author E. Sergio Burgos
 * \section genDesc Descripción general
 *
 * Esta es una primera aplicación diseñada con el objetivo de verificar la integridad del sistema
 * de desarrollo. A los fines de facilitar la elaboración de los informes posteriores, es recomendable
 * que a continuación complete la documentación de este archivo según progrese en el desarrollo de
 * las actividades propuestas.
 *
 *\section desarrolloObs Observaciones generales
 * [Complete aquí con sus observaciones]
 *
 * \section changelog Registro de cambios
 *
 * |   Fecha    | Descripción                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/09/2018 | Creación del documento                         |
 *
 */

#include "hardware.h"
#include "utils.h"
#include <stdint.h>

#define CERRADA 0
#define ABIERTA 1
#define CANT_DIGS 6
#define TODOS 2
#define ROJO 1
#define VERDE 0
#define DESTELLOS 5
uint8_t pass[]={1,2,3,3,2,1};
bool verficar(uint8_t,uint8_t);
void destellar5(uint8_t);
uint8_t numTecla(void);



int main(void){
	systemInit();
	uint8_t estado=CERRADA;
	uint8_t numTec=0;
	uint8_t passIngresada[CANT_DIGS];
	uint8_t cont=0;
	uint8_t contNueva=0;


	while (1){
		numTec=numTecla();
		if(numTec!=0){
			delayMs(500);
			setLedFromMsk(0);
			if(estado==ABIERTA){
				pass[contNueva]=numTec;
				contNueva++;
				if(contNueva==6){
					contNueva=0;
					destellar5(TODOS); // todos
					estado=CERRADA;
				}
			}else{
				if(verficar(numTec,cont)){
					if(cont==5){
						destellar5(VERDE); //color verde
						cont=0;
						estado=ABIERTA;
					}
					cont++;
				}
				else{
					destellar5(ROJO); // color rojo
					cont=0;
				}
			}

		}
	}
	return 0;
}



bool verficar(uint8_t num,uint8_t pos){
	if(pass[pos]==num)
		return TRUE;
	else
		return FALSE;

}

uint8_t numTecla(){
	uint8_t key=getKeyPressed();
    key=key&0x0F;
	uint8_t key01=key&key1;
	uint8_t key02=key&key2;
	uint8_t key03=key&key3;
	uint8_t key04=key&key4;
	if(key01==1){
		setLedFromMsk(led6); //azul RGB
		key= 1;
	}
	if(key02>1){
		setLedFromMsk(led1);
		key= 2;
	}
	if(key03>1){
		setLedFromMsk(led2);
		key= 3;
	}

	if(key04>1){
		setLedFromMsk(led3);
		key= 4;
	}
	return key;

}

void destellar5(uint8_t color){
	uint8_t gpioPin=0;
	uint8_t gpioPort=0;
	if(color==ROJO){
		gpioPort=GPIO_PORT_LED1;
		gpioPin=GPIO_PIN_LED1;
	}
	if(color==VERDE){
		gpioPort=GPIO_PORT_LED3;
		gpioPin=GPIO_PIN_LED3;
	}

	for(uint8_t i=0;i<2*DESTELLOS;i++){

		if(color==TODOS){
			ledToggle(GPIO_PORT_RGB, GPIO_PIN_BLU);
			ledToggle(GPIO_PORT_LED1, GPIO_PIN_LED1);
			ledToggle(GPIO_PORT_LED2, GPIO_PIN_LED2);
			ledToggle(GPIO_PORT_LED3, GPIO_PIN_LED3);
		}else{
			ledToggle(gpioPort, gpioPin);
		}
		delayMs(250);
	}
}
















