#include "morse.h"
#include "sysConfig.h"
#include "ledsUtils.h"
#define T 300 	/**<ms*/
#define P T 	/**<Punto*/
#define R 3*P 	/**<Raya */
#define PS 1*P	/**<Pausa Símbolo*/
#define PL 3*P	/**<Pausa Letra*/
#define PP 5*P 	/**<Pausa Palabra*/

const uint16_t a[]={P,R,0};
const uint16_t b[]={R,P,P,P,0};
const uint16_t c[]={R,P,R,P,0};
const uint16_t d[]={R,P,P,0};
const uint16_t e[]={P,0};
const uint16_t f[]={P,P,R,P,0};
const uint16_t g[]={R,R,P,0};
const uint16_t h[]={P,P,P,P,0};
const uint16_t i[]={P,P,0};
const uint16_t j[]={P,R,R,R,0};
const uint16_t k[]={R,P,R,0};
const uint16_t l[]={P,R,P,P,0};
const uint16_t m[]={R,R,0};
const uint16_t n[]={R,P,0};
const uint16_t o[]={R,R,R,0};
const uint16_t p[]={P,R,R,P,0};
const uint16_t q[]={R,R,P,R,0};
const uint16_t r[]={P,R,P,0};
const uint16_t s[]={P,P,P,0};
const uint16_t t[]={R,0};
const uint16_t u[]={P,P,R,0};
const uint16_t v[]={P,P,P,R,0};
const uint16_t w[]={P,R,R,0};
const uint16_t x[]={R,P,P,R,0};
const uint16_t y[]={R,P,R,R,0};
const uint16_t z[]={R,R,P,P,0};

const uint16_t n0[]={R,R,R,R,R,0};
const uint16_t n1[]={P,R,R,R,R,0};
const uint16_t n2[]={P,P,R,R,R,0};
const uint16_t n3[]={P,P,P,R,R,0};
const uint16_t n4[]={P,P,P,P,R,0};
const uint16_t n5[]={P,P,P,P,P,0};
const uint16_t n6[]={R,P,P,P,P,0};
const uint16_t n7[]={R,R,P,P,P,0};
const uint16_t n8[]={R,R,R,P,P,0};
const uint16_t n9[]={R,R,R,R,P,0};

const uint16_t * const numerosMorse[]={n0,n1,n2,n3,n4,n5,n6,n7,n8,n9};
const uint16_t * const letrasMorse[]={a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z};

void textToMorse(uint8_t * txt){
	uint16_t cont=0;
	while(txt[cont]!='\0'&& cont<MAXLEN){
		charToMorse(txt[cont]);
		cont++;
	}
	print("\n"); //to test
}

void charToMorse(char ch){
	uint16_t *morse;
	if(ch==' '){
		print("| "); //to test
		delayMs(PP);
	}
	else{
		if(ch>='0'&&ch<='9')
			morse=numerosMorse[ch-'0'];
		if(ch>='A'&&ch<='Z')
			morse=letrasMorse[ch-'A'];
		if(ch>='a'&&ch<='z')
			morse=letrasMorse[ch-'a'];
		uint8_t pos=0;
		do
		{
			displaySymbol(morse[pos]); //to test
			led(ALL,TRUE);
			delayMs(morse[pos]);
			led(ALL,FALSE);
			pos++;
			if(morse[pos]!=0){
				delayMs(PS);
			}
			else{
				print(" "); //to test
				delayMs(PL);
			}
		}while(morse[pos]!=0);

	}

}

void displaySymbol(uint16_t morse ){
	if(morse==P)
		print("P");
	if(morse==R)
		print("R");
}
