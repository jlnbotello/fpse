/** @addtogroup ledsUtils Utilidad para leds
   \brief Funciones para manipular Leds de la EDU-CIAA
*  @{
*/
#ifndef INC_LEDSUTILS_H_
#define INC_LEDSUTILS_H_

#define LED0	0 /**< Led Azul RGB		*/
#define LED1 	1 /**< Led 1: rojo		*/
#define LED2 	2 /**< Led 2: amarillo	*/
#define LED3 	3 /**< Led 3: verde		*/
#define LEDR	4 /**< Led Rojo RGB		*/
#define LEDG	5 /**< Led Verde RGB	*/
#define LEDB	0 /**< Led Azul RGB		*/
#define ALL		6 /**< Led 1,2,3 y LEDB	*/

/**\brief Representación binaria mediante leds (4 bits menos significativos)
 *
 * Permite representar los 4 bits menos significativos de un número a través
 * los leds de la EDU-CIAA. Peso de los leds utilizados: LEDB(8) LED1(4) LED2(2) LED3(1)
 * \param[in] v Valor a mostrar
 */
void displayCounter(uint8_t v);

/**\brief Realiza un ida y vuelta de los leds (zigzag). En cada llamada realiza un transición
 *
 * Se recomienda llamarla en el systick con una bandera que se levante a la frecuencia deseada
 */

void ledZigZag();

/**\brief Destella el led indicado N veces a intervalos de ms
 *
 * \param[in] led Identificador de led
 * \param[in] N   Numero de destellos
 * \param[in] ms Intervalo entre destellos en milisegundos
 */
void destellar(uint8_t led,uint8_t N,uint32_t ms );

/**\brief Enciende o apaga un led
 * \param[in] led Identificador de led
 * \param[in] setting Determina el estado del led. TRUE: enciende y FALSE: apaga
 */
void led(uint8_t led, _Bool setting);

/**\brief Conmuta el estado del led indicado
 * \param[in] led Identificador de led
 */
void toggleLed(uint8_t led);

#endif /* INC_LEDSUTILS_H_ */

/** @}*/
