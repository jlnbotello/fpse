#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include "stdint.h"
#include <lpc_types.h>
uint8_t digitoBase10(uint32_t valor, uint8_t pos);
uint8_t numASCII(uint8_t num);
uint8_t cantDigitos(uint32_t v);
void registroDeCuentas(uint8_t tecla);


#endif /* INC_UTILS_H_ */
