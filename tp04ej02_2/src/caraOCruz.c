/*
 * caraOCruz.c
 *
 *  Created on: Oct 30, 2018
 *      Author: quack
 */
#include "stdint.h"
#include "ledsUtils.h"
#include "sysConfig.h"
#include "lpc_types.h"
#include "stdlib.h"
#include "caraOCruz.h"



#define DELAY 250
#define WIN  75
#define LOSE 300
#define ST_SECUENCIA  1
#define ST_WAIT_USER  2
#define ST_WAIT_EDU	  3
#define ST_RESULT	  4

uint32_t seed=0;
static uint8_t state=ST_SECUENCIA;
static uint8_t userLed;
static uint8_t eduLed;
static uint8_t lastKey=0;
void keyPressed(uint8_t key){

	switch (key) {
		case TECLA1:
			if(state==ST_RESULT)
				state=ST_SECUENCIA;
			break;
		case TECLA2:
			if(state==ST_WAIT_USER){
				state=ST_WAIT_EDU;
				userLed=LED1; //RED
				led(LED3,FALSE);
				led(LED1,TRUE);
			}
			break;
		case TECLA3:
			if(state==ST_SECUENCIA){
				state=ST_WAIT_USER;
				srand(seed);
			}
			break;
		case TECLA4:
			if(state==ST_WAIT_USER){
				state=ST_WAIT_EDU;
				userLed=LED3; //GREEN
				led(LED1,FALSE);
				led(LED3,TRUE);

			}
			break;
		default:
			break;
	}
	lastKey=key;
}

void initSec(){
	led(ALL,FALSE);
	led(LEDR,FALSE);
	led(LEDG,FALSE);
	while(state==ST_SECUENCIA){
		ledZigZag();
		delayMs(DELAY);
	}
}

void waitUser(){
	led(ALL,FALSE);
	while(state==ST_WAIT_USER){
		toggleLed(LED1);
		toggleLed(LED3);
		delayMs(DELAY);
	}

}

uint8_t waitEdu(uint8_t enableDelay){
	uint8_t delta=rand()%20;
	uint8_t n=20+delta;
	int num=0;
	while(n--){
		num = rand()%2;
		if(num==CARA){
			led(LEDG,FALSE);
			led(LEDR,TRUE);
			eduLed=LEDR;
		}
		else{
			led(LEDR,FALSE);
			led(LEDG,TRUE);
			eduLed=LEDG;
		}
		if(enableDelay==TRUE)
		delayMs(250);
	}
	state=ST_RESULT;
	return num;

}

void result(){
	while(state==ST_RESULT){
		toggleLed(userLed);
		toggleLed(eduLed);
		if((userLed==LED1&&eduLed==LEDR)||(userLed==LED3&&eduLed==LEDG))
			delayMs(WIN);
		else
			delayMs(LOSE);
	}
}

void play(){
	initSec();
	waitUser();
	waitEdu(TRUE);
	result();
}
