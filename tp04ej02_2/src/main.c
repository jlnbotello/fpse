/** \mainpage Página Principal
 *
 * Esta aplicación gestiona un contador. Constantemente titilan todos los leds.
 * Cuando se presiona una tecla titila 5 veces el led asociado
 * ##Teclas en EDU-CIAA##
 * + La \ref TECLA1 incrementa el contador
 * + La \ref TECLA2 bloquea el contador
 * + La \ref TECLA3 decrementa el contador
 * + La \ref TECLA4 Vuelve a cero el contador
 *
 * ##Visualización##
 * A través de una terminal gtkTerm
 * ###Configuración###
 * + __Baud Rate:__ 115200
 * + __Paridad:__ No
 * + __Bit de parada:__ 1
 * + __Bits:__ 8
 */
#include "stdint.h"

#include "sysConfig.h"
#include "caraOCruz.h"
#include "textUtils.h"
#include "ledsUtils.h"


int main(void){;
	systemInit();
	SysTick_Config ( SystemCoreClock / 1000);
//	/*
	while(1){
		play();
	}
//	*/
	/*
	char numero1[10];
	char numero2[10];
	uint32_t cara=0;
	uint32_t porcentajeCara=0;
	uint32_t cruz=0;
	uint32_t porcentajeCruz=0;
	uint32_t total=0;
	print("cara|cruz\n");
	while (1){
		if(CARA==waitEdu(FALSE))
			cara++;
		else
			cruz++;
		porcentajeCara=(100*cara)/(cara+cruz);
		porcentajeCruz=100-porcentajeCara;
		uint32ToASCII(porcentajeCara,numero1);
		print(numero1);
		print(" | ");
		uint32ToASCII(porcentajeCruz,numero2);
		print(numero2);
		print("\r\n");
	};
	*/
	return 0;
}
