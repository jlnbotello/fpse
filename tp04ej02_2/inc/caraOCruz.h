/*
 * caraOcruz.h
 *
 *  Created on: Oct 31, 2018
 *      Author: quack
 */

#ifndef INC_CARAOCRUZ_H_
#define INC_CARAOCRUZ_H_
#include "stdint.h"
#define CARA 0
#define CRUZ 1
extern uint32_t seed;
void keyPressed(uint8_t key);

void initSec();

uint8_t waitEdu(uint8_t enableDelay);

void result();

void play();

#endif /* INC_CARAOCRUZ_H_ */
