
#ifndef LC_H_
#define LC_H_

#include "sc_types.h"
		
#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'LC'.
*/

/*! Enumeration of all states */ 
typedef enum
{
	LC_main_wait,
	LC_main_cRegresiva,
	LC_main_cRegresiva_r1_Led2On,
	LC_main_cRegresiva_r1_Led2Off,
	LC_main_launch,
	LC_last_state
} LCStates;

/*! Type definition of the data structure for the LCIface interface scope. */
typedef struct
{
	sc_boolean keyPress_raised;
	sc_integer keyPress_value;
} LCIface;

/*! Type definition of the data structure for the LCTimeEvents interface scope. */
typedef struct
{
	sc_boolean lC_main_cRegresiva_tev0_raised;
	sc_boolean lC_main_cRegresiva_r1_Led2On_tev0_raised;
	sc_boolean lC_main_cRegresiva_r1_Led2Off_tev0_raised;
} LCTimeEvents;


/*! Define dimension of the state configuration vector for orthogonal states. */
#define LC_MAX_ORTHOGONAL_STATES 1

/*! 
 * Type definition of the data structure for the LC state machine.
 * This data structure has to be allocated by the client code. 
 */
typedef struct
{
	LCStates stateConfVector[LC_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	LCIface iface;
	LCTimeEvents timeEvents;
} LC;

/*! Initializes the LC state machine data structures. Must be called before first usage.*/
extern void lC_init(LC* handle);

/*! Activates the state machine */
extern void lC_enter(LC* handle);

/*! Deactivates the state machine */
extern void lC_exit(LC* handle);

/*! Performs a 'run to completion' step. */
extern void lC_runCycle(LC* handle);

/*! Raises a time event. */
extern void lC_raiseTimeEvent(const LC* handle, sc_eventid evid);

/*! Raises the in event 'keyPress' that is defined in the default interface scope. */ 
extern void lCIface_raise_keyPress(LC* handle, sc_integer value);


/*!
 * Checks whether the state machine is active (until 2.4.1 this method was used for states).
 * A state machine is active if it was entered. It is inactive if it has not been entered at all or if it has been exited.
 */
extern sc_boolean lC_isActive(const LC* handle);

/*!
 * Checks if all active states are final. 
 * If there are no active states then the state machine is considered being inactive. In this case this method returns false.
 */
extern sc_boolean lC_isFinal(const LC* handle);

/*! Checks if the specified state is active (until 2.4.1 the used method for states was called isActive()). */
extern sc_boolean lC_isStateActive(const LC* handle, LCStates state);

#ifdef __cplusplus
}
#endif 

#endif /* LC_H_ */
