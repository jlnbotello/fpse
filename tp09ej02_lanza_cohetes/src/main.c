#include <stdint.h>
#include "sysUtils.h"
#include "chip.h"
#include "sc_types.h"
#include "LC.h"
#include "timerTicks.h"
#define NOF_TIMERS (sizeof(LCTimeEvents)/sizeof(sc_boolean))

static TimerTicks ticks[NOF_TIMERS];

void lC_setTimer(LC* handle, const sc_eventid evid, const sc_integer time_ms, const sc_boolean periodic)
{
	SetNewTimerTick(ticks, NOF_TIMERS, evid, time_ms, periodic);
}


void lC_unsetTimer(LC* handle, const sc_eventid evid)
{
	UnsetTimerTick(ticks, NOF_TIMERS, evid);
}

void lCIface_ledOn(const LC* handle, const sc_integer n){
	ledOn(n);
}
void lCIface_ledOff(const LC* handle, const sc_integer n){
	ledOff(n);
}

int main(void)
{
	int i;
	LC estados;
	sysInit();
	lC_init(&estados);
	lC_enter(&estados);
	while(1){
	    __WFI();
	    if(getSysTickEv())
	    {
	      rstSysTickEv();
	      UpdateTimers(ticks, NOF_TIMERS);
	      for (i = 0; i < NOF_TIMERS; i++)
	      {
	        if (IsPendEvent(ticks, NOF_TIMERS, ticks[i].evid))
	        {
	          lC_raiseTimeEvent(&estados, ticks[i].evid);
	          MarkAsAttEvent(ticks, NOF_TIMERS, ticks[i].evid);
	        }
	      }

		}
	    if(getKeyPressed() != 0)
	    {
	    	lCIface_raise_keyPress(&estados, getKeyPressed());
	    	rstKeyPressed();
	    }
		lC_runCycle(&estados);
	  }
  return 0;
}






