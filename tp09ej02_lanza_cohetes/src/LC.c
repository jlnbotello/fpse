
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "LC.h"
#include "LCRequired.h"
/*! \file Implementation of the state machine 'LC'
*/

/* prototypes of all internal functions */
static sc_boolean lC_check_main_wait_tr0_tr0(const LC* handle);
static sc_boolean lC_check_main_cRegresiva_tr0_tr0(const LC* handle);
static sc_boolean lC_check_main_cRegresiva_tr1_tr1(const LC* handle);
static sc_boolean lC_check_main_cRegresiva_r1_Led2On_tr0_tr0(const LC* handle);
static sc_boolean lC_check_main_cRegresiva_r1_Led2Off_tr0_tr0(const LC* handle);
static sc_boolean lC_check_main_launch_tr0_tr0(const LC* handle);
static void lC_effect_main_wait_tr0(LC* handle);
static void lC_effect_main_cRegresiva_tr0(LC* handle);
static void lC_effect_main_cRegresiva_tr1(LC* handle);
static void lC_effect_main_cRegresiva_r1_Led2On_tr0(LC* handle);
static void lC_effect_main_cRegresiva_r1_Led2Off_tr0(LC* handle);
static void lC_effect_main_launch_tr0(LC* handle);
static void lC_enact_main_cRegresiva(LC* handle);
static void lC_enact_main_cRegresiva_r1_Led2On(LC* handle);
static void lC_enact_main_cRegresiva_r1_Led2Off(LC* handle);
static void lC_enact_main_launch(LC* handle);
static void lC_exact_main_cRegresiva(LC* handle);
static void lC_exact_main_cRegresiva_r1_Led2On(LC* handle);
static void lC_exact_main_cRegresiva_r1_Led2Off(LC* handle);
static void lC_exact_main_launch(LC* handle);
static void lC_enseq_main_wait_default(LC* handle);
static void lC_enseq_main_cRegresiva_default(LC* handle);
static void lC_enseq_main_cRegresiva_r1_Led2On_default(LC* handle);
static void lC_enseq_main_cRegresiva_r1_Led2Off_default(LC* handle);
static void lC_enseq_main_launch_default(LC* handle);
static void lC_enseq_main_default(LC* handle);
static void lC_enseq_main_cRegresiva_r1_default(LC* handle);
static void lC_exseq_main_wait(LC* handle);
static void lC_exseq_main_cRegresiva(LC* handle);
static void lC_exseq_main_cRegresiva_r1_Led2On(LC* handle);
static void lC_exseq_main_cRegresiva_r1_Led2Off(LC* handle);
static void lC_exseq_main_launch(LC* handle);
static void lC_exseq_main(LC* handle);
static void lC_exseq_main_cRegresiva_r1(LC* handle);
static void lC_react_main_wait(LC* handle);
static void lC_react_main_cRegresiva_r1_Led2On(LC* handle);
static void lC_react_main_cRegresiva_r1_Led2Off(LC* handle);
static void lC_react_main_launch(LC* handle);
static void lC_react_main__entry_Default(LC* handle);
static void lC_react_main_cRegresiva_r1__entry_Default(LC* handle);
static void lC_clearInEvents(LC* handle);
static void lC_clearOutEvents(LC* handle);


void lC_init(LC* handle)
{
	sc_integer i;

	for (i = 0; i < LC_MAX_ORTHOGONAL_STATES; ++i)
	{
		handle->stateConfVector[i] = LC_last_state;
	}
	
	
	handle->stateConfVectorPosition = 0;

	lC_clearInEvents(handle);
	lC_clearOutEvents(handle);


}

void lC_enter(LC* handle)
{
	/* Default enter sequence for statechart LC */
	lC_enseq_main_default(handle);
}

void lC_exit(LC* handle)
{
	/* Default exit sequence for statechart LC */
	lC_exseq_main(handle);
}

sc_boolean lC_isActive(const LC* handle)
{
	sc_boolean result;
	if (handle->stateConfVector[0] != LC_last_state)
	{
		result =  bool_true;
	}
	else
	{
		result = bool_false;
	}
	return result;
}

/* 
 * Always returns 'false' since this state machine can never become final.
 */
sc_boolean lC_isFinal(const LC* handle)
{
   return bool_false;
}

static void lC_clearInEvents(LC* handle)
{
	handle->iface.keyPress_raised = bool_false;
	handle->timeEvents.lC_main_cRegresiva_tev0_raised = bool_false;
	handle->timeEvents.lC_main_cRegresiva_r1_Led2On_tev0_raised = bool_false;
	handle->timeEvents.lC_main_cRegresiva_r1_Led2Off_tev0_raised = bool_false;
}

static void lC_clearOutEvents(LC* handle)
{
}

void lC_runCycle(LC* handle)
{
	
	lC_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < LC_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++)
		{
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition])
		{
		case LC_main_wait :
		{
			lC_react_main_wait(handle);
			break;
		}
		case LC_main_cRegresiva_r1_Led2On :
		{
			lC_react_main_cRegresiva_r1_Led2On(handle);
			break;
		}
		case LC_main_cRegresiva_r1_Led2Off :
		{
			lC_react_main_cRegresiva_r1_Led2Off(handle);
			break;
		}
		case LC_main_launch :
		{
			lC_react_main_launch(handle);
			break;
		}
		default:
			break;
		}
	}
	
	lC_clearInEvents(handle);
}

void lC_raiseTimeEvent(const LC* handle, sc_eventid evid)
{
	if ( ((sc_intptr_t)evid) >= ((sc_intptr_t)&(handle->timeEvents))
		&&  ((sc_intptr_t)evid) < ((sc_intptr_t)&(handle->timeEvents)) + sizeof(LCTimeEvents))
		{
		*(sc_boolean*)evid = bool_true;
	}		
}

sc_boolean lC_isStateActive(const LC* handle, LCStates state)
{
	sc_boolean result = bool_false;
	switch (state)
	{
		case LC_main_wait :
			result = (sc_boolean) (handle->stateConfVector[0] == LC_main_wait
			);
			break;
		case LC_main_cRegresiva :
			result = (sc_boolean) (handle->stateConfVector[0] >= LC_main_cRegresiva
				&& handle->stateConfVector[0] <= LC_main_cRegresiva_r1_Led2Off);
			break;
		case LC_main_cRegresiva_r1_Led2On :
			result = (sc_boolean) (handle->stateConfVector[0] == LC_main_cRegresiva_r1_Led2On
			);
			break;
		case LC_main_cRegresiva_r1_Led2Off :
			result = (sc_boolean) (handle->stateConfVector[0] == LC_main_cRegresiva_r1_Led2Off
			);
			break;
		case LC_main_launch :
			result = (sc_boolean) (handle->stateConfVector[0] == LC_main_launch
			);
			break;
		default:
			result = bool_false;
			break;
	}
	return result;
}

void lCIface_raise_keyPress(LC* handle, sc_integer value)
{
	handle->iface.keyPress_value = value;
	handle->iface.keyPress_raised = bool_true;
}



/* implementations of all internal functions */

static sc_boolean lC_check_main_wait_tr0_tr0(const LC* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value == 1)) ? bool_true : bool_false;
}

static sc_boolean lC_check_main_cRegresiva_tr0_tr0(const LC* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value == 2 || handle->iface.keyPress_value == 3)) ? bool_true : bool_false;
}

static sc_boolean lC_check_main_cRegresiva_tr1_tr1(const LC* handle)
{
	return handle->timeEvents.lC_main_cRegresiva_tev0_raised;
}

static sc_boolean lC_check_main_cRegresiva_r1_Led2On_tr0_tr0(const LC* handle)
{
	return handle->timeEvents.lC_main_cRegresiva_r1_Led2On_tev0_raised;
}

static sc_boolean lC_check_main_cRegresiva_r1_Led2Off_tr0_tr0(const LC* handle)
{
	return handle->timeEvents.lC_main_cRegresiva_r1_Led2Off_tev0_raised;
}

static sc_boolean lC_check_main_launch_tr0_tr0(const LC* handle)
{
	return ((handle->iface.keyPress_raised) && (handle->iface.keyPress_value == 4)) ? bool_true : bool_false;
}

static void lC_effect_main_wait_tr0(LC* handle)
{
	lC_exseq_main_wait(handle);
	lC_enseq_main_cRegresiva_default(handle);
}

static void lC_effect_main_cRegresiva_tr0(LC* handle)
{
	lC_exseq_main_cRegresiva(handle);
	lC_enseq_main_wait_default(handle);
}

static void lC_effect_main_cRegresiva_tr1(LC* handle)
{
	lC_exseq_main_cRegresiva(handle);
	lC_enseq_main_launch_default(handle);
}

static void lC_effect_main_cRegresiva_r1_Led2On_tr0(LC* handle)
{
	lC_exseq_main_cRegresiva_r1_Led2On(handle);
	lC_enseq_main_cRegresiva_r1_Led2Off_default(handle);
}

static void lC_effect_main_cRegresiva_r1_Led2Off_tr0(LC* handle)
{
	lC_exseq_main_cRegresiva_r1_Led2Off(handle);
	lC_enseq_main_cRegresiva_r1_Led2On_default(handle);
}

static void lC_effect_main_launch_tr0(LC* handle)
{
	lC_exseq_main_launch(handle);
	lC_enseq_main_wait_default(handle);
}

/* Entry action for state 'cRegresiva'. */
static void lC_enact_main_cRegresiva(LC* handle)
{
	/* Entry action for state 'cRegresiva'. */
	lC_setTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_tev0_raised) , 10 * 1000, bool_false);
}

/* Entry action for state 'Led2On'. */
static void lC_enact_main_cRegresiva_r1_Led2On(LC* handle)
{
	/* Entry action for state 'Led2On'. */
	lC_setTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_r1_Led2On_tev0_raised) , 500, bool_false);
	lCIface_ledOn(handle, 2);
}

/* Entry action for state 'Led2Off'. */
static void lC_enact_main_cRegresiva_r1_Led2Off(LC* handle)
{
	/* Entry action for state 'Led2Off'. */
	lC_setTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_r1_Led2Off_tev0_raised) , 500, bool_false);
	lCIface_ledOff(handle, 2);
}

/* Entry action for state 'launch'. */
static void lC_enact_main_launch(LC* handle)
{
	/* Entry action for state 'launch'. */
	lCIface_ledOn(handle, 1);
}

/* Exit action for state 'cRegresiva'. */
static void lC_exact_main_cRegresiva(LC* handle)
{
	/* Exit action for state 'cRegresiva'. */
	lC_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_tev0_raised) );		
	lCIface_ledOff(handle, 2);
}

/* Exit action for state 'Led2On'. */
static void lC_exact_main_cRegresiva_r1_Led2On(LC* handle)
{
	/* Exit action for state 'Led2On'. */
	lC_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_r1_Led2On_tev0_raised) );		
}

/* Exit action for state 'Led2Off'. */
static void lC_exact_main_cRegresiva_r1_Led2Off(LC* handle)
{
	/* Exit action for state 'Led2Off'. */
	lC_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.lC_main_cRegresiva_r1_Led2Off_tev0_raised) );		
}

/* Exit action for state 'launch'. */
static void lC_exact_main_launch(LC* handle)
{
	/* Exit action for state 'launch'. */
	lCIface_ledOff(handle, 1);
}

/* 'default' enter sequence for state wait */
static void lC_enseq_main_wait_default(LC* handle)
{
	/* 'default' enter sequence for state wait */
	handle->stateConfVector[0] = LC_main_wait;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state cRegresiva */
static void lC_enseq_main_cRegresiva_default(LC* handle)
{
	/* 'default' enter sequence for state cRegresiva */
	lC_enact_main_cRegresiva(handle);
	lC_enseq_main_cRegresiva_r1_default(handle);
}

/* 'default' enter sequence for state Led2On */
static void lC_enseq_main_cRegresiva_r1_Led2On_default(LC* handle)
{
	/* 'default' enter sequence for state Led2On */
	lC_enact_main_cRegresiva_r1_Led2On(handle);
	handle->stateConfVector[0] = LC_main_cRegresiva_r1_Led2On;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Led2Off */
static void lC_enseq_main_cRegresiva_r1_Led2Off_default(LC* handle)
{
	/* 'default' enter sequence for state Led2Off */
	lC_enact_main_cRegresiva_r1_Led2Off(handle);
	handle->stateConfVector[0] = LC_main_cRegresiva_r1_Led2Off;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state launch */
static void lC_enseq_main_launch_default(LC* handle)
{
	/* 'default' enter sequence for state launch */
	lC_enact_main_launch(handle);
	handle->stateConfVector[0] = LC_main_launch;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for region main */
static void lC_enseq_main_default(LC* handle)
{
	/* 'default' enter sequence for region main */
	lC_react_main__entry_Default(handle);
}

/* 'default' enter sequence for region r1 */
static void lC_enseq_main_cRegresiva_r1_default(LC* handle)
{
	/* 'default' enter sequence for region r1 */
	lC_react_main_cRegresiva_r1__entry_Default(handle);
}

/* Default exit sequence for state wait */
static void lC_exseq_main_wait(LC* handle)
{
	/* Default exit sequence for state wait */
	handle->stateConfVector[0] = LC_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state cRegresiva */
static void lC_exseq_main_cRegresiva(LC* handle)
{
	/* Default exit sequence for state cRegresiva */
	lC_exseq_main_cRegresiva_r1(handle);
	lC_exact_main_cRegresiva(handle);
}

/* Default exit sequence for state Led2On */
static void lC_exseq_main_cRegresiva_r1_Led2On(LC* handle)
{
	/* Default exit sequence for state Led2On */
	handle->stateConfVector[0] = LC_last_state;
	handle->stateConfVectorPosition = 0;
	lC_exact_main_cRegresiva_r1_Led2On(handle);
}

/* Default exit sequence for state Led2Off */
static void lC_exseq_main_cRegresiva_r1_Led2Off(LC* handle)
{
	/* Default exit sequence for state Led2Off */
	handle->stateConfVector[0] = LC_last_state;
	handle->stateConfVectorPosition = 0;
	lC_exact_main_cRegresiva_r1_Led2Off(handle);
}

/* Default exit sequence for state launch */
static void lC_exseq_main_launch(LC* handle)
{
	/* Default exit sequence for state launch */
	handle->stateConfVector[0] = LC_last_state;
	handle->stateConfVectorPosition = 0;
	lC_exact_main_launch(handle);
}

/* Default exit sequence for region main */
static void lC_exseq_main(LC* handle)
{
	/* Default exit sequence for region main */
	/* Handle exit of all possible states (of LC.main) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case LC_main_wait :
		{
			lC_exseq_main_wait(handle);
			break;
		}
		case LC_main_cRegresiva_r1_Led2On :
		{
			lC_exseq_main_cRegresiva_r1_Led2On(handle);
			lC_exact_main_cRegresiva(handle);
			break;
		}
		case LC_main_cRegresiva_r1_Led2Off :
		{
			lC_exseq_main_cRegresiva_r1_Led2Off(handle);
			lC_exact_main_cRegresiva(handle);
			break;
		}
		case LC_main_launch :
		{
			lC_exseq_main_launch(handle);
			break;
		}
		default: break;
	}
}

/* Default exit sequence for region r1 */
static void lC_exseq_main_cRegresiva_r1(LC* handle)
{
	/* Default exit sequence for region r1 */
	/* Handle exit of all possible states (of LC.main.cRegresiva.r1) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case LC_main_cRegresiva_r1_Led2On :
		{
			lC_exseq_main_cRegresiva_r1_Led2On(handle);
			break;
		}
		case LC_main_cRegresiva_r1_Led2Off :
		{
			lC_exseq_main_cRegresiva_r1_Led2Off(handle);
			break;
		}
		default: break;
	}
}

/* The reactions of state wait. */
static void lC_react_main_wait(LC* handle)
{
	/* The reactions of state wait. */
	if (lC_check_main_wait_tr0_tr0(handle) == bool_true)
	{ 
		lC_effect_main_wait_tr0(handle);
	} 
}

/* The reactions of state Led2On. */
static void lC_react_main_cRegresiva_r1_Led2On(LC* handle)
{
	/* The reactions of state Led2On. */
	if (lC_check_main_cRegresiva_tr0_tr0(handle) == bool_true)
	{ 
		lC_effect_main_cRegresiva_tr0(handle);
	}  else
	{
		if (lC_check_main_cRegresiva_tr1_tr1(handle) == bool_true)
		{ 
			lC_effect_main_cRegresiva_tr1(handle);
		}  else
		{
			if (lC_check_main_cRegresiva_r1_Led2On_tr0_tr0(handle) == bool_true)
			{ 
				lC_effect_main_cRegresiva_r1_Led2On_tr0(handle);
			} 
		}
	}
}

/* The reactions of state Led2Off. */
static void lC_react_main_cRegresiva_r1_Led2Off(LC* handle)
{
	/* The reactions of state Led2Off. */
	if (lC_check_main_cRegresiva_tr0_tr0(handle) == bool_true)
	{ 
		lC_effect_main_cRegresiva_tr0(handle);
	}  else
	{
		if (lC_check_main_cRegresiva_tr1_tr1(handle) == bool_true)
		{ 
			lC_effect_main_cRegresiva_tr1(handle);
		}  else
		{
			if (lC_check_main_cRegresiva_r1_Led2Off_tr0_tr0(handle) == bool_true)
			{ 
				lC_effect_main_cRegresiva_r1_Led2Off_tr0(handle);
			} 
		}
	}
}

/* The reactions of state launch. */
static void lC_react_main_launch(LC* handle)
{
	/* The reactions of state launch. */
	if (lC_check_main_launch_tr0_tr0(handle) == bool_true)
	{ 
		lC_effect_main_launch_tr0(handle);
	} 
}

/* Default react sequence for initial entry  */
static void lC_react_main__entry_Default(LC* handle)
{
	/* Default react sequence for initial entry  */
	lC_enseq_main_wait_default(handle);
}

/* Default react sequence for initial entry  */
static void lC_react_main_cRegresiva_r1__entry_Default(LC* handle)
{
	/* Default react sequence for initial entry  */
	lC_enseq_main_cRegresiva_r1_Led2On_default(handle);
}


