/** @addtogroup ISR Interrupciones
   \brief Agrupa los handlers de interrupción
*  @{
*/
#ifndef INC_ISR_H_
#define INC_ISR_H_
/**\brief Handler UART2
 */
void UART2_IRQHandler (void);
/**\brief Handler GPIO0
 */
void GPIO0_IRQHandler(void);
/**\brief Handler GPIO1
 */
void GPIO1_IRQHandler(void);
/**\brief Handler GPIO2
 */
void GPIO2_IRQHandler(void);
/**\brief Handler GPIO3
 */
void GPIO3_IRQHandler(void);
/**\brief Handler de SysTick
 */
void SysTick_Handler ( void );

#endif /* INC_ISR_H_ */

/** @}*/
