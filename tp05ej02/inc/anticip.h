/*
 * anticip.h
 *
 *  Created on: Oct 30, 2018
 *      Author: quack
 */

#ifndef INC_ANTICIP_H_
#define INC_ANTICIP_H_

#include "stdint.h"

extern uint32_t interval;

void keyPressed(uint8_t key);

static void resetTest();

static void nextStage();

void tickAnticip();

static void showResults();


#endif /* INC_ANTICIP_H_ */
