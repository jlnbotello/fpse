/*
 * anticip.c
 *
 *  Created on: Oct 30, 2018
 *      Author: quack
 */
#include "anticip.h"
#include "ledsUtils.h"
#include "textUtils.h"
#include "lpc_types.h"
#include "sysConfig.h"

#define DELTA_T 10
#define INITIAL 1000
uint32_t interval=INITIAL;
static uint8_t activeLed;
static uint8_t lastResult=0;
static uint8_t nTick=0;
static uint8_t sec[]={0,1,2,3,2,1,0};
static uint8_t posFin=sizeof(sec);

void keyPressed(uint8_t key){
	if(key==activeLed){
		lastResult+=(1<<(nTick-1));
	}
}

void resetTest(){
	interval=INITIAL;
	nTick=0;
	led(ALL,FALSE);
	delayMs(500);
	lastResult=0;
}

void nextStage(){
	nTick=0;
	interval-=DELTA_T;
	lastResult=0;
}

void tickAnticip(){
	if(nTick<posFin){
		activeLed=sec[nTick];
		led(ALL,FALSE);
		led(sec[nTick],TRUE);
		nTick++;
	}
	else{
		showResults();
		if(lastResult==0x7F){
			destellar(ALL,4,200);
			if(interval>10)
				nextStage();
			else
				resetTest(); //llegó a 10 ms...RE crack!
		}
		else{
			resetTest();
		}
	}
}

void showResults(){
	char ASCIInum[10];
	print("Intervalo de comuntación alcanzado ");
	uint32ToASCII(interval,ASCIInum);
	print(ASCIInum);
	for(uint8_t i=0;i<posFin;i++){
		uint8_t mask=1<<i;
		if(lastResult&mask){
			print(":Pass");
		}
		else
			print(":Fail");
	}
	print("\n");
}
