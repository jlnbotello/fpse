#include "ISR.h"
#include "chip.h"
#include "sysConfig.h"
#include "anticip.h"


void UART2_IRQHandler (void){
	Chip_UART_IRQRBHandler(LPC_USART2, &rxRing, &txRing);
}

void GPIO0_IRQHandler(void){
	keyPressed(TECLA1);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));
}
void GPIO1_IRQHandler(void){
	keyPressed(TECLA2);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(1));
}
void GPIO2_IRQHandler(void){
	keyPressed(TECLA3);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(2));
}
void GPIO3_IRQHandler(void){
	keyPressed(TECLA4);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(3));
}

void SysTick_Handler ( void ){
	static uint32_t cnt = 0;
	cnt = cnt % interval;
	if (cnt == 0){
		tickAnticip();
	}
	cnt ++;
}

