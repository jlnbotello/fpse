#include "sysConfig.h"
#include "ledsUtils.h"

#define DECRECIENTE 0
#define CRECIENTE 	1

void displayCounter(uint8_t v){
	uint8_t bit0=v%2;
	uint8_t bit1=(v>>1)%2;
	uint8_t bit2=(v>>2)%2;
	uint8_t bit3=(v>>3)%2;
	led(LED3,bit0);
	led(LED2,bit1);
	led(LED1,bit2);
	led(LED0,bit3);
}

uint8_t ledZigZag(){
	static uint8_t i=0;
	static uint8_t dir=CRECIENTE;
	led(ALL,FALSE);
	led(i,TRUE);
	uint8_t aux=i;;
	if(i==0)
		dir=CRECIENTE;
	if(i==3)
		dir=DECRECIENTE;

	if(dir==CRECIENTE)
		i++;
	if(dir==DECRECIENTE)
		i--;
	return aux;
}

void destellar(uint8_t nled,uint8_t N,uint32_t ms ){
	led(ALL,FALSE);
	for(uint8_t i=0;i<2*N;i++){
		toggleLed(nled);
		StopWatch_DelayMs(ms);
	}
}

void led(uint8_t led, _Bool setting){
	switch (led) {
		case 0:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU,setting);
			break;
		case 1:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1,setting);
			break;
		case 2:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2,setting);
			break;
		case 3:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3,setting);
			break;
		case 4:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_RED,setting);
			break;
		case 5:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_GRN,setting);
			break;
		case 6:
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2,setting);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3,setting);
			break;
		default:
			break;
	}
}

void toggleLed(uint8_t led){
	switch (led) {
		case 0:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU);
			break;
		case 1:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1);
			break;
		case 2:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2);
			break;
		case 3:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3);
			break;
		case 4:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_RED);
			break;
		case 5:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_GRN);
			break;
		case 6:
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_RGB, GPIO_PIN_BLU);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED2, GPIO_PIN_LED2);
			Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED3, GPIO_PIN_LED3);
			break;
		default:
			break;
	}
}
