/** \mainpage Página Principal
 *
 * Esta aplicación gestiona un contador. Constantemente titilan todos los leds.
 * Cuando se presiona una tecla titila 5 veces el led asociado
 * ##Teclas en EDU-CIAA##
 * + La \ref TECLA1 incrementa el contador
 * + La \ref TECLA2 bloquea el contador
 * + La \ref TECLA3 decrementa el contador
 * + La \ref TECLA4 Vuelve a cero el contador
 *
 * ##Visualización##
 * A través de una terminal gtkTerm
 * ###Configuración###
 * + __Baud Rate:__ 115200
 * + __Paridad:__ No
 * + __Bit de parada:__ 1
 * + __Bits:__ 8
 */
#include "stdint.h"

#include "sysConfig.h"
#include "textUtils.h"
#include "ISR.h"


int main(void){;
	systemInit();
	SysTick_Config ( SystemCoreClock / 1000);
	while (1){
		__WFI();
	};
	return 0;
}
