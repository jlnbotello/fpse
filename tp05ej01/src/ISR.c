#include "ISR.h"
#include "chip.h"

#include "sysConfig.h"
#include "counter.h"

#define INTERVALO 200

void UART2_IRQHandler (void)
{
  Chip_UART_IRQRBHandler(LPC_USART2, &rxRing, &txRing);
}

void GPIO0_IRQHandler(void){
	registroDeCuentas(1);
	destellar(0,5,INTERVALO);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));
}
void GPIO1_IRQHandler(void){
	registroDeCuentas(2);
	destellar(1,5,INTERVALO);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(1));
}
void GPIO2_IRQHandler(void){
	registroDeCuentas(3);
	destellar(2,5,INTERVALO);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(2));
}
void GPIO3_IRQHandler(void){
	registroDeCuentas(4);
	destellar(3,5,INTERVALO);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(3));
}

void SysTick_Handler ( void ){
	static uint32_t cnt = 0;
	static uint8_t estado=0;
	cnt ++;
	cnt = cnt % 1000;
	if (cnt == 0){
		//punto 1.6
		displayCounter(estado);
		estado=~estado;
	}

}

