#include "textUtils.h"

#include "sysConfig.h"

uint32_t serialWrite(const uint8_t *data, unsigned int dataLen){	// autor:sergio burgos
  uint32_t transmited = 0;
  uint32_t toInsert;
  uint32_t freeSpc;
  while(transmited < dataLen)
  {
    freeSpc = RingBuffer_GetFree(&txRing);
    while(!freeSpc)
    	freeSpc = RingBuffer_GetFree(&txRing);
    toInsert = ((freeSpc > dataLen) ? dataLen : freeSpc);
    Chip_UART_SendRB(LPC_USART2, &txRing, &data[transmited], toInsert);
    transmited += toInsert;
  }
  return transmited;
}

uint32_t serialRead(uint8_t *data, unsigned int maxData){	// autor:sergio burgos
  return Chip_UART_ReadRB(LPC_USART2, &rxRing, data, maxData);
}

void putChr(char ch){
	Chip_UART_SendBlocking ( LPC_USART2 ,&ch , 1);
}

uint32_t print(char *txt){
	return serialWrite(txt,strlen(txt));
}

void mostrarDec(uint32_t valor){
	uint8_t N=nDigitos(valor);
	uint8_t auxASCII[N];
	for(uint8_t i=0; i<N;i++)
	{
		auxASCII[N-1-i]=menosSigniASCII(valor);
		valor/=10;
	}
	for(uint8_t i=0; i<N;i++){
		putChr(auxASCII[i]);
	}
}

void uint32ToASCII(uint32_t valor, char * toASCII){
	uint8_t aux[10];
	uint8_t N=digitosDec(valor,aux);
	for(uint8_t i=0; i<N;i++)
		toASCII[i]=aux[i]+'0';
	toASCII[N]='\0';
}

uint8_t menosSigniASCII(uint32_t valor){
	uint8_t mSigni = valor%10;
	return '0' + mSigni;
}

uint8_t digitosDec(uint32_t valor, uint8_t * digitos){

	uint8_t cant=1;
	uint8_t aux[10];
	aux[cant-1]=valor%10;
	while(valor>=10){
		cant++;
		valor/=10;
		aux[cant-1]=valor%10;
	}
	for(uint8_t i=0;i<cant;i++){
		digitos[cant-1-i]=aux[i];
	}
	return cant;
}

uint8_t nDigitos(uint32_t valor){
	uint8_t cant=1;
	while(valor>=10){
		valor/=10;
		cant++;
	}
	return cant;
}
