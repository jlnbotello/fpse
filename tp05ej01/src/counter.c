#include <lpc_types.h>
#include "counter.h"

void registroDeCuentas(uint8_t tecla){
	static uint32_t contador=0;
	static bool habilitado=TRUE;
	switch (tecla) {
		case 1:
			if(habilitado==TRUE)
				contador++;
			break;
		case 2:
			habilitado=!habilitado;
			break;
		case 3:
			if(habilitado==TRUE)
				contador--;
			break;
		case 4:
			contador=0;
			break;
		default:
			break;
	}
	mostrarDec(contador);
	print("\n");
}
