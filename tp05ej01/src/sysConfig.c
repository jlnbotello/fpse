#include "sysConfig.h"

const uint32_t ExtRateIn = 0;  /**< Frecuencia de clock externa inyectada al microcontrolador. No utilizado en las CIAA. */
const uint32_t OscRateIn = 12000000; /**< Frecuencia del oscilador externo incorporado en la CIAA-NXP. */
RINGBUFF_T txRing;
RINGBUFF_T rxRing;
#define BUFFSize 512 /**< Cantidad máxima de valores a alojar en el buffer de entrada y en el de salida*/
static uint8_t rxBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de entrada */
static uint8_t txBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de salida */

void systemInit(void)
{	//CLOCK
	Chip_SetupXtalClocking();
	SystemCoreClockUpdate();
	//FPU
    fpuInit();
    //STOPWATCH
    StopWatch_Init();
    //GPIO
	Chip_GPIO_Init(LPC_GPIO_PORT);
    //LEDS
    ledsInit();
    //KEYS
    keysInit();
    //UART
    uartInit();
    //PINS INTERRUPTIONS
    pinsIRQInit();

}

void ledsInit(void){
	// LEDS MUX
	Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);
	//LEDS DIR: OUTPUT
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);
	//LEDS INITIAL STATUS: OFF
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED2, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED3, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN, FALSE);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU, FALSE);
}

void keysInit(void){
	//KEYS MUX
	Chip_SCU_PinMux(PORT_PIN_KEY1, PIN_KEY1, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY2, PIN_KEY2, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY3, PIN_KEY3, MD_EZI|MD_ZI, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_KEY4, PIN_KEY4, MD_EZI|MD_ZI, FUNC0);
	//KEYS DIR: INPUT
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY1, GPIO_PIN_KEY1);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY2, GPIO_PIN_KEY2);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY3, GPIO_PIN_KEY3);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, GPIO_PORT_KEY4, GPIO_PIN_KEY4);
}

void uartInit(void){
	//MUX UART
	Chip_SCU_PinMux(7, 1, MD_PDN, FUNC6);
	Chip_SCU_PinMux(7, 2, MD_PLN|MD_EZI|MD_ZI, FUNC6);
	//UART CONFIG
	Chip_UART_Init(LPC_USART2);
    Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
    Chip_UART_SetBaud(LPC_USART2, 115200);
    Chip_UART_SetupFIFOS(LPC_USART2, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
    //RINGBUFFER
    RingBuffer_Init(&rxRing, rxBuff, 1, BUFFSize);
    RingBuffer_Init(&txRing, txBuff, 1, BUFFSize);
    //UART INTERRUP
    Chip_UART_IntEnable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT));
    NVIC_EnableIRQ(USART2_IRQn);
    Chip_UART_TXEnable(LPC_USART2);
}

void pinsIRQInit(){
	Chip_PININT_Init ( LPC_GPIO_PIN_INT );

	Chip_SCU_GPIOIntPinSel (TECLA1, GPIO_PORT_KEY1, GPIO_PIN_KEY1);
	Chip_SCU_GPIOIntPinSel (TECLA2, GPIO_PORT_KEY2, GPIO_PIN_KEY2);
	Chip_SCU_GPIOIntPinSel (TECLA3, GPIO_PORT_KEY3, GPIO_PIN_KEY3);
	Chip_SCU_GPIOIntPinSel (TECLA4, GPIO_PORT_KEY4, GPIO_PIN_KEY4);

	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
	Chip_PININT_SetPinModeEdge (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
	Chip_PININT_EnableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));

	NVIC_ClearPendingIRQ( PIN_INT0_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT1_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT2_IRQn );
	NVIC_ClearPendingIRQ( PIN_INT3_IRQn );

	NVIC_EnableIRQ( PIN_INT0_IRQn);
	NVIC_EnableIRQ( PIN_INT1_IRQn);
	NVIC_EnableIRQ( PIN_INT2_IRQn);
	NVIC_EnableIRQ( PIN_INT3_IRQn);
}



