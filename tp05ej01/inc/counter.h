/** @addtogroup counter Contador
   \brief Módulo principal del contador
*  @{
*/
#ifndef INC_COUNTER_H_
#define INC_COUNTER_H_
/**\brief Gestiona el contador de acuerdo a la tecla recibida
 *
 * La \ref TECLA1 incrementa el contador
 * La \ref TECLA2 bloquea el contador
 * La \ref TECLA3 decrementa el contador
 * La \ref TECLA4 Vuelve a cero el contador
 * NOTA: La función es llamada desde las interrupciones de las teclas
 * \param[in] tecla Tecla que se presionó
 */
void registroDeCuentas(uint8_t tecla);


#endif /* INC_COUNTER_H_ */

/** @}*/
