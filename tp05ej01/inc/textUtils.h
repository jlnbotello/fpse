/** @addtogroup textUtils Utilidades de texto
   \brief Funciones para enviar texto y números por USART2
*  @{
*/
#ifndef INC_TEXTUTILS_H_
#define INC_TEXTUTILS_H_
#include "stdint.h"
/**\brief Envía una cadena de caracteres por USART2
 *
 * \author Mg. Ing. E. Sergio Burgos
 *
 * Utiliza buffer circular (lpcopen). Actua en modo no bloqueante a menos que
 * se llene el buffer circular
 * \param[in] data Puntero a arreglo de caracteres
 * \param[in] dataLen Cantidad de elementos del arreglo que se desean enviar
 * \return Cantidad de elementos transmitidos
 */
uint32_t serialWrite(const uint8_t *data, unsigned int dataLen);

/**\brief Lee una cadena de caracteres de USART2
 *
 * \author Mg. Ing. E. Sergio Burgos
 *
 * Utiliza buffer circular (lpcopen).
 * \param[out] data puntero a arreglo donde se guarda los caractes recibidos
 * \param[in] maxData Cantidad de caratectes por leer
 */
uint32_t serialRead(uint8_t *data, unsigned int maxData);

/**\brief Enviar un caracter por USART2
 *
 * Envía el caracter en modo bloqueante
 * \param[in] ch Caracter que se envía
 */
void putChr(char ch);

/**\brief Envia por USART2 una cadena de caracteres. DEBE finalizar con el caracter '\0'
 * \param[in] txt Puntero a arreglo de datos tipo char
 */
uint32_t print(char *txt);

/**\brief Envia POR USART2 un numero como ASCII
 * \param[in] valor Valor que se convierte y envía
 */
void mostrarDec(uint32_t valor);

/**\brief Convierte un valor a un representación ASCII
 *
 * Función para utilizar en combinación con \ref print(char *txt)
 *
 * \param[in] valor Valor a convertir
 * \param[out] toASCII Arreglo de caracteres que respresentan valor. Agrega '\0' al final
 */
void uint32ToASCII(uint32_t valor, char * toASCII);

/**\brief Devuelve el ASCII del digito menos significativo
 * \param[in] valor Valor de entrada
 * \return ASCII del digito menos significativo del valor en representación decimal
 */
uint8_t menosSigniASCII(uint32_t valor);


/**\brief Determina los digitos de un número
 * \param[in] valor Número
 * \param[out] digitos Arreglo de digitos
 * \return Cantidad de dígitos
 */
uint8_t digitosDec(uint32_t valor, uint8_t * digitos);

/**\brief Retorna la cantidad de dígitos de un número en representación decimal
 * \param[in] valor Valor de entrada
 */
uint8_t nDigitos(uint32_t valor);

#endif /* INC_TEXTUTILS_H_ */

/** @}*/
