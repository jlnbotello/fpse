#ifndef INC_APPFUNC_H_
#define INC_APPFUNC_H_

#include "stdint.h"
#include "hw.h"

#define ETAPAS_TOTALES 	10
#define JUGAR 	0
#define GANA 	1
#define PIERDE	2

static uint8_t estado = JUGAR;
static uint8_t secuencia[ETAPAS_TOTALES]={2,2,3,3,4,4,2,2,3,3};
static uint8_t teclaPresionada=NINGUNA;

void generarSecuencia(void);
void jugar(void);
void registrarTecla(uint8_t);





#endif /* INC_APPFUNC_H_ */
