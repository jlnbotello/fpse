#include "appfunc.h"
#include "stdint.h"

void generarSecuencia(void){
	//  1 2 3 4 1 2 3 4 1 2
	//for(uint8_t i=0;i<ETAPAS_TOTALES;i++){
		//secuencia[i]=i%4+1;
	//};
}

void jugar(void){
	while(1){
		uint8_t etapa=0;
		generarSecuencia();
		SysTick->CTRL = SysTick->CTRL | (SysTick_CTRL_ENABLE_Msk);  //enable zigzag leds
		teclaPresionada=NINGUNA;
		Chip_PININT_EnableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
		while(teclaPresionada==NINGUNA){};
		SysTick->CTRL = SysTick->CTRL & (~SysTick_CTRL_ENABLE_Msk); //disable
		led(ALL,FALSE);
		delayMs(500);
		while(estado==JUGAR && etapa<ETAPAS_TOTALES){
			// Muetra la secuencia
			for(uint8_t i=0;i<=etapa;i++){
				delayMs(300);
				led(secuencia[i]-1,TRUE);
				delayMs(300);
				led(secuencia[i]-1,FALSE);
			}
			uint8_t subetapa=0;
			teclaPresionada=NINGUNA;
			while(estado!=PIERDE && subetapa<=etapa){
				while(teclaPresionada==NINGUNA){}; //espera
				Chip_PININT_DisableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
				if(teclaPresionada==secuencia[subetapa]){
					subetapa++;
					if(subetapa==ETAPAS_TOTALES)
						estado=GANA;
				}
				else{
					Chip_PININT_DisableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
					estado=PIERDE;
				}
				Chip_PININT_EnableIntLow (LPC_GPIO_PIN_INT , PININTCH (0)|PININTCH (1)|PININTCH (2)|PININTCH (3));
				teclaPresionada=NINGUNA;
			}
			etapa++;
		}
		if(estado==GANA)
			destellar(LED3,5,250);
		if(estado==PIERDE)
			destellar(LED1,5,250);
		estado=JUGAR;
	}
}

void registrarTecla(uint8_t tecla){
	teclaPresionada = tecla;
}







