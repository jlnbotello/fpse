#include <stdint.h>
#include <stdio.h>
#include <math.h>

/* Retorna la cantidad de dígitos de una valor contenido en una variable
 * de tipo entero de 32 bits.
 */
uint8_t nDigitos(uint32_t valor);

/* Retorna un valor conformado por los n dígitos más significativos de un
 * valor entero, sin signo de 32 bits.
 */
uint32_t nMasSigni(uint32_t valor, uint8_t n);

/* Mostrar por pantalla la representación en base 2 de un valor entero,
 * sin signo de 32 bits.
 */
void mostrarBin(uint32_t valor);

/* Retornar la representación ASCII del dígito menos significativo de un
 * valor entero de 32 bits, sin signo, dado como argumento.
 */
uint8_t menosSigni(uint32_t valor);

/* Retorna el bit de la posición pasada como argunmento
 */
uint8_t bitDePos(uint32_t valor, uint8_t pos);

/* Retorna el digito en la posición N de un decimal
 */
uint8_t digitoDecimalN(uint32_t valor, uint8_t pos);

/* Muestra en la salida estandar un array de caracteres.
 * El caracter de fin debe ser  '\0'
 */
void putStr(char * str);

/* Muestra en la salida estandar los digitos de un decimal separados con espacios
 */
void mostrarDec(uint32_t valor);

/* En puntero tuASCII apuntará a un arreglo con los caracteres ASCII
 * de un valor de uint32_t pasado como parámetro.
 */
void uint32ToASCII(uint32_t valor, char * toASCII);

/* Retorna la cantidad de digitos de un valor uint32_t. Se debe dar como parámetros
 * un puntero a un arrreglo de 10 elementos (máximas cantidad de dítigos de un uint32_t)
 */
uint8_t digitosDec(uint32_t valor, uint8_t * digitos);

// MAIN

int main(int argc, char * argv[]){
	uint32_t valor=0;
	char valorASCII[11];
	putStr("Ingrese un valor de prueba: ");
	scanf("%u", &valor);
	// Cantidad de ditigos
	uint8_t N= nDigitos(valor);
	putStr("Tiene ");
	uint32ToASCII(N, valorASCII);
	putStr(valorASCII);
	putStr(" digitos\n");
	// Digitos significativos
	for(uint8_t i=1;i<=N;i++){
		putStr("Cantidad de dígitos significativos ");
		uint32ToASCII(i, valorASCII);
		putStr(valorASCII);
		putStr(" = ");
		uint32ToASCII(nMasSigni(valor,i), valorASCII);
		putStr(valorASCII);
		putStr("\n");
	}
	// Base 2
	putStr("Representación en base 2: ");
	mostrarBin(valor);
	putStr("\n");
	// Por carateres
	putStr("Valor mostrado por caracteres: ");
	mostrarDec(valor);
}

uint8_t nDigitos(uint32_t valor){
	uint8_t cant=1;
	while(valor>=10){
		valor/=10;
		cant++;
	}
	return cant;
}

uint32_t nMasSigni(uint32_t valor, uint8_t n){
	uint8_t total = nDigitos(valor);
	if(n<1)
		return 0;
	if(n>total)
		n=total;
	for(uint8_t i=0;i<total-n;i++)
		valor/=10;
	return valor;
}

void mostrarBin(uint32_t valor){
	for(uint8_t i=32;i>0;i--){
		putchar('0'+ bitDePos(valor, i-1));
		if((i-1)%4==0&&i!=32) // pone un espacio cada 4
			putchar(' ');
	}
}

uint8_t menosSigni(uint32_t valor){
	uint8_t mSigni = valor%10;
	return '0' + mSigni;
}

uint8_t bitDePos(uint32_t valor, uint8_t pos){
	uint32_t masc=1<<pos;
	if(pos<32){
		return (uint8_t)((masc&valor)>>pos);
	}
	else{
		printf("Error: n fuera de rango. Rango: 0-31");
		return 2;
	}
}

uint8_t digitoDecimalN(uint32_t valor, uint8_t pos){
	uint8_t dig=0;
	uint32_t resto=0;
	uint32_t divisor=pow(10,pos);
	if(pos<9){
		divisor*=10;
		resto = valor%divisor;
		divisor/=10;
		dig=resto/divisor;
	}
	else{
	dig=(uint8_t)valor/divisor;
	}
	return dig;
}

void putStr(char * str){
	uint8_t i=0;
	while(str[i]!='\0')
	{
		putchar(str[i]);
		i++;
	}
}

void mostrarDec(uint32_t valor){
	uint8_t N=nDigitos(valor);
	uint8_t auxASCII[N];
	for(uint8_t i=0; i<N;i++)
	{
		auxASCII[N-1-i]=menosSigni(valor);
		valor/=10;
	}
	for(uint8_t i=0; i<N;i++){
		putchar(auxASCII[i]);
		putchar(' ');
	}
}

void uint32ToASCII(uint32_t valor, char * toASCII){
	uint8_t aux[10];
	uint8_t N=digitosDec(valor,aux);
	for(uint8_t i=0; i<N;i++)
		toASCII[i]=aux[i]+'0';
	toASCII[N]='\0';
}

uint8_t digitosDec(uint32_t valor, uint8_t * digitos){

	uint8_t cant=1;
	uint8_t aux[10];
	aux[cant-1]=valor%10;
	while(valor>=10){
		cant++;
		valor/=10;
		aux[cant-1]=valor%10;
	}
	for(uint8_t i=0;i<cant;i++){
		digitos[cant-1-i]=aux[i];
	}
	return cant;
}






