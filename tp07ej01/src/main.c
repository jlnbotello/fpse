#include "stdint.h"
#include "sysUtils.h"
#include "lpc_types.h"
#include "chip.h"
#include "microrl.h"
#include "secuencia.h"

#define CORE_M4
const uint32_t ExtRateIn = 0;  /**< Frecuencia de clock externa inyectada al microcontrolador. No utilizado en las CIAA. */
const uint32_t OscRateIn = 12000000; /**< Frecuencia del oscilador externo incorporado en la CIAA-NXP. */
#define GPIO_PORT_LED1 0x00
#define GPIO_PIN_LED1  0x0E

microrl_t rl ;

void UART2_IRQHandler (void)
{
	//Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,GPIO_PORT_LED1, GPIO_PIN_LED1);
	uint8_t data = Chip_UART_ReadByte ( LPC_USART2 );
	microrl_insert_char (&rl , data );
}


void SysTick_Handler ( void ){
	static uint16_t cntTick = 0;
	if(cntTick==0)
		tickSecuencia();
	cntTick++;
	cntTick=cntTick%iDelayMs;
}

int main(void){;
	Chip_SetupXtalClocking ();
	SystemCoreClockUpdate ();
	InitUI();
	InitUsart2();
	SysTick_Config ( SystemCoreClock / 1000);
	SysTick->CTRL = SysTick->CTRL & (~SysTick_CTRL_ENABLE_Msk);
	//secuencia(0, 5, 50);
	//uint8_t ch ;
	print("Bienvenido a la EDU-CIAA 2.0\n\r");
	microrl_init(& rl , print );
	microrl_set_execute_callback (& rl , cmdExecute) ;
	microrl_set_sigint_callback (& rl , sigint) ;
	while(1){};

	return 0;
}
