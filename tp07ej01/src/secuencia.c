/*
 * secuencia.c
 *
 *  Created on: Oct 15, 2018
 *      Author: quack
 */
#include "secuencia.h"
#include "sysUtils.h"
#include "chip.h"


static uint8_t iSecNro = 0 ;
static uint8_t iCntRep = 0 ;
	   uint16_t iDelayMs = 1000;
static uint8_t iActPos = 0 ;
const uint8_t ledSec1 [ ] = {8 , 1 , 2 , 4 , 8 , 8 , 4 , 2 , 1};
const uint8_t ledSec2 [ ] = {4 , 9 , 6 , 6 , 9};
const uint8_t * const secuencias [ ] ={ledSec1 , ledSec2 } ;

void secuencia(uint8_t secNro, uint8_t cntRep, uint16_t delayMs){
	iSecNro = secNro ;
	iCntRep = cntRep ;
	iDelayMs = delayMs;
	SysTick->CTRL = SysTick->CTRL | (SysTick_CTRL_ENABLE_Msk);
}

void tickSecuencia(){
	if(iCntRep!=0)
	{
		iActPos++;
		allLedsOff();
		setLedFromMsk(secuencias[iSecNro][iActPos],true);
		if(iActPos==secuencias[iSecNro][0])
		{
			iActPos=0;
			iCntRep--;
		}
	}
	else
	{
		SysTick->CTRL = SysTick->CTRL & (~SysTick_CTRL_ENABLE_Msk);
		allLedsOff();
	}



}

void setLedFromMsk(uint8_t msk,uint8_t setting)
{
	if (msk & 1)
		Chip_GPIO_SetPinState(LPC_GPIO_PORT,leds[0].gpioPort, leds[0].gpioPin,setting);

	if (msk & 2)
		Chip_GPIO_SetPinState(LPC_GPIO_PORT,leds[1].gpioPort, leds[1].gpioPin,setting);

	if (msk & 4)
		Chip_GPIO_SetPinState(LPC_GPIO_PORT,leds[2].gpioPort, leds[2].gpioPin,setting);

	if (msk & 8)
		Chip_GPIO_SetPinState(LPC_GPIO_PORT,leds[3].gpioPort, leds[3].gpioPin,setting);
}

void allLedsOff(void)
{
	Chip_GPIO_SetPortOutLow ( LPC_GPIO_PORT , leds [0]. gpioPort , ((1<<leds [0]. gpioPin) | (1<<leds [4]. gpioPin) | (1<<leds [5]. gpioPin)));
	Chip_GPIO_SetPortOutLow ( LPC_GPIO_PORT , leds [1]. gpioPort , (1<<leds [1]. gpioPin) );
	Chip_GPIO_SetPortOutLow ( LPC_GPIO_PORT , leds [2]. gpioPort , ((1<<leds [2]. gpioPin) | (1<<leds [3]. gpioPin))); // comparten el puerto del 2
}
