/*
 * comandos.c
 *
 *  Created on: Oct 16, 2018
 *      Author: quack
 */
#include "comandos.h"
#include "lpc_types.h"
menuItem menu [ ] =
{
	{"help" , help } ,

	{"led1On" , led1On } ,
	{"led2On" , led2On } ,
	{"led3On" , led3On } ,
	{"led4On" , led4On } ,

	{"led1Off" , led1Off } ,
	{"led2Off" , led2Off } ,
	{"led3Off" , led3Off } ,
	{"led4Off" , led4Off } ,

	{"secuencia_1" , sec1 } ,
	{"secuencia_2" , sec2 } ,
	{"cls" , cls }
};
menuItem * getMenuItems ( void ){
	return menu;
}
unsigned int getMenuItemsCount ( void ){
	return (sizeof(menu)/sizeof(menuItem));
}

void help ( int argc , const char * const * argv ){

}
void sec1 ( int argc , const char * const * argv )
{
	secuencia(0, 1, 500);
}
void sec2 ( int argc , const char * const * argv )
{
	secuencia(1, 1, 500);
}
void led1On ( int argc , const char * const * argv )
{
	setLedFromMsk(1,true);
}
void led2On ( int argc , const char * const * argv )
{
	setLedFromMsk(2,true);
}
void led3On ( int argc , const char * const * argv )
{
	setLedFromMsk(4,true);
}
void led4On ( int argc , const char * const * argv )
{
	setLedFromMsk(8,true);
}
void led1Off( int argc , const char * const * argv )
{
	setLedFromMsk(1,false);
}
void led2Off( int argc , const char * const * argv )
{
	setLedFromMsk(2,false);
}
void led3Off( int argc , const char * const * argv )
{
	setLedFromMsk(4,false);
}
void led4Off( int argc , const char * const * argv )
{
	setLedFromMsk(8,false);
}

void cls ( int argc , const char * const * argv )
{
	print("\033[2J\033[;f") ;
}
