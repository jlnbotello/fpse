/*
 * sysUtils.c
 *
 *  Created on: Oct 7, 2018
 *      Author: quack
 */
#include "sysUtils.h"
#include "chip.h"
#include "comandos.h"


static RINGBUFF_T txRing; /**< Estructura asociado al buffer circular de transmisión. */
static RINGBUFF_T rxRing; /**< Estructura asociada al buffer circular de recepción.  */
#define BUFFSize 512 /**< Cantidad máxima de valores a alojar en el buffer de entrada y en el de salida*/
static unsigned char rxBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de entrada */
static unsigned char txBuff[BUFFSize]; /**< Vector para contener los datos asociados el buffer de salida */

const digitalIO leds[] =
{
	{0x02 , 0x02 , 0x05 , 0x02 , SCU_MODE_INACT | SCU_MODE_FUNC4} ,
	{0x02 , 0x0A , 0x00 , 0x0E , SCU_MODE_INACT | SCU_MODE_FUNC0} ,
	{0x02 , 0x0B , 0x01 , 0x0B , SCU_MODE_INACT | SCU_MODE_FUNC0} ,
	{0x02 , 0x0C , 0x01 , 0x0C , SCU_MODE_INACT | SCU_MODE_FUNC0} ,
	{0x02 , 0x00 , 0x05 , 0x00 , SCU_MODE_INACT | SCU_MODE_FUNC4} ,
	{0x02 , 0x01 , 0x05 , 0x01 , SCU_MODE_INACT | SCU_MODE_FUNC4}
};
const digitalIO keys[] =
{
	{0x01 , 0x00 , 0x00 , 0x04 , SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN} ,
	{0x01 , 0x01 , 0x00 , 0x08 , SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN} ,
	{0x01 , 0x02 , 0x00 , 0x09 , SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN} ,
	{0x01 , 0x06 , 0x01 , 0x09 , SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN}
};

void InitUI(void){
	int i;
	for ( i = 0; i < sizeof ( leds )/ sizeof ( digitalIO ); i ++)
	{
		Chip_SCU_PinMuxSet(leds[i].hwPort, leds[i].hwPin, leds[i].modo);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT,leds[i].gpioPort, leds[i].gpioPin);
		//Chip_GPIO_SetPinState(LPC_GPIO_PORT,leds[i].gpioPort, leds[i].gpioPin,0);
		Chip_GPIO_SetPortOutLow ( LPC_GPIO_PORT , leds [i]. gpioPort , 1<<leds [i]. gpioPin );
	}
	for (uint8_t i = 0; i < sizeof ( keys )/ sizeof ( digitalIO ); i ++)
	{
		Chip_SCU_PinMuxSet(keys[i].hwPort, keys[i].hwPin, keys[i].modo);
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT,keys[i].gpioPort, keys[i].gpioPin);
	}
}

void InitUsart2(void){
	Chip_SCU_PinMuxSet (USART2_PORT, USART2_PIN_TX, SCU_MODE_PULLDOWN | SCU_MODE_FUNC6 );
	Chip_SCU_PinMuxSet (USART2_PORT, USART2_PIN_RX,	SCU_MODE_INACT|SCU_MODE_INBUFF_EN|SCU_MODE_ZIF_DIS|SCU_MODE_FUNC6);
	Chip_UART_Init(LPC_USART2);
	Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
	Chip_UART_SetBaud(LPC_USART2, 115200);
	//UART INTERRUP
	Chip_UART_IntEnable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT));
	NVIC_EnableIRQ(USART2_IRQn);
	Chip_UART_TXEnable ( LPC_USART2 );
}

void print(const char *txt){


	Chip_UART_SendBlocking(LPC_USART2,txt,strlen(txt));
}

void cmdExecute( int argc , const char * const * argv )
{
	menuItem* menu =  getMenuItems();
	for(unsigned int i=0;i<getMenuItemsCount();i++){
		if(0==strcmp(menu[i].cmdName,argv[0])){
			menu[i].cmdAct(argc,argv);
		}
	}

	/*
	print ( "Rcv: " );
	for(int i=0;i<argc;i++){
		print ( argv [i]);
		if(i!=argc-1)
			print(" ");
	}
	print ( "\r");
	*/
}

void sigint ( void )
{
Chip_RGU_TriggerReset (RGU_CORE_RST) ;
}
