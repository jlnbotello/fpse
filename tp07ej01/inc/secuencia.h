/*
 * secuencia.h
 *
 *  Created on: Oct 15, 2018
 *      Author: quack
 */

#ifndef INC_SECUENCIA_H_
#define INC_SECUENCIA_H_
#include "stdint.h"
extern uint16_t iDelayMs;

void secuencia(uint8_t secNro, uint8_t cntRep, uint16_t delayMs);
void tickSecuencia(void);
void setLedFromMsk(uint8_t msk,uint8_t setting);
void allLedsOff(void);


#endif /* INC_SECUENCIA_H_ */
