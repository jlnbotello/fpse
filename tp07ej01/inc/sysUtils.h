/*
 * sysUtils.h
 *
 *  Created on: Oct 7, 2018
 *      Author: quack
 */

#ifndef INC_SYSUTILS_H_
#define INC_SYSUTILS_H_
#include "stdint.h"

#define USART2_PORT    0x07
#define USART2_PIN_TX  0x01
#define USART2_PIN_RX  0x02

typedef struct{
	uint8_t hwPort ;
	uint8_t hwPin ;
	uint8_t gpioPort ;
	uint8_t gpioPin ;
	uint16_t modo ;
} digitalIO ;

extern const digitalIO leds[];

void InitUI(void);

void InitUsart2(void);

void print(const char *txt);

void cmdExecute( int argc , const char * const * argv );

void sigint ( void );

#endif /* INC_SYSUTILS_H_ */
