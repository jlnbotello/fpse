/*
 * comandos.h
 *
 *  Created on: Oct 16, 2018
 *      Author: quack
 */

#ifndef INC_COMANDOS_H_
#define INC_COMANDOS_H_
typedef void (* doAction ) ( int , const char * const * ) ;

typedef struct
{
	char * cmdName ;
	doAction cmdAct ;
}menuItem ;

void help ( int , const char * const * ) ;
void sec1 ( int , const char * const * ) ;
void sec2 ( int , const char * const * ) ;
void led1On ( int , const char * const * ) ;
void led2On ( int , const char * const * ) ;
void led3On ( int , const char * const * ) ;
void led4On ( int , const char * const * ) ;
void led1Off( int , const char * const * ) ;
void led2Off( int , const char * const * ) ;
void led3Off( int , const char * const * ) ;
void led4Off( int , const char * const * ) ;
void cls( int , const char * const * ) ;

menuItem * getMenuItems ( void ) ;
unsigned int getMenuItemsCount ( void ) ;

#endif /* INC_COMANDOS_H_ */
