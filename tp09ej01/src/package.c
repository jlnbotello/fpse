/*
 * package.c
 *
 *  Created on: Oct 16, 2018
 *      Author: quack
 *
 */
// MAQUINA DE MEALY
#include "package.h"
#include "hw.h"
static uint8_t headerVal=0x0C;
static uint8_t footerVal=0xC0;

const stFunctions stTrans [] = {
	stateHeader , stateCmd ,
	stateTarget  , stateSum ,
	stateFooter
};

void stateInit ( statePkg * stPkg ){
stPkg->actState = stHeader ;
}

uint8_t stateHeader ( statePkg * stPkg , uint8_t newData ){
	if( newData == headerVal )
	{
	stPkg->data.header = headerVal ;
	stPkg->data.scCalc = headerVal ;
	stPkg->actState = stCmd ;
	}
	return 0;
}

uint8_t stateCmd( statePkg * stPkg , uint8_t newData ){
	stPkg->data.scCalc += newData ;
	if (newData==0x0F||newData==0xF0)
	{
		stPkg->data.cmd=newData;
		stPkg->actState = stTarget ;
	}
	else
	stPkg->actState = stHeader ;
	return 0;
}

uint8_t stateTarget ( statePkg * stPkg , uint8_t newData ){
	stPkg->data.scCalc += newData ;
	if (newData>0&&newData<=6)
	{
		stPkg->data.target=newData;
		stPkg->actState = stSum ;
	}
	else
	stPkg->actState = stHeader ;
	return 0;
}

uint8_t stateSum ( statePkg * stPkg , uint8_t newData ){
	stPkg->data.scRcv = newData ;
	if (newData == stPkg->data.scCalc)
		stPkg->actState = stFooter ;
	else
		stPkg->actState = stHeader ;
	return 0;
}

uint8_t stateFooter ( statePkg * stPkg , uint8_t newData ){
	uint8_t ret = 0;
	stPkg->data.footer = newData ;
	if ( newData == footerVal )
		ret = 1;
	stPkg->actState = stHeader ;
	return ret;
}

const pkgAction pkgActions [] =
{
	doLed1 ,doLed2 , doLed3 ,
	doLedR , doLedG , doLedB
};

void doLed1 ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
			ledOn(LED1);
	if(pkg->cmd==0XF0)
			ledOff(LED1);
}

void doLed2 ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
			ledOn(LED2);
	if(pkg->cmd==0XF0)
		ledOff(LED2);
}
void doLed3 ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
			ledOn(LED3);
	if(pkg->cmd==0XF0)
		ledOff(LED3);
}

void doLedR ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
		ledOn(LEDR);
	if(pkg->cmd==0XF0)
		ledOff(LEDR);
}

void doLedG ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
		ledOn(LEDG);
	if(pkg->cmd==0XF0)
		ledOff(LEDG);
}

void doLedB ( pkgData * pkg ){
	if(pkg->cmd==0X0F)
		ledOn(LEDB);
	if(pkg->cmd==0XF0)
		ledOff(LEDB);
}

/* Conmutación de estados */
void processPackage ( uint8_t data ){
	static statePkg pkg ;
	static uint8_t firstTime = 1;
	if( firstTime ){
	stateInit (& pkg );
	firstTime = ! firstTime ;
	}
	if( stTrans[pkg.actState](& pkg , data ))
		pkgActions [pkg.data.target-1](& pkg.data );
}
