/*
 * package.h
 *
 *  Created on: Oct 16, 2018
 *      Author: quack
 */

#ifndef INC_PACKAGE_H_
#define INC_PACKAGE_H_
#include "stdint.h"


/* Definición asociada a un paquete de datos */
typedef struct {
	uint8_t header ;
	uint8_t cmd ;
	uint8_t target ;
	uint8_t scRcv ;
	uint8_t scCalc ;
	uint8_t footer ;
} pkgData ;

/* Declaración de los estados posibles */
typedef enum {
	stHeader = 0, stCmd = 1,
	stTarget  = 2, stSum   = 3,
	stFooter = 4
} stEnum ;

typedef struct {
	pkgData data ;
	stEnum actState ;
} statePkg ;

typedef uint8_t (* stFunctions )(statePkg *, uint8_t );

/* Inicializa la estructura */
void stateInit ( statePkg *);

/* Funciones para conmutar de estado */
uint8_t stateHeader ( statePkg *, uint8_t );
uint8_t stateCmd ( statePkg *, uint8_t );
uint8_t stateTarget ( statePkg *, uint8_t );
uint8_t stateSum ( statePkg *, uint8_t );
uint8_t stateFooter ( statePkg *, uint8_t );

/* Declaraciones asociadas a las acciones */
typedef void (* pkgAction )( pkgData *);
void doLed1 ( pkgData *);
void doLed2 ( pkgData *);
void doLed3 ( pkgData *);
void doLedR ( pkgData *);
void doLedG ( pkgData *);
void doLedB ( pkgData *);

void processPackage ( uint8_t data );

#endif /* INC_PACKAGE_H_ */
